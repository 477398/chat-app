# Creating a custom Dockerfile, because I couldn't find any Docker images with .NET 8 pre-installed, which my application uses.
# Credits: https://learn.microsoft.com/en-gb/aspnet/core/host-and-deploy/docker/building-net-docker-images

# Obtains the .NET 8 SDK image, which contains CLI and tools to run "build", "test", and "publish"
FROM mcr.microsoft.com/dotnet/sdk:8.0

# Copy my whole repository to the inside of the image.
COPY . ./MyProject

# Set app settings
COPY ./WebServer/Server/appsettings.Docker.json ./MyProject/WebServer/Server/appsettings.json

# Restore tools and dependencies (to make sure nothing broke during the copy-paste)
RUN dotnet restore "./MyProject/WebServer/WebServer.sln"

# # Updating "Advanced Packaging Tool"
# Installing Node.JS, which is mandatory to "dotnet build" the MVC Angular-/React-application. Credits: https://github.com/DotNet-Docker-Images/dotnet-nodejs-docker
# Installing Java required for "SonarScanner for .NET"
RUN apt-get -y update
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_lts.x | bash
RUN apt-get install -y nodejs
RUN apt-get install -y default-jre
RUN apt-get clean

# Add dotnet-tools/java directory to the PATH
ENV PATH="${PATH}:/root/.dotnet/tools"
ENV PATH="/usr/lib/jvm/java-11-openjdk-amd64/bin:${PATH}"