﻿namespace Server.Responses
{
    public class Response
    {
        public bool Success { get; protected set; } = true;

        public string Message { get; protected set; } = "";
    }
}
