﻿namespace Server.Responses
{
    public class ErrorResponse : Response
    { 
        public ErrorResponse(string message) {
            this.Success = false;
            this.Message = message;
        }
    }
}
