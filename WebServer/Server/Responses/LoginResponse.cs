﻿namespace Server.Responses
{
    public class LoginResponse : Response
    {
        public string JWT { get; set; } = "";
    }
}
