﻿using Logic.Entities;
using Logic.Enumerations.Status;

namespace Server.Responses
{
    public class FriendInfoResponse : Response
    {
        public int FriendUserId { get; set; }
        public string LoginName { get; set; }
        public string DisplayName { get; set; }
        public AssociationStatus AssociationStatus { get; set; }
        public int ChatId { get; set; }

        public FriendInfoResponse(User user, Association assoc)
        {
            this.FriendUserId = user.Id;
            this.LoginName = user.LoginName;
            this.DisplayName = user.DisplayName;
            this.AssociationStatus = assoc.Status;
            this.ChatId = 0;
        }

        public FriendInfoResponse(User user, Association assoc, Chat privateChat)
        {
            this.FriendUserId = user.Id;
            this.LoginName = user.LoginName;
            this.DisplayName = user.DisplayName;
            this.AssociationStatus = assoc.Status;
            this.ChatId = privateChat.Id;
        }
    }
}
