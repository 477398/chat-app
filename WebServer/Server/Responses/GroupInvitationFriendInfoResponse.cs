﻿using Logic.Entities;
using Logic.Enumerations.Status;

namespace Server.Responses
{
    public class GroupInvitationFriendInfoResponse : Response
    {
        public int FriendUserId { get; set; }
        public string FriendDisplayName { get; set; } = "";
        public ChatUserStatus InvitationStatus { get; set; }

        public GroupInvitationFriendInfoResponse(ChatUser cu, User user) 
        { 
            this.FriendUserId = user.Id;
            this.FriendDisplayName = user.DisplayName;
            this.InvitationStatus = cu.Status;
        }

    }
}
