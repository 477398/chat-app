﻿using Logic.Entities;

namespace Server.Responses
{
    public class GetGroupChatInvitationResponse
    {
        public int ChatId { get; set; }
        public string ChatDisplayName { get; set; } = "";

        public GetGroupChatInvitationResponse(Chat chat) 
        {
            this.ChatId = chat.Id;
            this.ChatDisplayName = chat.DisplayName;
        }
    }
}
