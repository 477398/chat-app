﻿using Logic.Entities;

namespace Server.Responses
{
    public class GroupChatInfoResponse : Response
    {
        public int ChatId { get; set; }
        public string ChatDisplayName { get; set; } = "";

        public GroupChatInfoResponse(Chat chat)
        {
            this.ChatId = chat.Id;
            this.ChatDisplayName = chat.DisplayName;
        }
    }
}
