﻿using Logic.Entities;
using Logic.Enumerations.Status;
using Server.DTOs;

namespace Server.Responses
{
    public class GetChatInfoResponse : Response
    {
        public int ChatId { get; set; }
        public string ChatDisplayName { get; set; } = "";
        public string ChatDescription { get; set; } = "";
        public ChatUserStatus MyChatRole { get; set; }
        public List<MessageDTO> Messages { get; set; }
        public List<ChatUserDTO> ChatUsers { get; set; }

        public GetChatInfoResponse(Chat chat, ChatUser myChatUser, List<MessageDTO> messageDTOs, List<ChatUserDTO> chatUserDTOs)
        {
            this.ChatId = chat.Id;
            this.ChatDisplayName = chat.DisplayName;
            this.ChatDescription = chat.Description;
            this.MyChatRole = myChatUser.Status;
            this.Messages = messageDTOs;
            this.ChatUsers = chatUserDTOs;
        }
    }
}
