﻿using Logic.Entities;
using System.Diagnostics;

namespace Server.Responses
{
	public class GetMyProfileResponse
	{
		public int UserId { get; set; }
		public string LoginName { get; set; } = string.Empty;
		public string DisplayName { get; set; } = string.Empty;
		public string Email { get; set; } = string.Empty;

		public GetMyProfileResponse(User user)
		{
			this.UserId = user.Id;
			this.LoginName = user.LoginName;
			this.DisplayName = user.DisplayName;
			this.Email = user.Email;
		}
	}
}
