
using CloudinaryDotNet;
using Data.Contexts;
using Data.Repositories;
using Logic.Interfaces.IRepositories;
using Logic.Interfaces.IServices;
using Logic.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using System.Text;

namespace Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
			var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddScoped<IChatRepository, ChatRepository>();
            builder.Services.AddScoped<IChatUserRepository, ChatUserRepository>();
            builder.Services.AddScoped<IMessageRepository, MessageRepository>();
            builder.Services.AddScoped<IUserRepository, UserRepository>();
            builder.Services.AddScoped<IAssocRepository, AssocRepository>();
            builder.Services.AddScoped<IImageRepository, ImageRepository>();
            builder.Services.AddScoped<ICloudinary, Cloudinary>();
            builder.Services.AddSingleton(new Account("ddi8u0lxw", "593676164844231", "DjV48LUGSjLBtjS5OTBy0J08_rA")); // Cloudinary Account

            builder.Services.AddScoped<IAuthService, AuthService>();
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddScoped<IAssocService, AssocService>();
            builder.Services.AddScoped<IChatService, ChatService>();
            builder.Services.AddScoped<IMessageService, MessageService>();

            builder.Services.AddControllers();

            // JWT Bearer Token
            builder.Services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    string key = builder.Configuration["JWT:Key"] ?? throw new NullReferenceException();
					options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key)),
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = false,
                    };
                });
            builder.Services.AddAuthorization();

            // CORS
            builder.Services.AddCors(options => options.AddPolicy(name: "FrontEndUI",
                policy =>
                {
                    policy.WithOrigins("https://localhost:5173").AllowAnyMethod().AllowAnyHeader();
                }
            ));

            // Context creation
            MySqlConnectionStringBuilder stringBld = new MySqlConnectionStringBuilder();
            stringBld.Server = builder.Configuration["DatabaseCreds:Server"];
            stringBld.Port = uint.Parse(builder.Configuration["DatabaseCreds:Port"] ?? throw new NullReferenceException());
            stringBld.UserID = builder.Configuration["DatabaseCreds:UserId"];
            stringBld.Password = builder.Configuration["DatabaseCreds:Password"];
            stringBld.Database = builder.Configuration["DatabaseCreds:Database"];
            builder.Services.AddDbContext<PlatformContext>(options => options.UseMySQL(stringBld.ConnectionString));

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            // Configure the HTTP request pipeline.
            if (true /*app.Environment.IsDevelopment()*/)
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCors("FrontEndUI");

            app.MapControllers();

            app.MapFallbackToFile("/index.html");

            app.Run();
        }
    }
}
