﻿namespace Server.Requests
{
    public class ChangeChatNameRequest
    {
        public string NewName { get; set; } = "";
    }
}