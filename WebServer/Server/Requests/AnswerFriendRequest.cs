﻿namespace Server.Requests
{
    public class AnswerFriendRequest
    {
        public int SenderUserId { get; set; }
        public bool AcceptFriendship { get; set; }
    }
}
