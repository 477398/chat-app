﻿namespace Server.Requests
{
    public class AnswerGroupChatInvitationRequest
    {
        public int ChatId { get; set; }
        public bool Accept { get; set; }
    }
}
