﻿namespace Server.Requests
{
    public class CreateMessageRequest
    {
        public int ChatId { get; set; }
        public string Content { get; set; } = "";
    }
}
