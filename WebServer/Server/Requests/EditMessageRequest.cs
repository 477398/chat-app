﻿namespace Server.Requests
{
    public class EditMessageRequest
    {
        public int MsgId { get; set; }
        public string Content { get; set; } = "";
    }
}
