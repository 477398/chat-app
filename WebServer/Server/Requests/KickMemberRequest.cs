﻿namespace Server.Requests
{
    public class KickMemberRequest
    {
        public int ChatId { get; set; }
        public int KickUserId { get; set; }
    }
}
