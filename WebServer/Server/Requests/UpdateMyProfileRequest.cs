﻿namespace Server.Requests
{
	public class UpdateMyProfileRequest
	{
		public string LoginName { get; set; } = string.Empty;
		public string DisplayName { get; set; } = string.Empty;
		public string Email { get; set; } = string.Empty;
	}
}
