﻿namespace Server.Requests
{
    public class HideMessageRequest
    {
        public int MsgId { get; set; }
    }
}
