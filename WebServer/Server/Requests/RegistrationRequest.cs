﻿using Microsoft.AspNetCore.Mvc;

namespace Server.Requests
{
    public class RegistrationRequest
    {
        public string LoginName { get; set; } = "";
        public string UnhashedPassword { get; set; } = "";
        public string Email { get; set; } = "";
        public string DisplayName { get; set; } = "";
        public string UnhashedPasswordConfirm { get; set; } = "";
    }
}
