﻿namespace Server.Requests
{
    public class UnfriendRequest
    {
        public int FriendUserId { get; set; }
    }
}
