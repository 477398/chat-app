﻿namespace Server.Requests
{
    public class SendGroupChatInvitationRequest
    {
        public int friendUserId {  get; set; }
        public int chatId { get; set; }
    }
}
