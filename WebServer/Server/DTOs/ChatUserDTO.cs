﻿using Logic.Entities;
using Logic.Enumerations.Status;
using Server.Responses;

namespace Server.DTOs
{
    public class ChatUserDTO : Response
    {
        public int UserId { get; set; }
        public string UserDisplayName { get; set; } = "";
        public ChatUserStatus ChatRole { get; set; }

        public ChatUserDTO(User user, ChatUser cu)
        {
            UserId = user.Id;
            UserDisplayName = user.DisplayName;
            ChatRole = cu.Status;
        }
    }
}
