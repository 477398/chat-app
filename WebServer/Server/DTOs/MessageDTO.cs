﻿using Logic.Entities;
using Logic.Enumerations.Status;

namespace Server.DTOs
{
    public class MessageDTO
    {
        public int MsgId { get; set; }
        public string Content { get; set; } = "";
        public string UserDisplayName { get; set; } = "";
        public int SenderUserId { get; set; }
        public ChatUserStatus SenderChatRole { get; set; }

        public MessageDTO(Message msg, User user, ChatUser chatUser)
        {
            MsgId = msg.Id;
            Content = msg.Content;
            UserDisplayName = user.DisplayName;
            SenderUserId = user.Id;
            SenderChatRole = chatUser.Status;
        }
    }
}