﻿using Logic.Exceptions;
using Logic.Interfaces.IServices;
using Logic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Requests;
namespace Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : Controller
    {

        private readonly IAuthService _authService;
        private readonly IChatService _chatService;
        private readonly IMessageService _msgService;

        public MessageController(IAuthService authService, IChatService chatService, IMessageService msgService)
        {
            this._authService = authService;
            this._chatService = chatService;
            this._msgService = msgService;
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateMessage([FromBody] CreateMessageRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                this._chatService.ValidateUserIsAChatMember(request.ChatId, userId);
                this._msgService.CreateMessage(userId, request.ChatId, request.Content);
                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (ChatException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpPut("Delete")]
        public IActionResult HideMessage([FromBody] HideMessageRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                if (this._msgService.IsAllowedToHideMessage(request.MsgId, userId))
                {
                    this._msgService.HideMessage(request.MsgId);
                }
                else
                {
                    return BadRequest();
                }

                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (MessageException)
            {
                return BadRequest();
            }
        }

		[Authorize]
		[HttpPut("Edit")]
        public IActionResult EditMessage([FromBody] EditMessageRequest request)
        {
            try
            {
				int userId = this._authService.GetUserIdFromClaimPrincipal(User);

				if (this._msgService.IsAllowedToEditMessage(request.MsgId, userId))
                {
					this._msgService.EditMessage(request.MsgId, request.Content);
				}
                else
                {
                    return BadRequest();
                }

                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
			catch (MessageException)
			{
				return BadRequest();
			}
		}
    }
}
