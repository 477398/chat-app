﻿using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IServices;
using Logic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.Ocsp;
using Server.Requests;
using Server.Responses;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AssocController : Controller
    {

        private readonly IUserService _userService;
        private readonly IAssocService _assocService;
        private readonly IAuthService _authService;
        private readonly IChatService _chatService;

        public AssocController(IUserService userService, IAssocService assocService, IAuthService authService, IChatService chatService)
        {
            this._userService = userService;
            this._assocService = assocService;
            this._authService = authService;
            this._chatService = chatService;
        }

        [Authorize]
        [HttpGet("Others")]
        public IActionResult GetOtherUsersByPartialLoginName(string partialDisplayName)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                List<User> otherUsers = this._assocService.GetOtherUsersByPartialDisplayName(userId, partialDisplayName);
                List<FriendInfoResponse> friendInfoResponse = new List<FriendInfoResponse>();
                foreach (User otherUser in otherUsers)
                {
                    Association assoc = this._assocService.GetAssociationByUserIds(userId, otherUser.Id);
                    friendInfoResponse.Add(new FriendInfoResponse(otherUser, assoc));
                }
                return Ok(friendInfoResponse);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet("Incomings")]
        public IActionResult GetIncomingFriendRequests()
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                List<Association> assocs = this._assocService.GetIncomingAssociationsByPrimaryUserId(userId);
                List<FriendInfoResponse> response = new List<FriendInfoResponse>();
                foreach (Association assoc in assocs)
                {
                    User otherUser = this._userService.GetUserById(assoc.SecondaryUserId);
                    response.Add(new FriendInfoResponse(otherUser, assoc));
                }
                return Ok(response);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet("Outgoings")]
        public IActionResult GetOutgoingFriendRequests()
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                List<Association> assocs = this._assocService.GetOutgoingAssociationsByPrimaryUserId(userId);
                List<FriendInfoResponse> response = new List<FriendInfoResponse>();
                foreach (Association assoc in assocs)
                {
                    User otherUser = this._userService.GetUserById(assoc.SecondaryUserId);
                    response.Add(new FriendInfoResponse(otherUser, assoc));
                }
                return Ok(response);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpPut("Befriend")]
        public IActionResult SendFriendRequest([FromBody] BefriendRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                this._assocService.CreateFriendRequest(userId, request.ReceiverUserId);

                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpPut("Answer")]
        public IActionResult AnswerFriendRequest([FromBody] AnswerFriendRequest request)
        {
            try
            {   
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                this._assocService.AnswerFriendRequest(userId, request.SenderUserId, request.AcceptFriendship);
                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpGet("Friends")]
        public IActionResult GetFriendsByPartialDisplayName([FromQuery] string? partialDisplayName)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                List<Association> assocs = this._assocService.GetFriendsByPrimaryUserId(userId);
                List<FriendInfoResponse> response = new List<FriendInfoResponse>();
                foreach (Association assoc in assocs)
                {
                    User otherUser = this._userService.GetUserById(assoc.SecondaryUserId);
                    if (partialDisplayName == null || otherUser.DisplayName.ToLower().Contains(partialDisplayName.ToLower()))
                    {
                        Chat privateChat = this._chatService.GetPrivateChatByUserIds(userId, otherUser.Id);
                        response.Add(new FriendInfoResponse(otherUser, assoc, privateChat));
                    }
                }
                return Ok(response);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpPut("Unfriend")]
        public IActionResult Unfriend([FromBody] UnfriendRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
                this._assocService.Unfriend(userId, request.FriendUserId);
                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (AssociationException)
            {
                return BadRequest();
            }
        }
    }
}
