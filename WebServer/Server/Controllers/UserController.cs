﻿using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IServices;
using Logic.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Server.Requests;
using Server.Responses;

namespace Server.Controllers
{
	[ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public UserController(IUserService userService, IAuthService authService)
        {
            this._userService = userService;
            this._authService = authService;
        }

        [HttpGet("Login")]
        public IActionResult LoginUser([FromQuery] string loginNameOrEmail, [FromQuery] string unhashedPassword )
        {
            // Trimming
            loginNameOrEmail = loginNameOrEmail.Trim();
            unhashedPassword = unhashedPassword.Trim();

            try
            {
                string hashedPassword = PasswordHasher.Hash(unhashedPassword);
                User user;
                if (EmailValidator.IsValid(loginNameOrEmail))
                {
                    user = this._authService.AuthenticateUserByEmail(loginNameOrEmail, hashedPassword);
                }
                else
                {
                    user = this._authService.AuthenticateUserByLoginName(loginNameOrEmail, hashedPassword);
                }
                LoginResponse response = new LoginResponse { JWT = this._authService.GenerateJWT(user.Id) };
                return Ok(response);
            }
            catch (AuthException ex)
            {
                return Unauthorized(new ErrorResponse(ex.Message));
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] RegistrationRequest request)
        {
            try
            {
                User user = this._userService.CreateUser(request.LoginName, request.DisplayName, request.Email, request.UnhashedPassword, request.UnhashedPasswordConfirm);
                await this._userService.UploadDefaultPfp(user.Id);
                LoginResponse response = new LoginResponse { JWT = this._authService.GenerateJWT(user.Id) };
                return Ok(response);
            }
            catch (UserException ex)
            {
                return BadRequest(new ErrorResponse(ex.Message));
            }
        }

        [Authorize]
        [HttpGet("MyProfile")]
        public IActionResult GetMyProfile()
        {
			try
			{
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                User user = this._userService.GetUserById(userId);

				return Ok(new GetMyProfileResponse(user));
			}
			catch (AuthException)
			{
				return Unauthorized();
			}
		}

        [Authorize]
		[HttpPost("Pfp")]
		public async Task<IActionResult> UploadPfp(IFormFile file)
		{
			if (file == null) return BadRequest();

			try
			{
				int userId = this._authService.GetUserIdFromClaimPrincipal(User);

				await this._userService.UploadPfp(file, userId);

				return Ok();
			}
			catch (AuthException)
			{
				return Unauthorized();
			}
		}

        [Authorize]
        [HttpPut("MyProfile")]
        public IActionResult UpdateMyProfile([FromBody] UpdateMyProfileRequest request)
        {
			try
			{
				int userId = this._authService.GetUserIdFromClaimPrincipal(User);

				User user = this._userService.GetUserById(userId);
                user.DisplayName = request.DisplayName;
                user.LoginName = request.LoginName;
                user.Email = request.Email;

                this._userService.UpdateUser(user);

				return Ok();
			}
			catch (AuthException)
			{
				return Unauthorized();
			}
            catch (UserException)
            {
                return BadRequest();
            }
		}
	}
}
