﻿using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IServices;
using Logic.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Asn1.Crmf;
using Org.BouncyCastle.Asn1.Ocsp;
using Server.DTOs;
using Server.Requests;
using Server.Responses;
using System.Security.Claims;

namespace Server.Controllers
{
    [ApiController]
    [Route("Chat")]
    public class ChatController : Controller
    {
        private readonly IChatService _chatService;
        private readonly IAuthService _authService;
        private readonly IUserService _userService;
        private readonly IMessageService _msgService;
        private readonly IAssocService _assocService;

        public ChatController(IChatService chatService, IAuthService authService, IUserService userService, IMessageService msgService, IAssocService assocService)
        {
            this._chatService = chatService;
            this._authService = authService;
            this._userService = userService;
            this._msgService = msgService;
            this._assocService = assocService;
        }

        [Authorize]
		[HttpGet("{chatId}")]
		public IActionResult GetChat(int chatId)
		{
			try
			{
				int userId = this._authService.GetUserIdFromClaimPrincipal(User);

				this._chatService.ValidateUserIsAChatMember(chatId, userId);

                // Get basic chat info and the requesters info
				Chat chat = this._chatService.GetChatById(chatId);
				ChatUser myChatUser = this._chatService.GetChatUserByChatIdAndUserId(chatId, userId);

				// Get all the messages in chat
				List<Message> messages = this._msgService.GetAllVisibleMessagesByChatId(chatId);
				List<MessageDTO> messageDTOs = new List<MessageDTO>();
				foreach (Message msg in messages)
				{
					User user = this._userService.GetUserById(msg.UserId);
					ChatUser chatUser = this._chatService.GetChatUserByChatIdAndUserId(chatId, user.Id);
					messageDTOs.Add(new MessageDTO(msg, user, chatUser));
				}

				// Get all members in chat
				List<ChatUser> chatUsers = this._chatService.GetChatMembers(chatId);
				List<ChatUserDTO> chatUserDTOs = new List<ChatUserDTO>();
                foreach (ChatUser chatUser in chatUsers)
                {
                    User user = this._userService.GetUserById(chatUser.UserId);
                    chatUserDTOs.Add(new ChatUserDTO(user, chatUser));
                }

                // Respond
				GetChatInfoResponse response = new GetChatInfoResponse(chat, myChatUser, messageDTOs, chatUserDTOs);
				return Ok(response);

			}
			catch (AuthException)
			{
				return Unauthorized();
			}
			catch (ChatException)
			{
				return BadRequest();
			}
		}

        [Authorize]
		[HttpPost("Group")]
        public IActionResult CreateGroupChat()
        {
            {
                try
                {
                    int founderUserId = this._authService.GetUserIdFromClaimPrincipal(User);

                    this._chatService.CreateGroupChat(founderUserId);
                    
                    return Ok();
                }
                catch (AuthException)
                {
                    return Unauthorized();
                }
            }
        }

        [Authorize]
        [HttpGet("Groups")]
        public IActionResult GetGroupChats([FromQuery] string? partialDisplayName)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                List<Chat> chats = this._chatService.GetGroupChatsWithUserInIt(userId);

                if (partialDisplayName != null)
                {
                    chats = chats.Where(c => c.DisplayName.ToLower().Contains(partialDisplayName.ToLower())).ToList();
                }

                List<GroupChatInfoResponse> response = chats.Select(c => new GroupChatInfoResponse(c)).ToList();

                return Ok(response);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
        }

        [Authorize]
        [HttpPut("Group/Kick")]
        public IActionResult KickMember([FromBody] KickMemberRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                this._chatService.ValidateUserIsAChatMember(request.ChatId, userId);

                if (this._chatService.IsAllowedToKick(request.ChatId, userId, request.KickUserId)) {
                    this._chatService.LeaveGroupChat(request.ChatId, request.KickUserId);
					return Ok();
				}
                else
                {
                    return BadRequest();
                }
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (ChatException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpGet("Group/Invite/Friends")]
        public IActionResult GetFriendsForGroupInvitation([FromQuery] int chatId, [FromQuery] string? partialDisplayName)
        {
            try
            {
                // Validation
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);
				this._chatService.ValidateUserIsAChatMember(chatId, userId);

				// Get this user's friends
				List<Association> assocs = this._assocService.GetFriendsByPrimaryUserId(userId);
                List<User> friends = assocs.Select(x => this._userService.GetUserById(x.SecondaryUserId)).ToList();

                // Filter names if needed
                if (partialDisplayName != null) {
                    friends = friends.Where(x => x.DisplayName.ToLower().Contains(partialDisplayName.ToLower())).ToList();
                }

                // Generate response + getting the friend's association status to the group chat
                List<GroupInvitationFriendInfoResponse> response = new List<GroupInvitationFriendInfoResponse>();
                foreach (User friend in friends)
                {
                    ChatUser friendCu = this._chatService.GetChatUserByChatIdAndUserId(chatId, friend.Id);
                    response.Add(new GroupInvitationFriendInfoResponse(friendCu, friend));
                }

                return Ok(response);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (ChatException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpPut("Group/Invite")]
        public IActionResult SendGroupChatInvitationToFriend([FromBody] SendGroupChatInvitationRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                // Validation: sender must be a group member to be able to invite friends
                this._chatService.ValidateUserIsAChatMember(request.chatId, userId);

                this._chatService.SendGroupChatInvitation(request.chatId, request.friendUserId);
                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (ChatException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpGet("Group/Invites")]
        public IActionResult GetGroupChatInvitations()
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                List<ChatUser> invitations = this._chatService.GetGroupInvitationsByUserId(userId);
                List<GetGroupChatInvitationResponse> response = new List<GetGroupChatInvitationResponse>();
                foreach (ChatUser inv in invitations)
                {
                    Chat chat = this._chatService.GetChatById(inv.ChatId);
                    response.Add(new GetGroupChatInvitationResponse(chat));
                }

                return Ok(response);
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (ChatException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpPut("Group/Invite/Answer")]
        public IActionResult AnswerGroupChatInvitation([FromBody] AnswerGroupChatInvitationRequest request)
        {
            try
            {
                int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                this._chatService.AnswerGroupChatInvitation(request.ChatId, userId, request.Accept);

                return Ok();
            }
            catch (AuthException)
            {
                return Unauthorized();
            }
            catch (ChatException)
            {
                return BadRequest();
            }
        }

        [Authorize]
        [HttpPut("{chatId}/Name")]
        public IActionResult ChangeChatName(int chatId, [FromBody] ChangeChatNameRequest request)
        {
			try
			{
				int userId = this._authService.GetUserIdFromClaimPrincipal(User);

                if (this._chatService.IsAllowedToChangeChatName(chatId, userId))
                {
                    this._chatService.ChangeChatName(chatId, request.NewName);
                }
                else
                {
                    return BadRequest();
                }

				return Ok(new Response());
			}
			catch (AuthException)
			{
				return Unauthorized();
			}
			catch (ChatException e)
			{
				return BadRequest(new ErrorResponse(e.Message));
			}
		}
    }
}
