﻿using Microsoft.EntityFrameworkCore;
using Logic.Entities;

namespace Data.Contexts
{
    public class PlatformContext : DbContext
    {
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ChatUser> ChatUsers { get; set; }
        public DbSet<Association> Associations { get; set; }

        public PlatformContext(DbContextOptions<PlatformContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Overriding tables (e.g.: detecting singular-named tables instead of plural-named)
            modelBuilder.Entity<Chat>().ToTable("Chat");
            modelBuilder.Entity<Message>().ToTable("Message");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<ChatUser>().ToTable("ChatUser");
            modelBuilder.Entity<Association>().ToTable("Association");
        }
    }
}
