﻿using Logic.Entities;
using Data.Contexts;
using Logic.Interfaces.IRepositories;

namespace Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(PlatformContext context) : base(context) { }

        public User? GetUserByEmail(string email)
        {
            return this._dbSet.Where(x => x.Email == email).FirstOrDefault();
        }

        public User? GetUserByLoginName(string loginName)
        {
            return this._dbSet.Where(x => x.LoginName == loginName).FirstOrDefault();
        }

        public List<User> GetAllByPartialDisplayName(string partialDisplayName)
        {
            return this._dbSet.Where(x => x.DisplayName.ToLower().Contains(partialDisplayName.ToLower())).ToList();
        }
    }
}
