﻿using Logic.Entities;
using Data.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using Logic.Enumerations;
using Logic.Interfaces.IRepositories;

namespace Data.Repositories
{
    public class ChatRepository : Repository<Chat>, IChatRepository
    {
        public ChatRepository(PlatformContext context) : base(context) {}

        public List<Chat> GetAllPrivateChats()
        {
            return this._dbSet.Where(x => x.Type == ChatType.Private).ToList();
        }

        public List<Chat> GetAllGroupChats()
        {
            return this._dbSet.Where(x => x.Type == ChatType.Group).ToList();
        }
    }
}
