﻿using Logic.Entities;
using Logic.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Contexts;

namespace Data.Repositories
{
    public class Repository<T> where T : Entity
    {
        protected PlatformContext _context;
        protected DbSet<T> _dbSet;

        public Repository(PlatformContext context)
        {
            this._context = context;
            this._dbSet = context.Set<T>();
        }

        public void Add(T entity)
        {
            entity.CreationDate = DateTime.Now;
            entity.ModifiedDate = DateTime.Now;
            this._dbSet.Add(entity);
            this._context.SaveChanges();
        }

        public void Delete(T entity)
        {
            this._dbSet.Remove(entity);
            this._context.SaveChanges();
        }

        public void Update(T entity)
        {
            entity.ModifiedDate = DateTime.Now;
            this._dbSet.Update(entity);
            this._context.SaveChanges();
        }

        public T? GetById(int id)
        {
            return this._dbSet.Where(x => x.Id == id).FirstOrDefault();
        }

    }
}
