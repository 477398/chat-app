﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Logic.Entities;
using Logic.Interfaces.IRepositories;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Repositories
{
	public class ImageRepository : IImageRepository
	{
		private readonly ICloudinary _cldy;

		public ImageRepository(ICloudinary cldy)
		{
			this._cldy = cldy;
		}

		public async Task UploadImage(IFormFile file, string publicId)
		{
			ImageUploadParams parameters = new ImageUploadParams
			{
				File = new FileDescription(file.FileName, file.OpenReadStream()),
				PublicId = publicId,
				Invalidate = true,
			};

			await this._cldy.UploadAsync(parameters);
		}

		public async Task UploadImage(string imagePath, string fileName, string publicId)
		{
			using (FileStream stream = new FileStream(imagePath, FileMode.Open))
			{
				ImageUploadParams parameters = new ImageUploadParams
				{
					File = new FileDescription(fileName, stream),
					PublicId = publicId,
					Invalidate = true,
				};

				await this._cldy.UploadAsync(parameters);
			}
		}

	}
}
