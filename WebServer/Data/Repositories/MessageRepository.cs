﻿using Logic.Entities;
using Data.Contexts;
using Microsoft.EntityFrameworkCore;
using Logic.Enumerations.Status;
using Logic.Interfaces.IRepositories;

namespace Data.Repositories
{
    public class MessageRepository : Repository<Message>, IMessageRepository
    {
        public MessageRepository(PlatformContext context) : base(context) { }

        public List<Message> GetAllVisiblesByChatId(int chatId)
        {
            return this._dbSet.Where(x => x.ChatId == chatId && x.Status >= MessageStatus.Displaying).ToList();
        }

        public List<Message>GetAllByUserId(int userId)
        {
            return this._dbSet.Where(x => x.UserId == userId).ToList();
        }
    }
}
