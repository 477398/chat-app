﻿using Data.Contexts;
using Logic.Entities;
using Logic.Enumerations.Status;
using Logic.Interfaces.IRepositories;

namespace Data.Repositories
{
    public class AssocRepository : Repository<Association>, IAssocRepository
    {
        public AssocRepository(PlatformContext context) : base(context) { }

        public Association? GetByUserIds(int primaryUserId, int secondaryUserId)
        {
            return this._dbSet.Where(x =>
                x.PrimaryUserId == primaryUserId
                && x.SecondaryUserId == secondaryUserId)
                    .FirstOrDefault();
        }

        public List<Association> GetIncomingsByPrimaryUserId(int primaryUserId)
        {
            return this._dbSet.Where(x =>
                x.PrimaryUserId == primaryUserId
                && x.Status == AssociationStatus.PendingReceiver)
                    .ToList();
        }

        public List<Association> GetOutgoingsByPrimaryUserId(int primaryUserId)
        {
            return this._dbSet.Where(x =>
                x.PrimaryUserId == primaryUserId
                && x.Status == AssociationStatus.PendingSender)
                    .ToList();
        }

        public List<Association> GetFriendsByPrimaryUserId(int primaryUserId)
        {
            return this._dbSet.Where(x =>
                x.PrimaryUserId == primaryUserId
                && x.Status == AssociationStatus.Consociated)
                    .ToList();
        }
    }
}
