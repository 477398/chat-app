﻿using Logic.Entities;
using Data.Contexts;
using Logic.Enumerations.Status;
using Logic.Interfaces.IRepositories;

namespace Data.Repositories
{
    public class ChatUserRepository : Repository<ChatUser>, IChatUserRepository
    {
        public ChatUserRepository(PlatformContext context) : base(context) { }

        public List<ChatUser> GetAllMembersByChatId(int chatId)
        {
            return this._dbSet.Where(x => x.ChatId == chatId && x.Status >= ChatUserStatus.JoinedAsMember).ToList();
        }

        public ChatUser? GetByChatIdAndUserId(int chatId, int userId)
        {
            return this._dbSet.Where(x => x.UserId == userId && x.ChatId == chatId).FirstOrDefault();
        }

        public List<ChatUser> GetAllPendingByUserId(int userId)
        {
            return this._dbSet.Where(x => x.UserId == userId && x.Status == ChatUserStatus.Pending).ToList();
        }
    }
}
