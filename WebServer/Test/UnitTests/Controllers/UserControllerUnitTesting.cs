﻿using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IServices;
using Logic.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Server.Controllers;
using Server.Requests;
using Server.Responses;
using System.Security.Claims;

namespace Test.UnitTests.Controllers
{
    public class UserControllerUnitTesting
    {
        private readonly Mock<IUserService> _mockUserService;
        private readonly Mock<IAuthService> _mockAuthService;
        private readonly UserController _controller;

        public UserControllerUnitTesting()
        {
            _mockUserService = new Mock<IUserService>();
            _mockAuthService = new Mock<IAuthService>();
            _controller = new UserController(_mockUserService.Object, _mockAuthService.Object);
        }

        [Fact]
        public void LoginUser_ReturnsOkResult_ByEmail()
        {
            // Arrange
            string loginNameOrEmail = "test@example.com";
            string unhashedPassword = "password123";
            string hashedPassword = PasswordHasher.Hash(unhashedPassword);
            User user = new User
            {
                Id = 1
            };

            _mockAuthService.Setup(s => s.AuthenticateUserByEmail(loginNameOrEmail, hashedPassword)).Returns(user);
            _mockAuthService.Setup(s => s.GenerateJWT(user.Id)).Returns("JWT");

            // Act
            IActionResult result = _controller.LoginUser(loginNameOrEmail, unhashedPassword);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            LoginResponse response = Assert.IsType<LoginResponse>(okResult.Value);
            Assert.Equal("JWT", response.JWT);
            _mockAuthService.Verify(s => s.AuthenticateUserByEmail(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _mockAuthService.Verify(s => s.AuthenticateUserByLoginName(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _mockAuthService.Verify(s => s.GenerateJWT(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void LoginUser_ReturnsOkResult_ByLoginName()
        {
            // Arrange
            string loginNameOrEmail = "loginName123";
            string unhashedPassword = "password123";
            string hashedPassword = PasswordHasher.Hash(unhashedPassword);
            User user = new User
            {
                Id = 1
            };

            _mockAuthService.Setup(s => s.AuthenticateUserByLoginName(loginNameOrEmail, hashedPassword)).Returns(user);
            _mockAuthService.Setup(s => s.GenerateJWT(user.Id)).Returns("JWT");

            // Act
            IActionResult result = _controller.LoginUser(loginNameOrEmail, unhashedPassword);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            LoginResponse response = Assert.IsType<LoginResponse>(okResult.Value);
            Assert.Equal("JWT", response.JWT);
            _mockAuthService.Verify(s => s.AuthenticateUserByEmail(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _mockAuthService.Verify(s => s.AuthenticateUserByLoginName(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _mockAuthService.Verify(s => s.GenerateJWT(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void LoginUser_ReturnsUnauthorizedResult_OnAuthException()
        {
            // Arrange
            string loginNameOrEmail = "test@example.com";
            string unhashedPassword = "password123";
            string hashedPassword = PasswordHasher.Hash(unhashedPassword);

            _mockAuthService.Setup(s => s.AuthenticateUserByEmail(loginNameOrEmail, hashedPassword)).Throws(new AuthException());

            // Act
            IActionResult result = _controller.LoginUser(loginNameOrEmail, unhashedPassword);

            // Assert
            UnauthorizedObjectResult unauthorizedResult = Assert.IsType<UnauthorizedObjectResult>(result);
            Assert.IsType<ErrorResponse>(unauthorizedResult.Value);
            _mockAuthService.Verify(s => s.AuthenticateUserByLoginName(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _mockAuthService.Verify(s => s.AuthenticateUserByEmail(loginNameOrEmail, hashedPassword), Times.Once);
        }

        [Fact]
        public async Task CreateUser_ReturnsOkResult()
        {
            // Arrange
            RegistrationRequest request = new RegistrationRequest
            {
                LoginName = "testUser",
                DisplayName = "Test User",
                Email = "test@example.com",
                UnhashedPassword = "password123",
                UnhashedPasswordConfirm = "password123"
            };
            User user = new User
            {
                Id = 1
            };

            _mockUserService.Setup(s => s.CreateUser(request.LoginName, request.DisplayName, request.Email, request.UnhashedPassword, request.UnhashedPasswordConfirm)).Returns(user);
            _mockAuthService.Setup(s => s.GenerateJWT(user.Id)).Returns("JWT");

            // Act
            IActionResult result = await _controller.CreateUser(request);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            LoginResponse response = Assert.IsType<LoginResponse>(okResult.Value);
            Assert.Equal("JWT", response.JWT);

            _mockUserService.Verify(s => s.CreateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
            _mockUserService.Verify(s => s.UploadDefaultPfp(It.IsAny<int>()), Times.Once);
            _mockAuthService.Verify(s => s.GenerateJWT(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task CreateUser_ReturnsBadRequestResult_OnUserException()
        {
            // Arrange
            RegistrationRequest request = new RegistrationRequest
            {
                LoginName = "testUser",
                DisplayName = "Test User",
                Email = "test@example.com",
                UnhashedPassword = "password123",
                UnhashedPasswordConfirm = "password123"
            };

            _mockUserService.Setup(s => s.CreateUser(request.LoginName, request.DisplayName, request.Email, request.UnhashedPassword, request.UnhashedPasswordConfirm)).Throws(new UserException());

            // Act
            IActionResult result = await _controller.CreateUser(request);

            // Assert
            BadRequestObjectResult badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<ErrorResponse>(badRequestResult.Value);
            _mockUserService.Verify(s => s.CreateUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void GetMyProfile_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            User user = new User
            {
                Id = userId,
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockUserService.Setup(s => s.GetUserById(userId)).Returns(user);

            // Act
            IActionResult result = _controller.GetMyProfile();

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<GetMyProfileResponse>(okResult.Value);

            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public async Task UploadPfp_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            Mock<IFormFile> mockFile = new Mock<IFormFile>();

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = await _controller.UploadPfp(mockFile.Object);

            // Assert
            Assert.IsType<OkResult>(result);

            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockUserService.Verify(s => s.UploadPfp(It.IsAny<IFormFile>(), It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void UpdateMyProfile_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            UpdateMyProfileRequest request = new UpdateMyProfileRequest
            {
                DisplayName = "Updated User",
                LoginName = "updatedUser",
                Email = "updated@example.com"
            };
            User user = new User
            {
                Id = userId
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockUserService.Setup(s => s.GetUserById(userId)).Returns(user);

            // Act
            IActionResult result = _controller.UpdateMyProfile(request);

            // Assert
            Assert.IsType<OkResult>(result);

            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.UpdateUser(It.IsAny<User>()), Times.Once);

            Assert.Equal(request.DisplayName, user.DisplayName);
            Assert.Equal(request.LoginName, user.LoginName);
            Assert.Equal(request.Email, user.Email);
        }

        [Fact]
        public void UpdateMyProfile_ReturnsBadRequestResult_OnUserException()
        {
            // Arrange
            int userId = 1;
            UpdateMyProfileRequest request = new UpdateMyProfileRequest
            {
                DisplayName = "Updated User",
                LoginName = "updatedUser",
                Email = "updated@example.com"
            };
            User user = new User
            {
                Id = userId
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockUserService.Setup(s => s.GetUserById(userId)).Returns(user);
            _mockUserService.Setup(s => s.UpdateUser(It.IsAny<User>())).Throws(new UserException());

            // Act
            IActionResult result = _controller.UpdateMyProfile(request);

            // Assert
            Assert.IsType<BadRequestResult>(result);

            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.UpdateUser(It.IsAny<User>()), Times.Once);
        }
    }
}
