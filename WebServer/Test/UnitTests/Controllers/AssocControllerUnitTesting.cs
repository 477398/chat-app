﻿using Moq;
using Logic.Entities;
using Logic.Exceptions;
using Logic.Services;
using System.Security.Claims;
using Server.Controllers;
using Microsoft.AspNetCore.Mvc;
using Server.Responses;
using Server.Requests;
using Logic.Interfaces.IServices;

namespace Test.UnitTests.Controllers
{
    public class AssocControllerUnitTesting
    {
        private readonly Mock<IUserService> _mockUserService;
        private readonly Mock<IAssocService> _mockAssocService;
        private readonly Mock<IAuthService> _mockAuthService;
        private readonly Mock<IChatService> _mockChatService;
        private readonly AssocController _controller;

        public AssocControllerUnitTesting()
        {
            _mockUserService = new Mock<IUserService>();
            _mockAssocService = new Mock<IAssocService>();
            _mockAuthService = new Mock<IAuthService>();
            _mockChatService = new Mock<IChatService>();
            _controller = new AssocController(_mockUserService.Object, _mockAssocService.Object, _mockAuthService.Object, _mockChatService.Object);
        }

        [Fact]
        public void GetOtherUsersByPartialLoginName_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int otherUserId = 2;
            string partialDisplayName = "test";
            List<User> otherUsers = new List<User>
            {
                new User {
                    Id = otherUserId,
                    DisplayName = "testUser"
                }
            };
            Association assoc = new Association
            {
                PrimaryUserId = userId,
                SecondaryUserId = otherUserId,
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockAssocService.Setup(s => s.GetOtherUsersByPartialDisplayName(userId, partialDisplayName)).Returns(otherUsers);
            _mockAssocService.Setup(s => s.GetAssociationByUserIds(userId, otherUserId)).Returns(assoc);

            // Act
            IActionResult result = _controller.GetOtherUsersByPartialLoginName(partialDisplayName);

            // Assert
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.GetOtherUsersByPartialDisplayName(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            _mockAssocService.Verify(s => s.GetAssociationByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            List<FriendInfoResponse> returnValue = Assert.IsType<List<FriendInfoResponse>>(okResult.Value);
            Assert.Single(returnValue);
        }

        [Fact]
        public void GetIncomingFriendRequests_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int otherUserId = 2;
            List<Association> assocs = new List<Association>
            {
                new Association
                {
                    PrimaryUserId = userId,
                    SecondaryUserId = 2,
                }
            };
            User otherUser = new User
            {
                Id = otherUserId,
                DisplayName = "testUser",
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockAssocService.Setup(s => s.GetIncomingAssociationsByPrimaryUserId(userId)).Returns(assocs);
            _mockUserService.Setup(s => s.GetUserById(otherUserId)).Returns(otherUser);

            // Act
            IActionResult result = _controller.GetIncomingFriendRequests();

            // Assert
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.GetIncomingAssociationsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            List<FriendInfoResponse> returnValue = Assert.IsType<List<FriendInfoResponse>>(okResult.Value);
            Assert.Single(returnValue);
        }

        [Fact]
        public void GetOutgoingFriendRequests_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int otherUserId = 2;
            List<Association> assocs = new List<Association>
            {
                new Association
                {
                    PrimaryUserId = userId,
                    SecondaryUserId = 2,
                }
            };
            User otherUser = new User
            {
                Id = otherUserId,
                DisplayName = "testUser"
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockAssocService.Setup(s => s.GetOutgoingAssociationsByPrimaryUserId(userId)).Returns(assocs);
            _mockUserService.Setup(s => s.GetUserById(otherUserId)).Returns(otherUser);

            // Act
            IActionResult result = _controller.GetOutgoingFriendRequests();

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.GetOutgoingAssociationsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
            List<FriendInfoResponse> returnValue = Assert.IsType<List<FriendInfoResponse>>(okResult.Value);
            Assert.Single(returnValue);
        }

        [Fact]
        public void SendFriendRequest_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            BefriendRequest request = new BefriendRequest
            {
                ReceiverUserId = 2
            };
            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = _controller.SendFriendRequest(request);

            // Assert
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.CreateFriendRequest(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void AnswerFriendRequest_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            AnswerFriendRequest request = new AnswerFriendRequest
            {
                SenderUserId = 2,
                AcceptFriendship = true,
            };
            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = _controller.AnswerFriendRequest(request);

            // Assert
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.AnswerFriendRequest(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>()), Times.Once);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void GetFriendsByPartialDisplayName_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int otherUserId = 2;
            string partialDisplayName = "test";
            List<Association> assocs = new List<Association>
            {
                new Association
                {
                    PrimaryUserId = userId,
                    SecondaryUserId = otherUserId,
                }
            };
            User otherUser = new User
            {
                Id = otherUserId,
                DisplayName = "testUser",
            };
            Chat privateChat = new Chat
            {
                Id = 1,
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockAssocService.Setup(s => s.GetFriendsByPrimaryUserId(userId)).Returns(assocs);
            _mockUserService.Setup(s => s.GetUserById(otherUserId)).Returns(otherUser);
            _mockChatService.Setup(s => s.GetPrivateChatByUserIds(userId, otherUserId)).Returns(privateChat);

            // Act
            IActionResult result = _controller.GetFriendsByPartialDisplayName(partialDisplayName);

            // Assert
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.GetFriendsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.GetPrivateChatByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            List<FriendInfoResponse> returnValue = Assert.IsType<List<FriendInfoResponse>>(okResult.Value);
            Assert.Single(returnValue);
        }

        [Fact]
        public void Unfriend_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            UnfriendRequest request = new UnfriendRequest
            {
                FriendUserId = 2
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = _controller.Unfriend(request);

            // Assert
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockAssocService.Verify(s => s.Unfriend(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void Unfriend_ReturnsUnauthorizedResult_WhenAuthExceptionThrown()
        {
            // Arrange
            UnfriendRequest request = new UnfriendRequest
            {
                FriendUserId = 2
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Throws(new AuthException());

            // Act
            IActionResult result = _controller.Unfriend(request);

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
        }

        [Fact]
        public void Unfriend_ReturnsBadRequestResult_WhenAssociationExceptionThrown()
        {
            // Arrange
            int userId = 1;
            UnfriendRequest request = new UnfriendRequest
            {
                FriendUserId = 2
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockAssocService.Setup(s => s.Unfriend(userId, request.FriendUserId)).Throws(new AssociationException());


            // Act
            IActionResult result = _controller.Unfriend(request);

            // Assert
            Assert.IsType<BadRequestResult>(result);
        }
    }
}
