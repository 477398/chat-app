﻿using Logic.Exceptions;
using Logic.Interfaces.IServices;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Server.Controllers;
using Server.Requests;
using System.Security.Claims;

namespace Test.UnitTests.Controllers
{
    public class MessageControllerUnitTesting
    {
        private readonly Mock<IAuthService> _mockAuthService;
        private readonly Mock<IChatService> _mockChatService;
        private readonly Mock<IMessageService> _mockMessageService;
        private readonly MessageController _controller;

        public MessageControllerUnitTesting()
        {
            _mockAuthService = new Mock<IAuthService>();
            _mockChatService = new Mock<IChatService>();
            _mockMessageService = new Mock<IMessageService>();
            _controller = new MessageController(_mockAuthService.Object, _mockChatService.Object, _mockMessageService.Object);
        }

        [Fact]
        public void CreateMessage_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            CreateMessageRequest request = new CreateMessageRequest
            {
                ChatId = 1,
                Content = "Test message"
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = _controller.CreateMessage(request);

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockMessageService.Verify(s => s.CreateMessage(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void CreateMessage_ReturnsBadRequestResult_OnChatException()
        {
            // Arrange
            int userId = 1;
            CreateMessageRequest request = new CreateMessageRequest
            {
                ChatId = 1,
                Content = "Test message"
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.ValidateUserIsAChatMember(request.ChatId, userId)).Throws<ChatException>();

            // Act
            IActionResult result = _controller.CreateMessage(request);

            // Assert
            Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockMessageService.Verify(s => s.CreateMessage(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void HideMessage_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            HideMessageRequest request = new HideMessageRequest
            {
                MsgId = 1
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockMessageService.Setup(s => s.IsAllowedToHideMessage(request.MsgId, userId)).Returns(true);

            // Act
            IActionResult result = _controller.HideMessage(request);

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockMessageService.Verify(s => s.IsAllowedToHideMessage(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockMessageService.Verify(s => s.HideMessage(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void HideMessage_ReturnsBadRequestResult_WhenNotAllowed()
        {
            // Arrange
            int userId = 1;
            HideMessageRequest request = new HideMessageRequest
            {
                MsgId = 1
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockMessageService.Setup(s => s.IsAllowedToHideMessage(request.MsgId, userId)).Returns(false);

            // Act
            IActionResult result = _controller.HideMessage(request);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockMessageService.Verify(s => s.IsAllowedToHideMessage(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockMessageService.Verify(s => s.HideMessage(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public void HideMessage_ReturnsBadRequestResult_OnMessageException()
        {
            // Arrange
            int userId = 1;
            HideMessageRequest request = new HideMessageRequest
            {
                MsgId = 1
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockMessageService.Setup(s => s.IsAllowedToHideMessage(request.MsgId, userId)).Throws<MessageException>();

            // Act
            IActionResult result = _controller.HideMessage(request);

            // Assert
            Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockMessageService.Verify(s => s.IsAllowedToHideMessage(request.MsgId, userId), Times.Once);
            _mockMessageService.Verify(s => s.HideMessage(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public void EditMessage_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            var request = new EditMessageRequest { MsgId = 1, Content = "Updated content" };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockMessageService.Setup(s => s.IsAllowedToEditMessage(request.MsgId, userId)).Returns(true);

            // Act
            IActionResult result = _controller.EditMessage(request);

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockMessageService.Verify(s => s.IsAllowedToEditMessage(request.MsgId, userId), Times.Once);
            _mockMessageService.Verify(s => s.EditMessage(request.MsgId, request.Content), Times.Once);
        }

        [Fact]
        public void EditMessage_ReturnsBadRequestResult_WhenNotAllowed()
        {
            // Arrange
            int userId = 1;
            var request = new EditMessageRequest { MsgId = 1, Content = "Updated content" };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockMessageService.Setup(s => s.IsAllowedToEditMessage(request.MsgId, userId)).Returns(false);

            // Act
            IActionResult result = _controller.EditMessage(request);

            // Assert
            Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockMessageService.Verify(s => s.IsAllowedToEditMessage(request.MsgId, userId), Times.Once);
            _mockMessageService.Verify(s => s.EditMessage(request.MsgId, request.Content), Times.Never);
        }

        [Fact]
        public void EditMessage_ReturnsBadRequestResult_OnMessageException()
        {
            // Arrange
            int userId = 1;
            var request = new EditMessageRequest { MsgId = 1, Content = "Updated content" };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockMessageService.Setup(s => s.IsAllowedToEditMessage(request.MsgId, userId)).Throws<MessageException>();

            // Act
            IActionResult result = _controller.EditMessage(request);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockMessageService.Verify(s => s.IsAllowedToEditMessage(request.MsgId, userId), Times.Once);
            _mockMessageService.Verify(s => s.EditMessage(It.IsAny<int>(), It.IsAny<string>()), Times.Never);
        }
    }
}
