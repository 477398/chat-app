﻿using Logic.Entities;
using Logic.Interfaces.IServices;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Server.Controllers;
using Server.Requests;
using Server.Responses;
using System.Security.Claims;

namespace Test.UnitTests.Controllers
{
    public class ChatControllerUnitTesting
    {
        private readonly Mock<IChatService> _mockChatService;
        private readonly Mock<IAuthService> _mockAuthService;
        private readonly Mock<IUserService> _mockUserService;
        private readonly Mock<IMessageService> _mockMessageService;
        private readonly Mock<IAssocService> _mockAssocService;
        private readonly ChatController _controller;

        public ChatControllerUnitTesting()
        {
            _mockChatService = new Mock<IChatService>();
            _mockAuthService = new Mock<IAuthService>();
            _mockUserService = new Mock<IUserService>();
            _mockMessageService = new Mock<IMessageService>();
            _mockAssocService = new Mock<IAssocService>();
            _controller = new ChatController(_mockChatService.Object, _mockAuthService.Object, _mockUserService.Object, _mockMessageService.Object, _mockAssocService.Object);
        }

        [Fact]
        public void GetChat_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int chatId = 1;

            Chat chat = new Chat
            {
                Id = chatId,
                DisplayName = "Test Chat",
            };
            ChatUser myChatUser = new ChatUser
            {
                ChatId = chatId,
                UserId = userId,
            };
            List<Message> messages = new List<Message>
            {
                new Message
                {
                    Id = 1,
                    ChatId = chatId,
                    UserId = userId,
                    Content = "Test Message",
                }
            };
            List<ChatUser> members = new List<ChatUser>
            {
                myChatUser
            };
            User user = new User
            {
                Id = userId,
                DisplayName = "Test User",
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.ValidateUserIsAChatMember(chatId, userId));
            _mockChatService.Setup(s => s.GetChatById(chatId)).Returns(chat);
            _mockChatService.Setup(s => s.GetChatUserByChatIdAndUserId(chatId, userId)).Returns(myChatUser);
            _mockMessageService.Setup(s => s.GetAllVisibleMessagesByChatId(chatId)).Returns(messages);
            _mockUserService.Setup(s => s.GetUserById(userId)).Returns(user);
            _mockChatService.Setup(s => s.GetChatMembers(chatId)).Returns(members);

            // Act
            IActionResult result = _controller.GetChat(chatId);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<GetChatInfoResponse>(okResult.Value);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.GetChatById(It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.GetChatUserByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            _mockMessageService.Verify(s => s.GetAllVisibleMessagesByChatId(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Exactly(2));
            _mockChatService.Verify(s => s.GetChatMembers(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void CreateGroupChat_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = _controller.CreateGroupChat();

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.CreateGroupChat(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void GetGroupChats_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            string partialDisplayName = "Test";
            List<Chat> groupChats = new List<Chat>
            {
                new Chat
                {
                    Id = 1,
                    DisplayName = "Test Group Chat",
                },
                new Chat
                {
                    Id = 2,
                    DisplayName = "This one should not appear in ReturnValue to pass the assert",
                },
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.GetGroupChatsWithUserInIt(userId)).Returns(groupChats);

            // Act
            IActionResult result = _controller.GetGroupChats(partialDisplayName);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            List<GroupChatInfoResponse> returnValue = Assert.IsType<List<GroupChatInfoResponse>>(okResult.Value);
            Assert.Single(returnValue);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.GetGroupChatsWithUserInIt(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void KickMember_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            KickMemberRequest request = new KickMemberRequest { ChatId = 1, KickUserId = 2 };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.ValidateUserIsAChatMember(request.ChatId, userId));
            _mockChatService.Setup(s => s.IsAllowedToKick(request.ChatId, userId, request.KickUserId)).Returns(true);

            // Act
            IActionResult result = _controller.KickMember(request);

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.IsAllowedToKick(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.LeaveGroupChat(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void KickMember_ReturnsBadRequestResult_WhenNotAllowedToKick()
        {
            // Arrange
            int userId = 1;
            KickMemberRequest request = new KickMemberRequest { ChatId = 1, KickUserId = 2 };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.ValidateUserIsAChatMember(request.ChatId, userId));
            _mockChatService.Setup(s => s.IsAllowedToKick(request.ChatId, userId, request.KickUserId)).Returns(false);

            // Act
            IActionResult result = _controller.KickMember(request);

            // Assert
            Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.IsAllowedToKick(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.LeaveGroupChat(It.IsAny<int>(), It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public void GetFriendsForGroupInvitation_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int otherUserId = 2;
            int chatId = 1;
            string partialDisplayName = "Test";

            List<Association> associations = new List<Association>
            {
                new Association
                {
                    PrimaryUserId = userId,
                    SecondaryUserId = 2,
                }
            };
            List<User> friends = new List<User>
            {
                new User
                {
                    Id = otherUserId,
                    DisplayName = "Test Friend"
                },
            };
            ChatUser otherChatUser = new ChatUser
            {
                ChatId = chatId,
                UserId = otherUserId,
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.ValidateUserIsAChatMember(userId, userId));
            _mockAssocService.Setup(s => s.GetFriendsByPrimaryUserId(userId)).Returns(associations);
            _mockUserService.Setup(s => s.GetUserById(otherUserId)).Returns(friends.First());
            _mockChatService.Setup(s => s.GetChatUserByChatIdAndUserId(chatId, otherUserId)).Returns(otherChatUser);

            // Act
            IActionResult result = _controller.GetFriendsForGroupInvitation(chatId, partialDisplayName);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            List<GroupInvitationFriendInfoResponse> returnValue = Assert.IsType<List<GroupInvitationFriendInfoResponse>>(okResult.Value);
            Assert.Single(returnValue);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockAssocService.Verify(s => s.GetFriendsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            _mockUserService.Verify(s => s.GetUserById(It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.GetChatUserByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void SendGroupChatInvitationToFriend_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            SendGroupChatInvitationRequest request = new SendGroupChatInvitationRequest
            {
                chatId = 1,
                friendUserId = 2,
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.ValidateUserIsAChatMember(request.chatId, userId));

            // Act
            IActionResult result = _controller.SendGroupChatInvitationToFriend(request);

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.ValidateUserIsAChatMember(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.SendGroupChatInvitation(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void GetGroupChatInvitations_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;

            ChatUser chatUser = new ChatUser
            {
                ChatId = 1,
                UserId = userId
            };
            List<ChatUser> invitations = new List<ChatUser>
            {
                chatUser
            };
            Chat chat = new Chat
            {
                Id = 1,
                DisplayName = "Test Chat",
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.GetGroupInvitationsByUserId(userId)).Returns(invitations);
            _mockChatService.Setup(s => s.GetChatById(chat.Id)).Returns(chat);

            // Act
            IActionResult result = _controller.GetGroupChatInvitations();

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            List<GetGroupChatInvitationResponse> returnValue = Assert.IsType<List<GetGroupChatInvitationResponse>>(okResult.Value);
            Assert.Single(returnValue);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.GetGroupInvitationsByUserId(It.IsAny<int>()), Times.Once);
            _mockChatService.Verify(s => s.GetChatById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void AnswerGroupChatInvitation_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            AnswerGroupChatInvitationRequest request = new AnswerGroupChatInvitationRequest
            {
                ChatId = 1,
                Accept = true,
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);

            // Act
            IActionResult result = _controller.AnswerGroupChatInvitation(request);

            // Assert
            Assert.IsType<OkResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.AnswerGroupChatInvitation(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>()), Times.Once);
        }

        [Fact]
        public void ChangeChatName_ReturnsOkResult()
        {
            // Arrange
            int userId = 1;
            int chatId = 1;
            ChangeChatNameRequest request = new ChangeChatNameRequest
            {
                NewName = "New Chat Name",
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.IsAllowedToChangeChatName(chatId, userId)).Returns(true);

            // Act
            IActionResult result = _controller.ChangeChatName(chatId, request);

            // Assert
            OkObjectResult okResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<Response>(okResult.Value);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.IsAllowedToChangeChatName(chatId, userId), Times.Once);
            _mockChatService.Verify(s => s.ChangeChatName(chatId, request.NewName), Times.Once);
        }

        [Fact]
        public void ChangeChatName_ReturnsBadRequest_WhenNotAllowed()
        {
            // Arrange
            int userId = 1;
            int chatId = 1;
            ChangeChatNameRequest request = new ChangeChatNameRequest
            {
                NewName = "New Chat Name",
            };

            _mockAuthService.Setup(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>())).Returns(userId);
            _mockChatService.Setup(s => s.IsAllowedToChangeChatName(chatId, userId)).Returns(false);

            // Act
            IActionResult result = _controller.ChangeChatName(chatId, request);

            // Assert
            Assert.IsType<BadRequestResult>(result);
            _mockAuthService.Verify(s => s.GetUserIdFromClaimPrincipal(It.IsAny<ClaimsPrincipal>()), Times.Once);
            _mockChatService.Verify(s => s.IsAllowedToChangeChatName(chatId, userId), Times.Once);
            _mockChatService.Verify(s => s.ChangeChatName(chatId, request.NewName), Times.Never);
        }
    }
}
