﻿using CloudinaryDotNet.Actions;
using CloudinaryDotNet.Core;
using Google.Protobuf.WellKnownTypes;
using Logic.Entities;
using Logic.Enumerations.Status;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Services;
using Moq;
using Mysqlx.Crud;
using Org.BouncyCastle.Bcpg.Sig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Xunit.Sdk;

namespace Test.UnitTests.Services
{
    public class AssocServiceTesting
    {
        private AssocService _assocService;

        private Mock<IAssocRepository> _assocRepoMock;
        private Mock<IUserRepository> _userRepoMock;

        public AssocServiceTesting()
        {
            _assocRepoMock = new Mock<IAssocRepository>();
            _userRepoMock = new Mock<IUserRepository>();
            _assocService = new AssocService(_assocRepoMock.Object, _userRepoMock.Object);
        }

        [Fact]
        public void CreateAssociation_SuccessAndReturnsAssocation()
        {
            // Arrange
            int primaryUserId = 1;
            int secondaryUserId = 2;

            // Act
            Association returnedAssoc = _assocService.CreateAssociation(primaryUserId, secondaryUserId);

            // Assert
            _assocRepoMock.Verify(r => r.Add(It.IsAny<Association>()), Times.Once);
            Assert.Equal(primaryUserId, returnedAssoc.PrimaryUserId);
            Assert.Equal(secondaryUserId, returnedAssoc.SecondaryUserId);
            Assert.Equal(AssociationStatus.Dissociated, returnedAssoc.Status);
        }

        [Fact]
        public void GetOtherUsersByPartialDisplayName()
        {
            // Arrange
            string partialDisplayName = "name";
            int ignoreUserId = 1;
            List<User> list = new List<User>
            {
                new User
                {
                    Id = 1,
                },
                new User
                {
                    Id = 2
                },
                new User
                {
                    Id = 3
                },
                new User
                {
                    Id = 4
                },
            };
            _userRepoMock.Setup(r => r.GetAllByPartialDisplayName(partialDisplayName)).Returns(list);

            // Act
            List<User> returnList = _assocService.GetOtherUsersByPartialDisplayName(ignoreUserId, partialDisplayName);

            // Assert
            _userRepoMock.Verify(r => r.GetAllByPartialDisplayName(It.IsAny<string>()), Times.Once);
            Assert.Equal(list.Where(x => x.Id != ignoreUserId), returnList);
        }

        [Fact]
        public void GetAssociationByUserIds_SuccessAndReturnsAssociation()
        {
            // Arrange
            int primaryUserId = 1;
            int secondaryUserId = 2;
            Association assoc = new Association
            {
                PrimaryUserId = primaryUserId,
                SecondaryUserId = secondaryUserId,
            };
            _assocRepoMock.Setup(r => r.GetByUserIds(primaryUserId, secondaryUserId)).Returns(assoc.Clone);

            // Act
            Association returnedAssoc = _assocService.GetAssociationByUserIds(primaryUserId, secondaryUserId);

            // Assert
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.Equal(primaryUserId, returnedAssoc.PrimaryUserId);
            Assert.Equal(secondaryUserId, returnedAssoc.SecondaryUserId);
        }

        [Fact]
        public void GetAssociationByUserIds_ReturnsAssociation_WhenItDoesNotExist()
        {
            // Arrange
            int primaryUserId = 1;
            int secondaryUserId = 2;

            // Act
            Association returnedAssoc = _assocService.GetAssociationByUserIds(primaryUserId, secondaryUserId);

            // Assert
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _assocRepoMock.Verify(r => r.Add(It.IsAny<Association>()), Times.Once);
            Assert.Equal(primaryUserId, returnedAssoc.PrimaryUserId);
            Assert.Equal(secondaryUserId, returnedAssoc.SecondaryUserId);
        }

        [Fact]
        public void GetIncomingAssociationsByPrimaryUserId_ReturnsList()
        {
            // Arrange
            int primaryUserId = 1;
            List<Association> list = new List<Association>
            {
                new Association(),
                new Association(),
                new Association(),
            };
            _assocRepoMock.Setup(r => r.GetIncomingsByPrimaryUserId(primaryUserId)).Returns(list.ToList());


            // Act
            List<Association> returnedList = _assocService.GetIncomingAssociationsByPrimaryUserId(primaryUserId);

            // Assert
            _assocRepoMock.Verify(r => r.GetIncomingsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            Assert.Equal(list, returnedList);
        }

        [Fact]
        public void GetOutgoingAssociationsByPrimaryUserId_ReturnsList()
        {
            // Arrange
            int primaryUserId = 1;
            List<Association> list = new List<Association>
            {
                new Association(),
                new Association(),
                new Association(),
            };
            _assocRepoMock.Setup(r => r.GetOutgoingsByPrimaryUserId(primaryUserId)).Returns(list.ToList());


            // Act
            List<Association> returnedList = _assocService.GetOutgoingAssociationsByPrimaryUserId(primaryUserId);

            // Assert
            _assocRepoMock.Verify(r => r.GetOutgoingsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            Assert.Equal(list, returnedList);
        }

        [Fact]
        public void GetFriendsByPrimaryUserId_ReturnsList()
        {
            // Arrange
            int primaryUserId = 1;
            List<Association> list = new List<Association>
            {
                new Association(),
                new Association(),
                new Association(),
            };
            _assocRepoMock.Setup(r => r.GetFriendsByPrimaryUserId(primaryUserId)).Returns(list.ToList());


            // Act
            List<Association> returnedList = _assocService.GetFriendsByPrimaryUserId(primaryUserId);

            // Assert
            _assocRepoMock.Verify(r => r.GetFriendsByPrimaryUserId(It.IsAny<int>()), Times.Once);
            Assert.Equal(list, returnedList);
        }

        [Fact]
        public void Unfriend_Success()
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            Association senderAssoc = new Association
            {
                PrimaryUserId = senderId,
                SecondaryUserId = receiverId,
            };
            Association receiverAssoc = new Association
            {
                PrimaryUserId = receiverId,
                SecondaryUserId = senderId,
            };

            _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(senderAssoc);
            _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(receiverAssoc);

            // Act
            _assocService.Unfriend(senderId, receiverId);

            // Assert
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            Assert.Equal(AssociationStatus.Dissociated, receiverAssoc.Status);
            Assert.Equal(AssociationStatus.Dissociated, senderAssoc.Status);
            Assert.Equal(senderId, senderAssoc.PrimaryUserId);
            Assert.Equal(senderId, receiverAssoc.SecondaryUserId);
            Assert.Equal(receiverId, senderAssoc.SecondaryUserId);
            Assert.Equal(receiverId, receiverAssoc.PrimaryUserId);
        }



        [Theory]
        [InlineData(false, true, "Sender does not exist")]
        [InlineData(true, false, "Receiver does not exist")]
        public void Unfriend_ThrowsException(bool sender, bool receiver, string expectedExceptionMsg)
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            if (sender) _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(new Association());
            if (receiver) _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(new Association());

            // Act & Assert
            AssociationException ex = Assert.Throws<AssociationException>(() => _assocService.Unfriend(senderId, receiverId));
            Assert.Equal(expectedExceptionMsg, ex.Message);
        }

        [Theory]
        [InlineData(true, true, 0)]
        [InlineData(true, false, 1)]
        [InlineData(false, true, 1)]
        [InlineData(false, false, 2)]
        public void CreateFriendRequest_Success(bool sender, bool receiver, int addedCount)
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            Association senderAssoc = new Association
            {
                PrimaryUserId = senderId,
                SecondaryUserId = receiverId,
                Status = AssociationStatus.Dissociated,
            };
            Association receiverAssoc = new Association
            {
                PrimaryUserId = receiverId,
                SecondaryUserId = senderId,
                Status = AssociationStatus.Dissociated,
            };

            if (sender) _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(senderAssoc);
            if (receiver) _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(receiverAssoc);

            // Act
            _assocService.CreateFriendRequest(senderId, receiverId);

            // Assert
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            _assocRepoMock.Verify(r => r.Add(It.IsAny<Association>()), Times.Exactly(addedCount));
            if (sender && receiver)
            {
                Assert.Equal(AssociationStatus.PendingReceiver, receiverAssoc.Status);
                Assert.Equal(AssociationStatus.PendingSender, senderAssoc.Status);
                Assert.Equal(senderId, senderAssoc.PrimaryUserId);
                Assert.Equal(senderId, receiverAssoc.SecondaryUserId);
                Assert.Equal(receiverId, senderAssoc.SecondaryUserId);
                Assert.Equal(receiverId, receiverAssoc.PrimaryUserId);
            }
        }

        [Theory]
        [InlineData(AssociationStatus.Blocked, AssociationStatus.Dissociated, 0, "Failed to sent friend request. One (or both) user(s) blocked each other.")]
        [InlineData(AssociationStatus.Dissociated, AssociationStatus.Blocked, 0, "Failed to sent friend request. One (or both) user(s) blocked each other.")]
        [InlineData(AssociationStatus.Consociated, AssociationStatus.Consociated, 0, "Users are already friends with each other")]
        [InlineData(AssociationStatus.Consociated, AssociationStatus.PendingReceiver, 2, "Unknown error has occured while sending a friend request. Reverting states...")]
        [InlineData(AssociationStatus.PendingSender, AssociationStatus.PendingReceiver, 0, "Friend request was already made, and in pending.")]
        [InlineData(AssociationStatus.PendingReceiver, AssociationStatus.PendingSender, 0, "Friend request was already made, and in pending.")]
        public void CreateFriendRequest_ThrowsException(AssociationStatus senderStatus, AssociationStatus receiverStatus, int updatedCount, string expectedExceptionMsg)
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            Association senderAssoc = new Association
            {
                PrimaryUserId = senderId,
                SecondaryUserId = receiverId,
                Status = senderStatus,
            };
            Association receiverAssoc = new Association
            {
                PrimaryUserId = receiverId,
                SecondaryUserId = senderId,
                Status = receiverStatus,
            };

            _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(senderAssoc);
            _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(receiverAssoc);

            // Act & Assert
            AssociationException ex = Assert.Throws<AssociationException>(() => _assocService.CreateFriendRequest(senderId, receiverId));
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            _assocRepoMock.Verify(r => r.Update(It.IsAny<Association>()), Times.Exactly(updatedCount));
            Assert.Equal(expectedExceptionMsg, ex.Message);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void AnswerFriendRequest_Success(bool acceptRequest)
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            Association senderAssoc = new Association
            {
                PrimaryUserId = senderId,
                SecondaryUserId = receiverId,
                Status = AssociationStatus.PendingSender,
            };
            Association receiverAssoc = new Association
            {
                PrimaryUserId = receiverId,
                SecondaryUserId = senderId,
                Status = AssociationStatus.PendingReceiver,
            };

            _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(senderAssoc);
            _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(receiverAssoc);

            // Act
            _assocService.AnswerFriendRequest(receiverId, senderId, acceptRequest);

            // Assert
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            _assocRepoMock.Verify(r => r.Update(It.IsAny<Association>()), Times.Exactly(2));
            Assert.Equal(senderId, senderAssoc.PrimaryUserId);
            Assert.Equal(senderId, receiverAssoc.SecondaryUserId);
            Assert.Equal(receiverId, senderAssoc.SecondaryUserId);
            Assert.Equal(receiverId, receiverAssoc.PrimaryUserId);
            if (acceptRequest)
            {
                Assert.Equal(AssociationStatus.Consociated, senderAssoc.Status);
                Assert.Equal(AssociationStatus.Consociated, receiverAssoc.Status);
            }
            else
            {
                Assert.Equal(AssociationStatus.Dissociated, senderAssoc.Status);
                Assert.Equal(AssociationStatus.Dissociated, receiverAssoc.Status);
            }
        }

        [Theory]
        [InlineData(true, false, "Receiver does not exist")]
        [InlineData(false, true, "Sender does not exist")]
        public void AnswerFriendRequest_ThrowsException_WhenAssocDoesNotExist(bool sender, bool receiver, string expectedExceptionMsg)
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            Association senderAssoc = new Association
            {
                PrimaryUserId = senderId,
                SecondaryUserId = receiverId,
                Status = AssociationStatus.PendingSender,
            };
            Association receiverAssoc = new Association
            {
                PrimaryUserId = receiverId,
                SecondaryUserId = senderId,
                Status = AssociationStatus.PendingReceiver,
            };

            if (sender) _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(senderAssoc);
            if (receiver) _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(receiverAssoc);

            // Act & Assert
            AssociationException ex = Assert.Throws<AssociationException>(() => _assocService.AnswerFriendRequest(receiverId, senderId, true));
            Assert.Equal(expectedExceptionMsg, ex.Message);
        }

        [Theory]
        [InlineData(AssociationStatus.Consociated, AssociationStatus.Consociated, 0, "These two users are already friends with each other.")]
        [InlineData(AssociationStatus.Blocked, AssociationStatus.Dissociated, 0, "Attempting (somehow) to accept friend requests, when one or both users blocked each other.")]
        [InlineData(AssociationStatus.Dissociated, AssociationStatus.Blocked, 0, "Attempting (somehow) to accept friend requests, when one or both users blocked each other.")]
        [InlineData(AssociationStatus.Dissociated, AssociationStatus.PendingReceiver, 2, "Unable to answer a friend request, because the status are incorrect. Did they even sent a friend request?")]
        [InlineData(AssociationStatus.PendingSender, AssociationStatus.Dissociated, 2, "Unable to answer a friend request, because the status are incorrect. Did they even sent a friend request?")]
        public void AnswerFriendRequest_ThrowsException_WhenAssocStatusIncorrect(AssociationStatus senderStatus, AssociationStatus receiverStatus, int updatedCount, string expectedExceptionMsg)
        {
            // Arrange
            int senderId = 1;
            int receiverId = 2;

            Association senderAssoc = new Association
            {
                PrimaryUserId = senderId,
                SecondaryUserId = receiverId,
                Status = senderStatus,
            };
            Association receiverAssoc = new Association
            {
                PrimaryUserId = receiverId,
                SecondaryUserId = senderId,
                Status = receiverStatus,
            };

            _assocRepoMock.Setup(r => r.GetByUserIds(senderId, receiverId)).Returns(senderAssoc);
            _assocRepoMock.Setup(r => r.GetByUserIds(receiverId, senderId)).Returns(receiverAssoc);

            // Act & Assert
            AssociationException ex = Assert.Throws<AssociationException>(() => _assocService.AnswerFriendRequest(receiverId, senderId, true));
            Assert.Equal(expectedExceptionMsg, ex.Message);
            _assocRepoMock.Verify(r => r.GetByUserIds(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            _assocRepoMock.Verify(r => r.Update(It.IsAny<Association>()), Times.Exactly(updatedCount));
        }

    }
}
