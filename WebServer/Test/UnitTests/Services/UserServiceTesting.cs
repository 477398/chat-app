﻿using CloudinaryDotNet.Core;
using Logic.Entities;
using Logic.Enumerations.Status;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Services;
using Logic.Utilities;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Test.UnitTests.Services
{

    public class UserServiceTesting
    {
        private readonly UserService _userService;

        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IMessageRepository> _msgRepositoryMock;
        private Mock<IImageRepository> _imgRepositoryMock;

        public UserServiceTesting()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _msgRepositoryMock = new Mock<IMessageRepository>();
            _imgRepositoryMock = new Mock<IImageRepository>();
            _userService = new UserService(_userRepositoryMock.Object, _msgRepositoryMock.Object, _imgRepositoryMock.Object);
        }

        [Fact]
        public void CreateUser_ReturnsUserAndCreatesSuccesfully()
        {
            // Arrange
            string loginName = "name";
            string displayName = "name2";
            string email = "email@email.com";
            string password = "unhashedpw";

            // Act
            User returnedUser = _userService.CreateUser(loginName, displayName, email, password, password);

            // Assert
            _userRepositoryMock.Verify(r => r.Add(It.IsAny<User>()), Times.Once);
            Assert.True(EmailValidator.IsValid(email));
            Assert.Equal(loginName, returnedUser.LoginName);
            Assert.Equal(displayName, returnedUser.DisplayName);
            Assert.Equal(email, returnedUser.Email);
            Assert.Equal(PasswordHasher.Hash(password), returnedUser.HashedPassword);
            Assert.Equal(UserStatus.Active, returnedUser.Status);
        }

        [Fact]
        public void CreateUser_ThrowsException_WhenLoginNameAlreadyExists()
        {
            // Arrange
            string loginName = "loginName";
            _userRepositoryMock.Setup(r => r.GetUserByLoginName(loginName)).Returns(new User());

            // Act & Assert
            UserException ex = Assert.Throws<UserException>(() => _userService.CreateUser(loginName, "displayName", "email@email.com", "password", "password"));
            Assert.Equal("Account already exists with this login name", ex.Message);
        }

        [Fact]
        public void CreateUser_ThrowsException_WhenEmailAlreadyExists()
        {
            // Arrange
            string email = "email@email.com";
            _userRepositoryMock.Setup(r => r.GetUserByEmail(email)).Returns(new User());

            // Act & Assert
            UserException ex = Assert.Throws<UserException>(() => _userService.CreateUser("loginName", "displayName", email, "password", "password"));
            Assert.Equal("Account already exists with this e-mail", ex.Message);
        }

        [Theory]
        [InlineData("", "displayName", "email@email.com", "password", "password", "Login name too short. Minimum 4 letters required.")] // LoginName length test
        [InlineData("loginName", "", "email@email.com", "password", "password", "Display name too short. Minimum 4 letters required.")] // DisplayName length test
        [InlineData("loginName", "displayName", "email", "password", "password", "Invalid e-mail")] // Email format validation
        [InlineData("loginName", "displayName", "email@email.com", "passwor", "passwor", "Password too short. Minimum 8 letters required.")] // Password length test
        [InlineData("loginName", "displayName", "email@email.com", "password", "passwor", "Passwords don't match.")] // Password match test
        public void CreateUser_ThrowsException(string loginName, string displayName, string email, string password, string unhashedPassword, string expectedExceptionMessage)
        {
            // Arrange
            // n/a

            // Act & Assert
            UserException ex = Assert.Throws<UserException>(() => _userService.CreateUser(loginName, displayName, email, password, unhashedPassword));
            Assert.Equal(expectedExceptionMessage, ex.Message);
        }

        [Fact]
        public void GetUserById_ReturnUser()
        {
            // Arrange
            User user = new User
            {
                Id = 1,
            };

            _userRepositoryMock.Setup(r => r.GetById(user.Id)).Returns(user.Clone()); // Clone: to seperate the reference to validate indifferences.

            // Act
            User returnedUser = _userService.GetUserById(user.Id);

            // Assert
            _userRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            Assert.Equal(user.LoginName, returnedUser.LoginName);
            Assert.Equal(user.DisplayName, returnedUser.DisplayName);
            Assert.Equal(user.Email, returnedUser.Email);
            Assert.Equal(user.HashedPassword, returnedUser.HashedPassword);
        }

        [Fact]
        public void GetUserById_ThrowsException_WhenUserDoesNotExist()
        {
            // Arrange
            // n/a

            // Act & Assert
            UserException ex = Assert.Throws<UserException>(() => _userService.GetUserById(1));
            Assert.Equal("User does not exist.", ex.Message);
        }

        [Fact]
        public void UpdateUser_Success()
        {
            // Arrange
            User user = new User
            {
                LoginName = "LoginName",
                DisplayName = "DisplayName",
                Email = "Email@Email.com",
            };

            // Act
            _userService.UpdateUser(user);

            // Assert
            _userRepositoryMock.Verify(r => r.Update(user), Times.Once);
        }

        [Theory]
        [InlineData("", "DisplayName", "Email@Email.com", "Login name too short. Minimum 4 letters required.")] // LoginName length test
        [InlineData("LoginName", "", "Email@Email.com", "Display name too short. Minimum 4 letters required.")] // DisplayName length test
        [InlineData("LoginName", "DisplayName", "", "Invalid e-mail")] // Email validity test
        public void UpdateUser_ThrowsExcception(string loginName, string displayName, string email, string expectedExceptionMessage)
        {
            // Arrange
            User user = new User
            {
                LoginName = loginName,
                DisplayName = displayName,
                Email = email,
            };

            // Act & Assert
            UserException ex = Assert.Throws<UserException>(() => _userService.UpdateUser(user));
            Assert.Equal(expectedExceptionMessage, ex.Message);
        }

        [Fact]
        public void GetMessagesByUserId_ReturnsMessages()
        {
            // Arrange
            int userId = 1;
            List<Message> messages = new List<Message>
            {
                new Message(),
                new Message(),
                new Message(),
            };
            _msgRepositoryMock.Setup(r => r.GetAllByUserId(userId)).Returns(messages.ToList()); // ToList(): to seperate the reference to validate indifferences.

            // Act
            List<Message> returnedMessages = _userService.GetMessagesByUserId(userId);

            // Assert
            _msgRepositoryMock.Verify(r => r.GetAllByUserId(userId), Times.Once);
            Assert.Equal(messages, returnedMessages);
        }

        [Fact]
        public async Task UploadPfp_Success()
        {
            // Arrange
            // n/a

            // Act
            await _userService.UploadPfp(new Mock<IFormFile>().Object, 1);

            // Assert
            _imgRepositoryMock.Verify(c => c.UploadImage(It.IsAny<IFormFile>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public async Task UploadDefaultPfp_Success()
        {
            // Arrange
            // n/a

            // Act
            await _userService.UploadDefaultPfp(1);
            Debug.Write($"Dir: {Directory.GetCurrentDirectory()}");

            // Assert
            _imgRepositoryMock.Verify(c => c.UploadImage(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
    }
}
