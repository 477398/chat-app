﻿using CloudinaryDotNet.Core;
using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Services;
using Microsoft.Extensions.Configuration;
using Moq;
using MySqlX.XDevAPI.Common;
using System.Security.Claims;

namespace Test.UnitTests.Services
{
    public class AuthServiceTesting
    {
        private readonly AuthService _authService;

        private readonly Mock<IUserRepository> _userRepoMock;
        private readonly Mock<IConfiguration> _configMock;

        public AuthServiceTesting()
        {
            _userRepoMock = new Mock<IUserRepository>();
            _configMock = new Mock<IConfiguration>();
            _authService = new AuthService(_configMock.Object, _userRepoMock.Object);
        }

        [Fact]
        public void AuthenticateUser_Success()
        {
            // Arrange
            User user = new User
            {
                HashedPassword = "ajfnksafjklashfafoafnjloasfjlasfjlkaslj",
            };

            // Act
            _authService.AuthenticateUser(user, user.HashedPassword);

            // Assert
            // Pass
        }

        [Fact]
        public void AuthenticateUser_ThrowException_OnWrongPassword()
        {
            // Arrange
            User user = new User
            {
                HashedPassword = "1",
            };

            // Act & Assert
            Assert.Throws<AuthException>(() => _authService.AuthenticateUser(user, "2"));
        }

        [Fact]
        public void AuthenticateUserByLoginName_Success()
        {
            // Arrange
            string loginName = "name";
            User user = new User
            {
                LoginName = loginName,
                HashedPassword = "amfnklasfklanfklalfasflafjkabka",
            };
            _userRepoMock.Setup(r => r.GetUserByLoginName(loginName)).Returns(user);

            // Act
            User result = _authService.AuthenticateUserByLoginName(loginName, user.HashedPassword);

            // Assert
            _userRepoMock.Verify(r => r.GetUserByLoginName(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void AuthenticateUserByLoginName_ThrowsException_WhenUserDoesNotExist()
        {
            // Arrange
            // n/a

            // Act & Assert
            Assert.Throws<AuthException>(() => _authService.AuthenticateUserByLoginName(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Fact]
        public void AuthenticateUserByEmail_Success()
        {
            // Arrange
            string email = "email@email.com";
            User user = new User
            {
                Email = email,
                HashedPassword = "amfnklasfklanfklalfasflafjkabka",
            };
            _userRepoMock.Setup(r => r.GetUserByEmail(email)).Returns(user);

            // Act
            User result = _authService.AuthenticateUserByEmail(email, user.HashedPassword);

            // Assert
            _userRepoMock.Verify(r => r.GetUserByEmail(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void AuthenticateUserByEmail_ThrowsException_WhenUserDoesNotExist()
        {
            // Arrange
            // n/a

            // Act & Assert
            Assert.Throws<AuthException>(() => _authService.AuthenticateUserByEmail(It.IsAny<string>(), It.IsAny<string>()));
        }

        [Fact]
        public void GenerateJWT_Success()
        {
            // Arrange
            int userId = 1;
            _configMock.Setup(r => r["JWT:Key"]).Returns("sdgnlagoagoagnwoagdjsgobgoaehgwaogjsdgoasbgogboawbgosjdgoj");

            // Act
            string result = _authService.GenerateJWT(userId);

            // Assert
            Assert.True(result != "");
        }

        [Fact]
        public void GenerateJWT_ThrowsException_WhenKeyDoesNotExist()
        {
            // Arrange
            int userId = 1;

            // Act & Assert
            Assert.Throws<NullReferenceException>(() => _authService.GenerateJWT(userId));
        }

        [Fact]
        public void GetUserIdFromClaimPrincipal_Succes()
        {
            // Arrange
            int userId = 1;
            ClaimsPrincipal principal = new ClaimsPrincipal(
                new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                })
            );

            // Act
            int result = _authService.GetUserIdFromClaimPrincipal(principal);

            // Assert
            Assert.Equal(userId, result);
        }

        [Fact]
        public void GetUserIdFromClaimPrincipal_ThrowsException_WhenNameIdDoesNotExist()
        {
            // Arrange
            ClaimsPrincipal principal = new ClaimsPrincipal(new ClaimsIdentity(new Claim[] { }));

            // Act & Assert
            Assert.Throws<AuthException>(() => _authService.GetUserIdFromClaimPrincipal(principal));
        }
    }
}
