﻿using CloudinaryDotNet.Core;
using Logic.Entities;
using Logic.Enumerations;
using Logic.Enumerations.Status;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Services;
using Moq;
using MySqlX.XDevAPI.Common;
using NuGet.Frameworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.UnitTests.Services
{
    public class ChatServiceTesting
    {
        private readonly ChatService _service;

        private readonly Mock<IChatRepository> _chatRepoMock;
        private readonly Mock<IChatUserRepository> _chatUserRepoMock;
        private readonly Mock<IMessageRepository> _msgRepoMock;

        public ChatServiceTesting()
        {
            _chatRepoMock = new Mock<IChatRepository>();
            _chatUserRepoMock = new Mock<IChatUserRepository>();
            _msgRepoMock = new Mock<IMessageRepository>();
            _service = new ChatService(_chatRepoMock.Object, _chatUserRepoMock.Object, _msgRepoMock.Object);
        }

        [Fact]
        public void CreatePrivateChat_Success()
        {
            // Arrange
            // n/a

            // Act
            Chat result = _service.CreatePrivateChat(1, 2);

            // Assert
            _chatRepoMock.Verify(r => r.Add(It.IsAny<Chat>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Add(It.IsAny<ChatUser>()), Times.Exactly(2));
            Assert.Equal(ChatType.Private, result.Type);
        }

        [Fact]
        public void GetPrivateChatByUserIds_ReturnsExistingChat()
        {
            // Arrange
            int chatId = 1;
            int userOneId = 1;
            int userTwoId = 2;
            List<Chat> privateChats = new List<Chat>
            {
                new Chat
                {
                    Id = chatId,
                }
            };
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userOneId,
                },
                new ChatUser
                {
                    UserId = userTwoId,
                }
            };
            _chatRepoMock.Setup(r => r.GetAllPrivateChats()).Returns(privateChats);
            _chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chatId)).Returns(members);

            // Act
            Chat result = _service.GetPrivateChatByUserIds(userOneId, userTwoId);

            // Assert
            Assert.Equal(privateChats[0], result);
            _chatRepoMock.Verify(r => r.GetAllPrivateChats(), Times.Once());
            _chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void GetPrivateChatByUserIds_ReturnsNewChat()
        {
            // Arrange
            int userOneId = 1;
            int userTwoId = 2;
            List<Chat> privateChats = new List<Chat>();
            _chatRepoMock.Setup(r => r.GetAllPrivateChats()).Returns(privateChats);

            // Act
            Chat result = _service.GetPrivateChatByUserIds(userOneId, userTwoId);

            // Assert
            Assert.True(result != null);
            _chatRepoMock.Verify(r => r.GetAllPrivateChats(), Times.Once());
            _chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Never);
        }

        [Fact]
        public void ValidateUserIsAChatMember_Sucess()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userId,
                    Status = ChatUserStatus.JoinedAsMember,
                },
            };
            _chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chatId)).Returns(members);

            // Act
            _service.ValidateUserIsAChatMember(chatId, userId);

            // Arrange
            _chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void ValidateUserIsAChatMember_ThrowsException_WhenUserIsNotAMember()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userId,
                    Status = ChatUserStatus.Dissociated,
                },
            };
            this._chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chatId)).Returns(members);

            // Act & Assert
            Assert.Throws<ChatException>(() => _service.ValidateUserIsAChatMember(chatId, userId));
            this._chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void IsUserAChatMember_ReturnsTrue()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userId,
                    Status = ChatUserStatus.JoinedAsMember,
                },
            };
            this._chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chatId)).Returns(members);

            // Act
            bool result = _service.IsUserAChatMember(chatId, userId);

            // Arrange
            this._chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
            Assert.True(result);
        }

        [Fact]
        public void IsUserAChatMember_ReturnsFalse()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userId,
                    Status = ChatUserStatus.Dissociated,
                },
            };
            this._chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chatId)).Returns(members);

            // Act
            bool result = _service.IsUserAChatMember(chatId, userId);

            // Arrange
            this._chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
            Assert.False(result);
        }

        [Fact]
        public void GetChatMembers_Success()
        {

            // Arrange
            int chatId = 1;
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser(),
                new ChatUser(),
                new ChatUser(),
            };
            this._chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chatId)).Returns(members.ToList());

            // Act
            List<ChatUser> result = _service.GetChatMembers(chatId);

            // Assert
            this._chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
            Assert.Equal(members, result);
        }

        [Fact]
        public void CreateGroupChat_Success()
        {
            // Arrange
            // n/a

            // Act
            Chat result = _service.CreateGroupChat(1);

            // Assert
            this._chatRepoMock.Verify(r => r.Add(It.IsAny<Chat>()), Times.Once);
            this._chatUserRepoMock.Verify(r => r.Add(It.IsAny<ChatUser>()), Times.Once);
            Assert.Equal(ChatType.Group, result.Type);
        }

        [Fact]
        public void GetGroupChatsWithUserInIt()
        {
            // Arrange
            int userId = 2;
            List<Chat> groupChats = new List<Chat>
            {
                new Chat
                {
                    Id = 1,
                    Status = ChatStatus.Active,
                },
                new Chat
                {
                    Id = 2,
                    Status = ChatStatus.Active,
                },
                new Chat
                {
                    Id = 3,
                    Status = ChatStatus.Deserted,
                },
            };
            List<ChatUser> members = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userId,
                    Status = ChatUserStatus.JoinedAsMember,
                },
            };
            List<ChatUser> members2 = new List<ChatUser>
            {
                new ChatUser
                {
                    UserId = userId,
                    Status = ChatUserStatus.Dissociated,
                },
            };
            _chatRepoMock.Setup(r => r.GetAllGroupChats()).Returns(groupChats);
            _chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(1)).Returns(members);
            _chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(2)).Returns(members2);

            // Act
            List<Chat> result = _service.GetGroupChatsWithUserInIt(userId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Exactly(2));
            _chatRepoMock.Verify(r => r.GetAllGroupChats(), Times.Once);
            Assert.Single(result);
            Assert.Equal(groupChats[0], result[0]);
        }

        [Fact]
        public void GetChatUserByChatIdAndUserId_ReturnsExistingObject()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            ChatUser cu = new ChatUser
            {
                ChatId = chatId,
                UserId = userId,
                Status = ChatUserStatus.JoinedAsFounder,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, userId)).Returns(cu.Clone());

            // Act
            ChatUser result = _service.GetChatUserByChatIdAndUserId(chatId, userId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Add(It.IsAny<ChatUser>()), Times.Never);
            Assert.Equal(cu.UserId, result.UserId);
            Assert.Equal(cu.ChatId, result.ChatId);
            Assert.Equal(cu.Status, result.Status);
        }

        [Fact]
        public void GetChatUserByChatIdAndUserId_CreatesNewObject()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;

            // Act
            ChatUser result = _service.GetChatUserByChatIdAndUserId(chatId, userId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Add(It.IsAny<ChatUser>()), Times.Once);
            Assert.Equal(userId, result.UserId);
            Assert.Equal(chatId, result.ChatId);
            Assert.Equal(ChatUserStatus.Dissociated, result.Status);
        }

        [Fact]
        public void SendGroupChatInvitation_Success()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            ChatUser cu = new ChatUser
            {
                ChatId = chatId,
                UserId = userId,
                Status = ChatUserStatus.Dissociated,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, userId)).Returns(cu);

            // Act
            _service.SendGroupChatInvitation(chatId, userId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Update(It.IsAny<ChatUser>()), Times.Once);
            Assert.Equal(userId, cu.UserId);
            Assert.Equal(chatId, cu.ChatId);
            Assert.Equal(ChatUserStatus.Pending, cu.Status);
        }

        [Fact]
        public void SendGroupChatInvitation_SucessAndNewObjectCreated()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;

            // Act
            _service.SendGroupChatInvitation(chatId, userId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Update(It.IsAny<ChatUser>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Add(It.IsAny<ChatUser>()), Times.Once);
        }

        [Fact]
        public void SendGroupChatInvitation_ThrowsException_WhenAlreadyAMember()
        {
            // Arrange
            ChatUser cu = new ChatUser
            {
                ChatId = 1,
                UserId = 2,
                Status = ChatUserStatus.JoinedAsMember,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(cu.ChatId, cu.UserId)).Returns(cu);

            // Act & Assert 
            ChatException ex = Assert.Throws<ChatException>(() => _service.SendGroupChatInvitation(cu.ChatId, cu.UserId));
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.Equal("User cannot receive groupchat invitations, because user is already a member of the group chat.", ex.Message);
        }

        [Fact]
        public void GetGroupInvitationsByUserId_Success()
        {
            // Arrange
            int userId = 1;
            List<ChatUser> cus = new List<ChatUser>
            {
                new ChatUser(),
                new ChatUser(),
                new ChatUser(),
            };
            _chatUserRepoMock.Setup(r => r.GetAllPendingByUserId(userId)).Returns(cus.ToList());

            // Act
            List<ChatUser> result = _service.GetGroupInvitationsByUserId(userId);

            // Assert
            Assert.Equal(cus, result);
            _chatUserRepoMock.Verify(r => r.GetAllPendingByUserId(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void GetChatById_Success()
        {
            // Arrange
            int chatId = 1;
            Chat chat = new Chat
            {
                Id = chatId,
            };
            _chatRepoMock.Setup(r => r.GetById(chatId)).Returns(chat.Clone());

            // Act
            Chat result = _service.GetChatById(chatId);

            // Assert
            Assert.Equal(chat.Id, result.Id);
            Assert.Equal(chat.DisplayName, result.DisplayName);
            Assert.Equal(chat.Status, result.Status);
            Assert.Equal(chat.Type, result.Type);
            _chatRepoMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
        }

        [Fact]
        public void LeaveGroupChat_Success()
        {
            // Arrange
            Chat chat = new Chat
            {
                Id = 1,
                Status = ChatStatus.Active,
            };
            ChatUser cu = new ChatUser
            {
                UserId = 2,
                Status = ChatUserStatus.JoinedAsMember,
            };
            List<ChatUser> members = new List<ChatUser>();
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chat.Id, cu.UserId)).Returns(cu);
            _chatUserRepoMock.Setup(r => r.GetAllMembersByChatId(chat.Id)).Returns(members);
            _chatRepoMock.Setup(r => r.GetById(chat.Id)).Returns(chat);

            // Act
            _service.LeaveGroupChat(chat.Id, cu.UserId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.GetAllMembersByChatId(It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Update(It.IsAny<ChatUser>()), Times.Once);
            _chatRepoMock.Verify(r => r.Update(It.IsAny<Chat>()), Times.Once);
            _chatRepoMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            Assert.Equal(ChatUserStatus.Dissociated, cu.Status);
            Assert.Equal(ChatStatus.Deserted, chat.Status);
        }

        [Theory]
        [InlineData(true, ChatUserStatus.JoinedAsMember)]
        [InlineData(false, ChatUserStatus.Dissociated)]
        public void AnswerGroupChatInvitation_Success(bool acceptInvite, ChatUserStatus expectedStatus)
        {
            // Arrange
            int chatId = 92;
            ChatUser cu = new ChatUser
            {
                UserId = 1,
                Status = ChatUserStatus.Pending,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, cu.UserId)).Returns(cu);

            // Act
            _service.AnswerGroupChatInvitation(chatId, cu.UserId, acceptInvite);

            // Assert
            Assert.Equal(expectedStatus, cu.Status);
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            _chatUserRepoMock.Verify(r => r.Update(It.IsAny<ChatUser>()), Times.Once);
        }

        [Theory]
        [InlineData(ChatUserStatus.JoinedAsMember, "User is already a member of this group chat.")]
        [InlineData(ChatUserStatus.Dissociated, "An unknown error occured while processing the invitation. Reverting state...")]
        public void AnswerGroupChatInvitation_ThrowsException(ChatUserStatus status, string expectedExMsg)
        {
            // Arrange
            int chatId = 74;
            ChatUser cu = new ChatUser
            {
                UserId = 1,
                Status = status,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, cu.UserId)).Returns(cu);

            // Act & Assert
            ChatException ex = Assert.Throws<ChatException>(() => _service.AnswerGroupChatInvitation(chatId, cu.UserId, true));
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Once);
            Assert.Equal(expectedExMsg, ex.Message);
        }

        [Theory]
        [InlineData(ChatUserStatus.Dissociated, false)]
        [InlineData(ChatUserStatus.Pending, false)]
        [InlineData(ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.JoinedAsModerator, false)]
        [InlineData(ChatUserStatus.JoinedAsFounder, true)]
        public void IsAllowedToChangeChatName_ReturnsDependingOnPriviliges(ChatUserStatus status, bool expectedBool)
        {
            // Arrange
            int chatId = 32;
            ChatUser cu = new ChatUser
            {
                UserId = 1,
                Status = status,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, cu.UserId)).Returns(cu);

            // Act
            bool result = _service.IsAllowedToChangeChatName(chatId, cu.UserId);

            // Assert
            Assert.Equal(expectedBool, result);
        }

        [Fact]
        public void IsAllowedToKick_ReturnsTrue_WhenUserItself()
        {
            // Arrange
            int userId = 1;

            // Act
            bool result = _service.IsAllowedToKick(It.IsAny<int>(), userId, userId);

            // Assert
            Assert.True(result);
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Never);
        }

        [Theory]
        [InlineData(ChatUserStatus.JoinedAsMember, ChatUserStatus.JoinedAsModerator, true)]
        [InlineData(ChatUserStatus.JoinedAsModerator, ChatUserStatus.JoinedAsFounder, true)]
        [InlineData(ChatUserStatus.JoinedAsModerator, ChatUserStatus.JoinedAsModerator, false)]
        [InlineData(ChatUserStatus.JoinedAsFounder, ChatUserStatus.JoinedAsFounder, false)]
        [InlineData(ChatUserStatus.JoinedAsMember, ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.Dissociated, ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.Dissociated, ChatUserStatus.JoinedAsModerator, true)]
        [InlineData(ChatUserStatus.JoinedAsFounder, ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.JoinedAsModerator, ChatUserStatus.JoinedAsMember, false)]
        public void IsAllowedToKick_ReturnsDependindOnPrivileges(ChatUserStatus beingKickedStatus, ChatUserStatus requesterStatus, bool expectedBool)
        {
            // Arrange
            int chatId = 29;
            ChatUser requester = new ChatUser
            {
                UserId = 1,
                Status = requesterStatus,
            };
            ChatUser beingKicked = new ChatUser
            {
                UserId = 2,
                Status = beingKickedStatus,
            };
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, requester.UserId)).Returns(requester);
            _chatUserRepoMock.Setup(r => r.GetByChatIdAndUserId(chatId, beingKicked.UserId)).Returns(beingKicked);

            // Act
            bool result = _service.IsAllowedToKick(chatId, requester.UserId, beingKicked.UserId);

            // Assert
            _chatUserRepoMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
            Assert.Equal(expectedBool, result);
        }

        [Fact]
        public void ChangeChatName_Success()
        {
            // Arrange
            string newName = "aksjflsajf    ";
            Chat chat = new Chat
            {
                Id = 1,
            };
            _chatRepoMock.Setup(r => r.GetById(chat.Id)).Returns(chat);

            // Act
            _service.ChangeChatName(chat.Id, newName);

            // Assert
            _chatRepoMock.Verify(r => r.Update(It.IsAny<Chat>()), Times.Once);
            _chatRepoMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            Assert.Equal(newName.Trim(), chat.DisplayName);
        }

        [Fact]
        public void ChangeChatName_ThrowsException_WhenNewNameTooShort()
        {
            // Arrange
            string newName = "";

            // Act & Assert
            ChatException ex = Assert.Throws<ChatException>(() => _service.ChangeChatName(It.IsAny<int>(), newName));
            Assert.Equal("New chat name is too short. Minimum of 4 letters required.", ex.Message);
        }
    }
}
