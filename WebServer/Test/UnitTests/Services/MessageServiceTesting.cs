﻿using Logic.Entities;
using Logic.Enumerations.Status;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Test.UnitTests.Services
{
    public class MessageServiceTesting
    {
        private readonly MessageService _msgService;

        private readonly Mock<IMessageRepository> _msgRepositoryMock;
        private readonly Mock<IChatUserRepository> _chatUserRepositoryMock;
        private readonly Mock<IChatRepository> _chatRepositoryMock;

        public MessageServiceTesting()
        {
            _msgRepositoryMock = new Mock<IMessageRepository>();
            _chatRepositoryMock = new Mock<IChatRepository>();
            _chatUserRepositoryMock = new Mock<IChatUserRepository>();

            _msgService = new MessageService(_msgRepositoryMock.Object, _chatRepositoryMock.Object, _chatUserRepositoryMock.Object);
        }

        [Fact]
        public void GetAllVisibleMessagesByChatId_Success()
        {
            // Arrange
            int chatId = 1;
            List<Message> msgs = new List<Message>
            {
                new Message(),
                new Message(),
                new Message(),
            };
            _msgRepositoryMock.Setup(r => r.GetAllVisiblesByChatId(chatId)).Returns(msgs.ToList());

            // Act
            List<Message> returnedMsgs = _msgService.GetAllVisibleMessagesByChatId(chatId);

            // Assert
            _msgRepositoryMock.Verify(r => r.GetAllVisiblesByChatId(It.IsAny<int>()), Times.Once);
            Assert.Equal(msgs, returnedMsgs);
        }

        [Fact]
        public void CreateMessage_Success()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            string content = "something ";

            // Act
            Message result = _msgService.CreateMessage(userId, chatId, content);

            // Assert
            _msgRepositoryMock.Verify(r => r.Add(It.IsAny<Message>()), Times.Once);
            Assert.Equal(chatId, result.ChatId);
            Assert.Equal(userId, result.UserId);
            Assert.Equal(content.Trim(), result.Content);
        }

        [Fact]
        public void CreateMessage_ThrowException_OnContentEmpty()
        {
            // Arrange
            int chatId = 1;
            int userId = 2;
            string content = "";

            // Act & Assert
            MessageException ex = Assert.Throws<MessageException>(() => _msgService.CreateMessage(userId, chatId, content));
            Assert.Equal("Message cannot be empty", ex.Message);
        }

        [Fact]
        public void IsAllowedToEditMessage_ReturnsTrue()
        {
            // Arrange
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
                Content = "",
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);

            // Act
            bool result = _msgService.IsAllowedToEditMessage(msg.Id, msg.UserId);

            // Assert
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            Assert.True(result);
        }

        [Fact]
        public void IsAllowedToEditMessage_ReturnsFalse()
        {
            // Arrange
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
                Content = "",
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);

            // Act
            bool result = _msgService.IsAllowedToEditMessage(msg.Id, msg.UserId + 1);

            // Assert
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            Assert.False(result);
        }

        [Fact]
        public void IsAllowedToEditMessage_ThrowsException_WhenMsgDoesNotExist()
        {
            // Arrange
            // n/a

            // Act & Assert
            MessageException ex = Assert.Throws<MessageException>(() => _msgService.IsAllowedToEditMessage(It.IsAny<int>(), It.IsAny<int>()));
            Assert.Equal("Message does not exist.", ex.Message);
        }

        [Fact]
        public void EditMessage_Success()
        {
            // Arrange
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
                Content = "",
            };
            string newContent = "something";
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);

            // Act
            _msgService.EditMessage(msg.Id, newContent);

            // Assert
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            _msgRepositoryMock.Verify(r => r.Update(It.IsAny<Message>()), Times.Once);
            Assert.Equal(MessageStatus.DisplayingModified, msg.Status);
            Assert.Equal(newContent, msg.Content);
        }

        [Fact]
        public void HideMessage_Success()
        {
            // Arrange
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
                Content = "",
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);

            // Act
            _msgService.HideMessage(msg.Id);

            // Assert
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            _msgRepositoryMock.Verify(r => r.Update(It.IsAny<Message>()), Times.Once);
            Assert.Equal(MessageStatus.Deleted, msg.Status);
        }

        [Fact]
        public void IsAllowedToHideMessage_ReturnsTrue_WhenSameUser()
        {
            // Arrange
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);

            // Act 
            bool result = _msgService.IsAllowedToHideMessage(msg.Id, msg.UserId);

            // Assert
            Assert.True(result);
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
        }

        [Theory]
        [InlineData(ChatUserStatus.JoinedAsMember, ChatUserStatus.JoinedAsModerator, true)]
        [InlineData(ChatUserStatus.JoinedAsModerator, ChatUserStatus.JoinedAsFounder, true)]
        [InlineData(ChatUserStatus.JoinedAsModerator, ChatUserStatus.JoinedAsModerator, false)]
        [InlineData(ChatUserStatus.JoinedAsFounder, ChatUserStatus.JoinedAsFounder, false)]
        [InlineData(ChatUserStatus.JoinedAsMember, ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.Dissociated, ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.Dissociated, ChatUserStatus.JoinedAsModerator, true)]
        [InlineData(ChatUserStatus.JoinedAsFounder, ChatUserStatus.JoinedAsMember, false)]
        [InlineData(ChatUserStatus.JoinedAsModerator, ChatUserStatus.JoinedAsMember, false)]
        public void IsAllowedToHideMessage_ReturnsTrueOrFalse_DependingOnPrivileges(ChatUserStatus cuStatus, ChatUserStatus requesterCuStatus, bool expectedResult)
        {
            // Arrange
            int requesterUserId = 3;
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
            };
            ChatUser chatUser = new ChatUser
            {
                UserId = msg.UserId,
                Status = cuStatus,
            };
            ChatUser requesterChatUser = new ChatUser
            {
                UserId = requesterUserId,
                Status = requesterCuStatus,
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);
            _chatUserRepositoryMock.Setup(r => r.GetByChatIdAndUserId(msg.ChatId, msg.UserId)).Returns(chatUser);
            _chatUserRepositoryMock.Setup(r => r.GetByChatIdAndUserId(msg.ChatId, requesterUserId)).Returns(requesterChatUser);

            // Act 
            bool result = _msgService.IsAllowedToHideMessage(msg.Id, requesterUserId);

            // Assert
            Assert.Equal(expectedResult, result);
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            _chatUserRepositoryMock.Verify(r => r.GetByChatIdAndUserId(It.IsAny<int>(), It.IsAny<int>()), Times.Exactly(2));
        }

        [Fact]
        public void IsAllowedToHideMessage_ThrowsException_WhenMsgDoesNotExist()
        {
            // Arrange
            // n/a

            // Act & Assert
            MessageException ex = Assert.Throws<MessageException>(() => _msgService.IsAllowedToHideMessage(It.IsAny<int>(), It.IsAny<int>()));
            _msgRepositoryMock.Verify(r => r.GetById(It.IsAny<int>()), Times.Once);
            Assert.Equal("Message does not exist.", ex.Message);
        }

        [Fact]
        public void IsAllowedToHideMessage_ThrowsException_WhenSenderChatUserDoesNotExist()
        {
            // Arrange
            int requesterUserId = 3;
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);

            // Act & Assert
            MessageException ex = Assert.Throws<MessageException>(() => _msgService.IsAllowedToHideMessage(msg.Id, requesterUserId));
            Assert.Equal("Sender's ChatUser does not exist.", ex.Message);
        }

        [Fact]
        public void IsAllowedToHideMessage_ThrowsException_WhenRequesterhatUserDoesNotExist()
        {
            // Arrange
            int requesterUserId = 3;
            Message msg = new Message
            {
                ChatId = 1,
                UserId = 2,
            };
            _msgRepositoryMock.Setup(r => r.GetById(msg.Id)).Returns(msg);
            _chatUserRepositoryMock.Setup(r => r.GetByChatIdAndUserId(msg.ChatId, msg.UserId)).Returns(new ChatUser());

            // Act & Assert
            MessageException ex = Assert.Throws<MessageException>(() => _msgService.IsAllowedToHideMessage(msg.Id, requesterUserId));
            Assert.Equal("Requester's ChatUser does not exist.", ex.Message);
        }

        [Fact]
        public void HideMessage_ThrowsException_WhenMsgDoesNotExist()
        {
            // Arrange
            // n/a

            // Act & Assert
            MessageException ex = Assert.Throws<MessageException>(() => _msgService.HideMessage(It.IsAny<int>()));
            Assert.Equal("Message does not exist.", ex.Message);
        }
    }
}
