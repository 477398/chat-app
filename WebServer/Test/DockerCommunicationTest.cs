﻿using Data.Contexts;
using Logic.Entities;
using Microsoft.EntityFrameworkCore;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace Test
{

	/* This test tests whether the Docker container is able to communicate 
	 * with the host computer's database and fetch data from it.
	 */

	public class DockerCommunicationTest
	{

		private readonly PlatformContext _context;

		public DockerCommunicationTest(ITestOutputHelper output)
		{
			MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
			builder.Server = "host.docker.internal";
			builder.Port = 3306;
			builder.UserID = "root";
			builder.Password = "1234";
			builder.Database = "chatwebapp";

			DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>().UseMySQL(builder.ConnectionString).Options;

			this._context = new PlatformContext(options);
		}

		[Fact]
		public void GetFirstUser()
		{
			Console.WriteLine("-----------------------------------------------------------------------------");
			Console.WriteLine("Docker Container to Host PC Database communication test");

			// Arrange
			// n/a

			// Act
			User? user = this._context.Users.FirstOrDefault();

			// Assert
			if (user == null)
			{
				Assert.Fail("User is null. Failed to fetch from the database. Test unsucessful.");
			}
			else
			{
				Console.WriteLine(
					$"Fetched the following user: \r\n" +
					$"Id: {user.Id} \r\n" +
					$"Login Name: {user.LoginName} \r\n" +
					$"Display Name: {user.DisplayName} \r\n" +
					$"Email: {user.Email} \r\n"
				);
			}
			Console.WriteLine("-----------------------------------------------------------------------------");
		}
	}
}
