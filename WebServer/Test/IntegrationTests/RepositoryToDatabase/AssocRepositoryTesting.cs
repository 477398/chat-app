﻿using Data.Contexts;
using Data.Repositories;
using Logic.Entities;
using Logic.Enumerations.Status;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    public class AssocRepositoryTesting
    {
        private readonly AssocRepository _repo;

        public AssocRepositoryTesting()
        {
            DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Generate a new database everytime to prevent old data interfering with subsequent tests.
                .Options;
            _repo = new AssocRepository(new PlatformContext(options));
        }

        [Fact]
        public void GetByUserIds_SuccessReturnsAssoc()
        {
            // Arrange
            Association assoc = new Association
            {
                PrimaryUserId = 1,
                SecondaryUserId = 2,
            };
            _repo.Add(assoc);

            // Act
            Association? result = _repo.GetByUserIds(assoc.PrimaryUserId, assoc.SecondaryUserId);

            // Assert
            Assert.NotNull(result);
        }

		[Fact]
		public void GetByUserIds_SuccessReturnsNull()
		{
			// Arrange
			// n/a

			// Act
			Association? result = _repo.GetByUserIds(1, 2);

			// Assert
			Assert.Null(result);
		}


		[Fact]
        public void GetIncomingsByPrimaryUserId_SuccessReturn()
        {
            // Arrange
            int userId = 1;
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.PendingReceiver,
            });
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.Dissociated,
            });
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.Consociated,
            });

            // Act
            List<Association> result = _repo.GetIncomingsByPrimaryUserId(userId);

            // Assert
            Assert.Single(result);
        }

        [Fact]
        public void GetOutgoingsByPrimaryUserId_SuccessReturn()
        {
            // Arrange
            int userId = 1;
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.PendingSender,
            });
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.Dissociated,
            });
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.Consociated,
            });

            // Act
            List<Association> result = _repo.GetOutgoingsByPrimaryUserId(userId);

            // Assert
            Assert.Single(result);
        }

        [Fact]
        public void GetFriendsByPrimaryUserId_SuccessReturn()
        {
            // Arrange
            int userId = 1;
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.PendingSender,
            });
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.Dissociated,
            });
            _repo.Add(new Association
            {
                PrimaryUserId = userId,
                Status = AssociationStatus.Consociated,
            });

            // Act
            List<Association> result = _repo.GetFriendsByPrimaryUserId(userId);

            // Assert
            Assert.Single(result);
        }
    }
}
