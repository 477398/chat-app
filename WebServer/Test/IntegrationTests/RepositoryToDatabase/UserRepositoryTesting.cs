﻿using Data.Contexts;
using Data.Repositories;
using Logic.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    public class UserRepositoryTesting
    {
        private readonly UserRepository _repo;

        public UserRepositoryTesting()
        {
            DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Generate a new database everytime to prevent old data interfering with subsequent tests.
                .Options;
            _repo = new UserRepository(new PlatformContext(options));
        }

        [Fact]
        public void GetUserByEmail_SuccessReturnsUser()
        {
            // Arrange
            _repo.Add(new User
            {
                Email = "email_1",
            });

            // Act
            User? result = _repo.GetUserByEmail("email_1");

            // Assert
            Assert.NotNull(result);
        }

		[Fact]
		public void GetUserByEmail_SuccessReturnsNull()
		{
			// Arrange
            // n/a

			// Act
			User? result = _repo.GetUserByEmail("email_1");

			// Assert
			Assert.Null(result);
		}

		[Fact]
        public void GetUserByLoginName_SuccessReturnsUser()
        {
            // Arrange
            _repo.Add(new User
            {
                LoginName = "user_1",
            });

            // Act
            User? result = _repo.GetUserByLoginName("user_1");

            // Assert
            Assert.NotNull(result);
        }

		[Fact]
		public void GetUserByLoginName_SuccessReturnsNull()
		{
			// Arrange
			// n/a
			// Act
			User? result = _repo.GetUserByLoginName("user_1");

			// Assert
			Assert.Null(result);
		}

		[Fact]
        public void GetAllByPartialDisplayName_ReturnSuccess()
        {
            // Arrange
            _repo.Add(new User
            {
                DisplayName = "test",
            });
            _repo.Add(new User
            {
                DisplayName = "another_user",
            });
            _repo.Add(new User
            {
                DisplayName = "tes_user",
            });

            // Act
            List<User> result = _repo.GetAllByPartialDisplayName("test");

            // Assert
            Assert.Single(result);
        }
    }
}
