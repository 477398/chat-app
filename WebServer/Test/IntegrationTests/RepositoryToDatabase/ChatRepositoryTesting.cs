﻿using Data.Contexts;
using Data.Repositories;
using Logic.Entities;
using Logic.Enumerations;
using Logic.Enumerations.Status;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    public class ChatRepositoryTesting
    {
        private readonly ChatRepository _repo;

        public ChatRepositoryTesting()
        {
            DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Generate a new database everytime to prevent old data interfering with subsequent tests.
                .Options;
            _repo = new ChatRepository(new PlatformContext(options));
        }

        [Fact]
        public void GetAllPrivateChats_SuccessReturns()
        {
            // Arrange
            _repo.Add(new Chat
            {
                Type = ChatType.Private
            });
            _repo.Add(new Chat
            {
                Type = ChatType.Group
            });

            // Act
            List<Chat> result = _repo.GetAllPrivateChats();

            // Assert
            Assert.Single(result);
        }

        [Fact]
        public void GetAllGroupChats_SuccessReturn()
        {
            // Arrange
            _repo.Add(new Chat
            {
                Type = ChatType.Private
            });
            _repo.Add(new Chat
            {
                Type = ChatType.Group
            });

            // Act
            List<Chat> result = _repo.GetAllGroupChats();

            // Assert
            Assert.Single(result);
        }
    }
}

