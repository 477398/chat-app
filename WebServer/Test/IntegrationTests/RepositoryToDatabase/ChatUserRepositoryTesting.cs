﻿using Data.Contexts;
using Data.Repositories;
using Logic.Entities;
using Logic.Enumerations.Status;
using Microsoft.EntityFrameworkCore;
using MySqlX.XDevAPI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    public class ChatUserRepositoryTesting
    {
        private readonly ChatUserRepository _repo;

        public ChatUserRepositoryTesting()
        {
            DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Generate a new database everytime to prevent old data interfering with subsequent tests.
                .Options;
            _repo = new ChatUserRepository(new PlatformContext(options));
        }

        [Fact]
        public void GetAllMembersByChatId_SuccessReturn()
        {
            // Arrange
            int chatId = 1;
            _repo.Add(new ChatUser
            {
                ChatId = chatId,
                Status = ChatUserStatus.Dissociated,
            });
            _repo.Add(new ChatUser
            {
                ChatId = chatId,
                Status = ChatUserStatus.Dissociated,
            });
            _repo.Add(new ChatUser
            {
                ChatId = chatId,
                Status = ChatUserStatus.JoinedAsMember,
            });
            _repo.Add(new ChatUser
            {
                ChatId = chatId,
                Status = ChatUserStatus.JoinedAsModerator,
            });
            _repo.Add(new ChatUser
            {
                ChatId = chatId,
                Status = ChatUserStatus.JoinedAsFounder,
            });

            // Act
            List<ChatUser> result = _repo.GetAllMembersByChatId(chatId);

            // Assert
            Assert.Equal(3, result.Count);

        }

        [Fact]
        public void GetByChatIdAndUserId_SuccessReturnsChatUser()
        {
            // Arrange
            _repo.Add(new ChatUser
            {
                ChatId = 1,
                UserId = 2,
            });

            // Act
            ChatUser? result = _repo.GetByChatIdAndUserId(1, 2);

            // Assert
            Assert.NotNull(result);
        }

		[Fact]
		public void GetByChatIdAndUserId_SuccessReturnsNull()
		{
			// Arrange
			// n/a

			// Act
			ChatUser? result = _repo.GetByChatIdAndUserId(1, 2);

			// Assert
			Assert.Null(result);
		}

		[Fact]
        public void GetAllPendingByUserId_SuccessReturn()
        {
            // Arrange
            int userId = 1;
            _repo.Add(new ChatUser
            {
                UserId = userId,
                Status = ChatUserStatus.Pending,
            });
            _repo.Add(new ChatUser
            {
                UserId = userId,
                Status = ChatUserStatus.Dissociated,
            });
            _repo.Add(new ChatUser
            {
                UserId = userId,
                Status = ChatUserStatus.JoinedAsFounder,
            });

            // Act
            List<ChatUser> result = _repo.GetAllPendingByUserId(userId);

            // Assert
            Assert.Single(result);
        }
    }
}
