﻿using Data.Contexts;
using Data.Repositories;
using Logic.Entities;
using Logic.Enumerations.Status;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    public class MessageRepositoryTesting
    {
        private readonly MessageRepository _repo;

        public MessageRepositoryTesting()
        {
            DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Generate a new database everytime to prevent old data interfering with subsequent tests.
                .Options;
            _repo = new MessageRepository(new PlatformContext(options));
        }

        [Fact]
        public void GetAllVisibleByChatId_SuccessReturn()
        {
            // Arrange
            int chatId = 1;
            _repo.Add(new Message
            {
                ChatId = chatId,
                Status = MessageStatus.Deleted
            });
            _repo.Add(new Message
            {
                ChatId = chatId,
                Status = MessageStatus.Displaying
            });
            _repo.Add(new Message
            {
                ChatId = 2,
                Status = MessageStatus.Displaying
            });
            _repo.Add(new Message
            {
                ChatId = chatId,
                Status = MessageStatus.DisplayingModified
            });

            // Act
            List<Message> result = _repo.GetAllVisiblesByChatId(chatId);

            // Assert
            Assert.Equal(2, result.Count);
        }

        [Fact]
        public void GetAllByUserId_SuccessReturn()
        {
            // Arrange
            int userId = 1;
            _repo.Add(new Message
            {
                UserId = userId,
            });
            _repo.Add(new Message
            {
                UserId = 2,
            });

            // Act
            List<Message> result = _repo.GetAllByUserId(userId);

            // Assert
            Assert.Single(result);
        }
    }
}
