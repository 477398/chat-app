﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Data.Contexts;
using Data.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    public class ImageRepositoryTesting
    {
        private readonly Mock<ICloudinary> _cldyMock;
        private readonly ImageRepository _repo;

        public ImageRepositoryTesting()
        {
            _cldyMock = new Mock<ICloudinary>();
            _repo = new ImageRepository(_cldyMock.Object);
        }

        [Fact]
        public async Task UploadPfp_WithIFormFile()
        {
            // Arrange
            Mock<IFormFile> file = new Mock<IFormFile>();

            // Act
            await _repo.UploadImage(file.Object, It.IsAny<string>());

            // Assert
            _cldyMock.Verify(r => r.UploadAsync(It.IsAny<ImageUploadParams>(), null), Times.Once);
        }

        [Fact]
        public async Task UploadPfp_WithImagePath()
        {
            // Arrange
            // Get the image path
            DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            while (dirInfo != null && dirInfo.Name != "WebServer")
            {
                dirInfo = dirInfo.Parent ?? throw new NullReferenceException();
            }
            if (dirInfo == null)
            {
                throw new NullReferenceException();
            }
            string imagePath = dirInfo.FullName + "/Server/Images/default_pfp.png";

            // Act
            await _repo.UploadImage(imagePath, It.IsAny<string>(), It.IsAny<string>());

            // Assert
            _cldyMock.Verify(r => r.UploadAsync(It.IsAny<ImageUploadParams>(), null), Times.Once);
        }
    }
}
