﻿using Data.Contexts;
using Data.Repositories;
using Logic.Entities;
using Microsoft.EntityFrameworkCore;

namespace Test.IntegrationTests.RepositoryToDatabase
{
    // Not using Moq, because you can't mock static methods (extensions) like "IQueryable<t> DbSet<T>.Where()" in GetById().
    // I get around this by setting up a in-memory test database.
    public class RepositoryTesting
    {
        private readonly Repository<User> _repo;
        private readonly PlatformContext _context;

        public RepositoryTesting()
        {
            DbContextOptions<PlatformContext> options = new DbContextOptionsBuilder<PlatformContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()) // Generate a new database everytime to prevent old data interfering with subsequent tests.
                .Options;
            this._context = new PlatformContext(options);
			this._repo = new Repository<User>(this._context);
        }

        [Fact]
        public void Add_ShouldAdd()
        {
            // Arrange
            int id = 1;
            User user = new User
            {
                Id = id,
            };
			int countBefore = this._context.Users.Count();

			// Act
            this._repo.Add(user);

			// Assert
			int countAfter = this._context.Users.Count();
            Assert.Equal(0, countBefore);
            Assert.Equal(1, countAfter);
			User? retrievedUser = _repo.GetById(id);
            Assert.NotNull(retrievedUser);
            Assert.Equal(id, retrievedUser.Id);
        }

        [Fact]
        public void Add_ThrowsArgumentException_WhenIdIsAlreadyTaken()
        {
            // Arrange
            User user = new User
            {
                Id = 1,
            };
            this._repo.Add(user);

            // Act & assert
            Assert.Throws<ArgumentException>(() => this._repo.Add(user));
        }

        [Fact]
        public void GetById_ReturnsEntity()
        {
            // Arrange
            int id = 1;
            User user = new User
            {
                Id = id,
            };
            _repo.Add(user);

            // Act
            User? retrievedUser = _repo.GetById(id);

            // Assert
            Assert.NotNull(retrievedUser);
            Assert.Equal(id, retrievedUser.Id);
        }

        [Fact]
        public void GetById_ReturnsNull()
        {
			// Arrange
			// n/a

			// Act
			User? retrievedUser = _repo.GetById(1);

			// Assert
			Assert.Null(retrievedUser);
		}

        [Fact]
        public void Update_Success()
        {

            // Arrange
            int id = 1;
            string newName = "New";
            User user = new User
            {
                Id = id,
                DisplayName = "Old",
            };
            _repo.Add(user);

            // Act
            user.DisplayName = newName;
            _repo.Update(user);

            // Assert
            User? retrievedUser = _repo.GetById(id);
            Assert.NotNull(retrievedUser);
            Assert.Equal(id, retrievedUser.Id);
            Assert.Equal(newName, retrievedUser.DisplayName);
        }

        [Fact]
        public void Update_ThrowsDbUpdateConcurrencyException_WhenEntityDoesNotExist()
        {
            // Arrange
			User user = new User
			{
				Id = 1,
			};

			// Act & Assert
			Assert.Throws<DbUpdateConcurrencyException>(() => this._repo.Update(user));
		}

        [Fact]
        public void Delete_Success()
        {

            // Arrange
            int id = 1;
            User user = new User
            {
                Id = id,
            };
            this._repo.Add(user);
			User? resultBefore = _repo.GetById(id);
			int countBefore = this._context.Users.Count();

			// Act
			this._repo.Delete(user);

			// Assert
			int countAfter = this._context.Users.Count();
			Assert.Equal(1, countBefore);
			Assert.Equal(0, countAfter);
			Assert.NotNull(resultBefore);
            User? resultAfter = _repo.GetById(id);
            Assert.Null(resultAfter);

        }

        [Fact]
        public void Delete_ThrowsDbUpdateConcurrencyException_WhenEntityDoesNotExist()
        {
            // Arrange
            User user = new User
            {
                Id = 1,
            };

			// Act & Assert
			Assert.Throws<DbUpdateConcurrencyException>(() => this._repo.Update(user));
		}
    };
}

