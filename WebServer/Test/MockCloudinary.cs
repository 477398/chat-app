﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Logic.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
	public class MockCloudinary : ICloudinary
	{
		public MetadataFieldResult AddMetadataField<T>(MetadataFieldCreateParams<T> parameters)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataFieldResult> AddMetadataFieldAsync<T>(MetadataFieldCreateParams<T> parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public AddRelatedResourcesResult AddRelatedResources(AddRelatedResourcesParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<AddRelatedResourcesResult> AddRelatedResourcesAsync(AddRelatedResourcesParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public AddRelatedResourcesResult AddRelatedResourcesByAssetIds(AddRelatedResourcesByAssetIdsParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<AddRelatedResourcesResult> AddRelatedResourcesByAssetIdsAsync(AddRelatedResourcesByAssetIdsParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ContextResult Context(ContextParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ContextResult> ContextAsync(ContextParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ArchiveResult CreateArchive(ArchiveParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ArchiveResult> CreateArchiveAsync(ArchiveParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public CreateFolderResult CreateFolder(string folder)
		{
			throw new NotImplementedException();
		}

		public Task<CreateFolderResult> CreateFolderAsync(string folder, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public CreateSlideshowResult CreateSlideshow(CreateSlideshowParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<CreateSlideshowResult> CreateSlideshowAsync(CreateSlideshowParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public StreamingProfileResult CreateStreamingProfile(StreamingProfileCreateParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<StreamingProfileResult> CreateStreamingProfileAsync(StreamingProfileCreateParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public TransformResult CreateTransform(CreateTransformParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<TransformResult> CreateTransformAsync(CreateTransformParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadMappingResults CreateUploadMapping(string folder, string template)
		{
			throw new NotImplementedException();
		}

		public Task<UploadMappingResults> CreateUploadMappingAsync(string folder, string template, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadPresetResult CreateUploadPreset(UploadPresetParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UploadPresetResult> CreateUploadPresetAsync(UploadPresetParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ArchiveResult CreateZip(ArchiveParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ArchiveResult> CreateZipAsync(ArchiveParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteAllResources()
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteAllResources(bool keepOriginal, string nextCursor)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteAllResourcesAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteAllResourcesAsync(bool keepOriginal, string nextCursor, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelDerivedResResult DeleteDerivedResources(params string[] ids)
		{
			throw new NotImplementedException();
		}

		public DelDerivedResResult DeleteDerivedResources(DelDerivedResParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<DelDerivedResResult> DeleteDerivedResourcesAsync(params string[] ids)
		{
			throw new NotImplementedException();
		}

		public Task<DelDerivedResResult> DeleteDerivedResourcesAsync(DelDerivedResParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelDerivedResResult DeleteDerivedResourcesByTransform(DelDerivedResParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<DelDerivedResResult> DeleteDerivedResourcesByTransformAsync(DelDerivedResParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DeleteFolderResult DeleteFolder(string folder)
		{
			throw new NotImplementedException();
		}

		public Task<DeleteFolderResult> DeleteFolderAsync(string folder, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataDataSourceResult DeleteMetadataDataSourceEntries(string fieldExternalId, List<string> entriesExternalIds)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataDataSourceResult> DeleteMetadataDataSourceEntriesAsync(string fieldExternalId, List<string> entriesExternalIds, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelMetadataFieldResult DeleteMetadataField(string fieldExternalId)
		{
			throw new NotImplementedException();
		}

		public Task<DelMetadataFieldResult> DeleteMetadataFieldAsync(string fieldExternalId, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DeleteRelatedResourcesResult DeleteRelatedResources(DeleteRelatedResourcesParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<DeleteRelatedResourcesResult> DeleteRelatedResourcesAsync(DeleteRelatedResourcesParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DeleteRelatedResourcesResult DeleteRelatedResourcesByAssetIds(DeleteRelatedResourcesByAssetIdsParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<DeleteRelatedResourcesResult> DeleteRelatedResourcesByAssetIdsAsync(DeleteRelatedResourcesByAssetIdsParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResources(ResourceType type, params string[] publicIds)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResources(params string[] publicIds)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResources(DelResParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesAsync(ResourceType type, params string[] publicIds)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesAsync(params string[] publicIds)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesAsync(DelResParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResourcesByPrefix(string prefix)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResourcesByPrefix(string prefix, bool keepOriginal, string nextCursor)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesByPrefixAsync(string prefix, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesByPrefixAsync(string prefix, bool keepOriginal, string nextCursor, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResourcesByTag(string tag)
		{
			throw new NotImplementedException();
		}

		public DelResResult DeleteResourcesByTag(string tag, bool keepOriginal, string nextCursor)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesByTagAsync(string tag, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<DelResResult> DeleteResourcesByTagAsync(string tag, bool keepOriginal, string nextCursor, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public StreamingProfileResult DeleteStreamingProfile(string name)
		{
			throw new NotImplementedException();
		}

		public Task<StreamingProfileResult> DeleteStreamingProfileAsync(string name, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public TransformResult DeleteTransform(string transformName)
		{
			throw new NotImplementedException();
		}

		public Task<TransformResult> DeleteTransformAsync(string transformName, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadMappingResults DeleteUploadMapping()
		{
			throw new NotImplementedException();
		}

		public UploadMappingResults DeleteUploadMapping(string folder)
		{
			throw new NotImplementedException();
		}

		public Task<UploadMappingResults> DeleteUploadMappingAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<UploadMappingResults> DeleteUploadMappingAsync(string folder, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DeleteUploadPresetResult DeleteUploadPreset(string name)
		{
			throw new NotImplementedException();
		}

		public Task<DeleteUploadPresetResult> DeleteUploadPresetAsync(string name, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public DeletionResult Destroy(DeletionParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<DeletionResult> DestroyAsync(DeletionParams parameters)
		{
			throw new NotImplementedException();
		}

		public string DownloadArchiveUrl(ArchiveParams parameters)
		{
			throw new NotImplementedException();
		}

		public string DownloadBackedUpAsset(string assetId, string versionId)
		{
			throw new NotImplementedException();
		}

		public string DownloadFolder(string folderPath, ArchiveParams? parameters = null)
		{
			throw new NotImplementedException();
		}

		public string DownloadMulti(MultiParams parameters)
		{
			throw new NotImplementedException();
		}

		public string DownloadPrivate(string publicId, bool? attachment = null, string format = "", string type = "", long? expiresAt = null, string resourceType = "image")
		{
			throw new NotImplementedException();
		}

		public string DownloadSprite(SpriteParams parameters)
		{
			throw new NotImplementedException();
		}

		public string DownloadZip(string tag, Transformation transform, string resourceType = "image")
		{
			throw new NotImplementedException();
		}

		public ExplicitResult Explicit(ExplicitParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ExplicitResult> ExplicitAsync(ExplicitParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ExplodeResult Explode(ExplodeParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ExplodeResult> ExplodeAsync(ExplodeParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataFieldResult GetMetadataField(string fieldExternalId)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataFieldResult> GetMetadataFieldAsync(string fieldExternalId, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public GetResourceResult GetResource(string publicId)
		{
			throw new NotImplementedException();
		}

		public GetResourceResult GetResource(GetResourceParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<GetResourceResult> GetResourceAsync(string publicId, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetResourceResult> GetResourceAsync(GetResourceParamsBase parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public GetResourceResult GetResourceByAssetId(string assetId)
		{
			throw new NotImplementedException();
		}

		public Task<GetResourceResult> GetResourceByAssetIdAsync(string assetId, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public StreamingProfileResult GetStreamingProfile(string name)
		{
			throw new NotImplementedException();
		}

		public Task<StreamingProfileResult> GetStreamingProfileAsync(string name, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public GetTransformResult GetTransform(string transform)
		{
			throw new NotImplementedException();
		}

		public GetTransformResult GetTransform(GetTransformParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<GetTransformResult> GetTransformAsync(string transform, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetTransformResult> GetTransformAsync(GetTransformParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public GetUploadPresetResult GetUploadPreset(string name)
		{
			throw new NotImplementedException();
		}

		public Task<GetUploadPresetResult> GetUploadPresetAsync(string name, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UsageResult GetUsage(DateTime? date = null)
		{
			throw new NotImplementedException();
		}

		public Task<UsageResult> GetUsageAsync(DateTime? date, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<UsageResult> GetUsageAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataFieldListResult ListMetadataFields()
		{
			throw new NotImplementedException();
		}

		public Task<MetadataFieldListResult> ListMetadataFieldsAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourceByAssetFolderAsync(string assetFolder, bool tags, bool context, bool moderations, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourceByAssetIdsAsync(IEnumerable<string> assetIds, bool tags, bool context, bool moderations, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourceByPublicIds(IEnumerable<string> publicIds, bool tags, bool context, bool moderations)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourceByPublicIdsAsync(IEnumerable<string> publicIds, bool tags, bool context, bool moderations, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResources(string? nextCursor = null, bool tags = true, bool context = true, bool moderations = true)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResources(ListResourcesParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesAsync(string? nextCursor = null, bool tags = true, bool context = true, bool moderations = true, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesAsync(ListResourcesParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByAssetFolder(string assetFolder, bool tags = false, bool context = false, bool moderations = false)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByAssetIDs(IEnumerable<string> assetIds, bool tags, bool context, bool moderations)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByContext(string key, string value = "", bool tags = false, bool context = false, string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByContextAsync(string key, string value = "", bool tags = false, bool context = false, string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByModerationStatus(string kind, ModerationStatus status, bool tags = true, bool context = true, bool moderations = true, string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByModerationStatusAsync(string kind, ModerationStatus status, bool tags = true, bool context = true, bool moderations = true, string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByPrefix(string prefix, string type = "upload", string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByPrefix(string prefix, bool tags, bool context, bool moderations, string type = "upload", string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByPrefixAsync(string prefix, string type = "upload", string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByPrefixAsync(string prefix, bool tags, bool context, bool moderations, string type = "upload", string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByPublicIds(IEnumerable<string> publicIds)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByPublicIdsAsync(IEnumerable<string> publicIds, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByTag(string tag, string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByTagAsync(string tag, string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourcesResult ListResourcesByType(string type, string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListResourcesResult> ListResourcesByTypeAsync(string type, string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListResourceTypesResult ListResourceTypes()
		{
			throw new NotImplementedException();
		}

		public Task<ListResourceTypesResult> ListResourceTypesAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public StreamingProfileListResult ListStreamingProfiles()
		{
			throw new NotImplementedException();
		}

		public Task<StreamingProfileListResult> ListStreamingProfilesAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListTagsResult ListTags()
		{
			throw new NotImplementedException();
		}

		public ListTagsResult ListTags(ListTagsParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ListTagsResult> ListTagsAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListTagsResult> ListTagsAsync(ListTagsParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListTagsResult ListTagsByPrefix(string prefix)
		{
			throw new NotImplementedException();
		}

		public Task<ListTagsResult> ListTagsByPrefixAsync(string prefix, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListTransformsResult ListTransformations()
		{
			throw new NotImplementedException();
		}

		public ListTransformsResult ListTransformations(ListTransformsParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ListTransformsResult> ListTransformationsAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListTransformsResult> ListTransformationsAsync(ListTransformsParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ListUploadPresetsResult ListUploadPresets(string? nextCursor = null)
		{
			throw new NotImplementedException();
		}

		public ListUploadPresetsResult ListUploadPresets(ListUploadPresetsParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<ListUploadPresetsResult> ListUploadPresetsAsync(string? nextCursor = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ListUploadPresetsResult> ListUploadPresetsAsync(ListUploadPresetsParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public SpriteResult MakeSprite(SpriteParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<SpriteResult> MakeSpriteAsync(SpriteParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MultiResult Multi(MultiParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<MultiResult> MultiAsync(MultiParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public PingResult Ping()
		{
			throw new NotImplementedException();
		}

		public Task<PingResult> PingAsync(CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public PublishResourceResult PublishResourceByIds(string tag, PublishResourceParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<PublishResourceResult> PublishResourceByIdsAsync(string tag, PublishResourceParams parameters, CancellationToken? cancellationToken)
		{
			throw new NotImplementedException();
		}

		public PublishResourceResult PublishResourceByPrefix(string prefix, PublishResourceParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<PublishResourceResult> PublishResourceByPrefixAsync(string prefix, PublishResourceParams parameters, CancellationToken? cancellationToken)
		{
			throw new NotImplementedException();
		}

		public PublishResourceResult PublishResourceByTag(string tag, PublishResourceParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<PublishResourceResult> PublishResourceByTagAsync(string tag, PublishResourceParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public RenameResult Rename(string fromPublicId, string toPublicId, bool overwrite = false)
		{
			throw new NotImplementedException();
		}

		public RenameResult Rename(RenameParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<RenameResult> RenameAsync(string fromPublicId, string toPublicId, bool overwrite = false, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<RenameResult> RenameAsync(RenameParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataDataSourceResult ReorderMetadataFieldDatasource(string fieldExternalId, string orderBy, string? direction = null)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataDataSourceResult> ReorderMetadataFieldDatasourceAsync(string fieldExternalId, string orderBy, string? direction = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataFieldListResult ReorderMetadataFields(MetadataReorderBy orderBy, MetadataReorderDirection? direction = null)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataFieldListResult> ReorderMetadataFieldsAsync(MetadataReorderBy orderBy, MetadataReorderDirection? direction = null)
		{
			throw new NotImplementedException();
		}

		public RestoreResult Restore(params string[] publicIds)
		{
			throw new NotImplementedException();
		}

		public RestoreResult Restore(RestoreParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<RestoreResult> RestoreAsync(params string[] publicIds)
		{
			throw new NotImplementedException();
		}

		public Task<RestoreResult> RestoreAsync(RestoreParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataDataSourceResult RestoreMetadataDataSourceEntries(string fieldExternalId, List<string> entriesExternalIds)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataDataSourceResult> RestoreMetadataDataSourceEntriesAsync(string fieldExternalId, List<string> entriesExternalIds, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public GetFoldersResult RootFolders(GetFoldersParams? parameters = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetFoldersResult> RootFoldersAsync(GetFoldersParams? parameters = null, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Search Search()
		{
			throw new NotImplementedException();
		}

		public SearchFolders SearchFolders()
		{
			throw new NotImplementedException();
		}

		public GetFoldersResult SubFolders(string folder, GetFoldersParams? parameters = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetFoldersResult> SubFoldersAsync(string folder, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetFoldersResult> SubFoldersAsync(string folder, GetFoldersParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public TagResult Tag(TagParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<TagResult> TagAsync(TagParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public TextResult Text(string text)
		{
			throw new NotImplementedException();
		}

		public TextResult Text(TextParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<TextResult> TextAsync(string text, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<TextResult> TextAsync(TextParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataUpdateResult UpdateMetadata(MetadataUpdateParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataUpdateResult> UpdateMetadataAsync(MetadataUpdateParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataDataSourceResult UpdateMetadataDataSourceEntries(string fieldExternalId, MetadataDataSourceParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataDataSourceResult> UpdateMetadataDataSourceEntriesAsync(string fieldExternalId, MetadataDataSourceParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public MetadataFieldResult UpdateMetadataField<T>(string fieldExternalId, MetadataFieldUpdateParams<T> parameters)
		{
			throw new NotImplementedException();
		}

		public Task<MetadataFieldResult> UpdateMetadataFieldAsync<T>(string fieldExternalId, MetadataFieldUpdateParams<T> parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public GetResourceResult UpdateResource(string publicId, ModerationStatus moderationStatus)
		{
			throw new NotImplementedException();
		}

		public GetResourceResult UpdateResource(UpdateParams parameters)
		{
			throw new NotImplementedException();
		}

		public UpdateResourceAccessModeResult UpdateResourceAccessModeByIds(UpdateResourceAccessModeParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UpdateResourceAccessModeResult> UpdateResourceAccessModeByIdsAsync(UpdateResourceAccessModeParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UpdateResourceAccessModeResult UpdateResourceAccessModeByPrefix(string prefix, UpdateResourceAccessModeParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UpdateResourceAccessModeResult> UpdateResourceAccessModeByPrefixAsync(string prefix, UpdateResourceAccessModeParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UpdateResourceAccessModeResult UpdateResourceAccessModeByTag(string tag, UpdateResourceAccessModeParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UpdateResourceAccessModeResult> UpdateResourceAccessModeByTagAsync(string tag, UpdateResourceAccessModeParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetResourceResult> UpdateResourceAsync(string publicId, ModerationStatus moderationStatus, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<GetResourceResult> UpdateResourceAsync(UpdateParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public StreamingProfileResult UpdateStreamingProfile(string name, StreamingProfileUpdateParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<StreamingProfileResult> UpdateStreamingProfileAsync(string name, StreamingProfileUpdateParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UpdateTransformResult UpdateTransform(UpdateTransformParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UpdateTransformResult> UpdateTransformAsync(UpdateTransformParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadMappingResults UpdateUploadMapping(string folder, string newTemplate)
		{
			throw new NotImplementedException();
		}

		public Task<UploadMappingResults> UpdateUploadMappingAsync(string folder, string newTemplate, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadPresetResult UpdateUploadPreset(UploadPresetParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UploadPresetResult> UpdateUploadPresetAsync(UploadPresetParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public ImageUploadResult Upload(ImageUploadParams parameters)
		{
			throw new NotImplementedException();
		}

		public VideoUploadResult Upload(VideoUploadParams parameters)
		{
			throw new NotImplementedException();
		}

		public RawUploadResult Upload(string resourceType, IDictionary<string, object> parameters, FileDescription fileDescription)
		{
			throw new NotImplementedException();
		}

		public RawUploadResult Upload(RawUploadParams parameters, string type = "auto")
		{
			throw new NotImplementedException();
		}

		public async Task<ImageUploadResult> UploadAsync(ImageUploadParams parameters, CancellationToken? cancellationToken = null)
		{
			await Task.Delay(10);
			return new ImageUploadResult();
		}

		public async Task<VideoUploadResult> UploadAsync(VideoUploadParams parameters, CancellationToken? cancellationToken = null)
		{
			await Task.Delay(10);
			return new VideoUploadResult();
		}

		public Task<RawUploadResult> UploadAsync(string resourceType, IDictionary<string, object> parameters, FileDescription fileDescription, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<RawUploadResult> UploadAsync(RawUploadParams parameters, string type = "auto", CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public T UploadChunk<T>(BasicRawUploadParams parameters) where T : UploadResult, new()
		{
			throw new NotImplementedException();
		}

		public RawUploadResult UploadChunk(RawUploadParams parameters)
		{
			throw new NotImplementedException();
		}

		public ImageUploadResult UploadChunk(ImageUploadParams parameters)
		{
			throw new NotImplementedException();
		}

		public VideoUploadResult UploadChunk(VideoUploadParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<T> UploadChunkAsync<T>(BasicRawUploadParams parameters, CancellationToken? cancellationToken = null) where T : UploadResult, new()
		{
			throw new NotImplementedException();
		}

		public Task<RawUploadResult> UploadChunkAsync(RawUploadParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ImageUploadResult> UploadChunkAsync(ImageUploadParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<VideoUploadResult> UploadChunkAsync(VideoUploadParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public RawUploadResult UploadLarge(RawUploadParams parameters, int bufferSize = 0)
		{
			throw new NotImplementedException();
		}

		public ImageUploadResult UploadLarge(ImageUploadParams parameters, int bufferSize = 0)
		{
			throw new NotImplementedException();
		}

		public VideoUploadResult UploadLarge(VideoUploadParams parameters, int bufferSize = 0)
		{
			throw new NotImplementedException();
		}

		public RawUploadResult UploadLarge(AutoUploadParams parameters, int bufferSize = 0)
		{
			throw new NotImplementedException();
		}

		public UploadResult UploadLarge(BasicRawUploadParams parameters, int bufferSize = 0, bool isRaw = false)
		{
			throw new NotImplementedException();
		}

		public T UploadLarge<T>(BasicRawUploadParams parameters, int bufferSize = 0) where T : UploadResult, new()
		{
			throw new NotImplementedException();
		}

		public Task<RawUploadResult> UploadLargeAsync(RawUploadParams parameters, int bufferSize = 0, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<ImageUploadResult> UploadLargeAsync(ImageUploadParams parameters, int bufferSize = 0, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<VideoUploadResult> UploadLargeAsync(VideoUploadParams parameters, int bufferSize = 0, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<RawUploadResult> UploadLargeAsync(AutoUploadParams parameters, int bufferSize = 0, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public Task<T> UploadLargeAsync<T>(BasicRawUploadParams parameters, int bufferSize = 0, CancellationToken? cancellationToken = null) where T : UploadResult, new()
		{
			throw new NotImplementedException();
		}

		public Task<T> UploadLargeAsync<T>(BasicRawUploadParams parameters, int bufferSize = 0, int maxConcurrentUploads = 1, CancellationToken? cancellationToken = null) where T : UploadResult, new()
		{
			throw new NotImplementedException();
		}

		public RawUploadResult UploadLargeRaw(BasicRawUploadParams parameters, int bufferSize = 0)
		{
			throw new NotImplementedException();
		}

		public Task<RawUploadResult> UploadLargeRawAsync(BasicRawUploadParams parameters, int bufferSize = 0, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadMappingResults UploadMapping(string folder)
		{
			throw new NotImplementedException();
		}

		public Task<UploadMappingResults> UploadMappingAsync(string folder, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public UploadMappingResults UploadMappings(UploadMappingParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<UploadMappingResults> UploadMappingsAsync(UploadMappingParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}

		public VisualSearchResult VisualSearch(VisualSearchParams parameters)
		{
			throw new NotImplementedException();
		}

		public Task<VisualSearchResult> VisualSearchAsync(VisualSearchParams parameters, CancellationToken? cancellationToken = null)
		{
			throw new NotImplementedException();
		}
	}
}
