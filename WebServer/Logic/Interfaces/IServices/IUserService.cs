﻿using Logic.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IServices
{
	public interface IUserService
	{
		public User CreateUser(string loginName, string displayName, string email, string unhashedPassword, string unhashedPasswordConfirm);
		public User GetUserById(int id);
		public void UpdateUser(User user);
		public List<Message> GetMessagesByUserId(int userId);
		public Task UploadPfp(IFormFile file, int userId);
		public Task UploadDefaultPfp(int userId);
	}
}
