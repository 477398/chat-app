﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IServices
{
	public interface IChatService
	{
		public Chat CreatePrivateChat(int userId, int friendUserId);
		public Chat GetPrivateChatByUserIds(int userId, int friendUserId);
		public void ValidateUserIsAChatMember(int chatId, int userId);
		public bool IsUserAChatMember(int chatId, int userId);
		public List<ChatUser> GetChatMembers(int chatId);
		public Chat CreateGroupChat(int founderUserId);
		public List<Chat> GetGroupChatsWithUserInIt(int userId);
		public void LeaveGroupChat(int chatId, int userId);
		public ChatUser GetChatUserByChatIdAndUserId(int chatId, int userId);
		public void SendGroupChatInvitation(int chatId, int userId);
		public List<ChatUser> GetGroupInvitationsByUserId(int userId);
		public Chat GetChatById(int chatId);
		public void AnswerGroupChatInvitation(int chatId, int userId, bool accept);
		public bool IsAllowedToKick(int chatId, int userId, int kickUserId);
		public void ChangeChatName(int chatId, string newName);
		public bool IsAllowedToChangeChatName(int chatId, int userId);
	}
}
