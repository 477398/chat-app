﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IServices
{
	public interface IMessageService
	{
		public List<Message> GetAllVisibleMessagesByChatId(int chatId);
		public Message CreateMessage(int userId, int chatId, string content);
		public bool IsAllowedToEditMessage(int msgId, int userId);
		public void EditMessage(int msgId, string newContent);
		public bool IsAllowedToHideMessage(int msgId, int userId);
		public void HideMessage(int msgId);
	}
}
