﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IServices
{
	public interface IAssocService
	{
		public Association CreateAssociation(int primaryUserId, int secondaryUserId);
		public List<User> GetOtherUsersByPartialDisplayName(int ignoreId, string partialDisplayName);
		public Association GetAssociationByUserIds(int primaryUserId, int secondaryUserId);
		public List<Association> GetIncomingAssociationsByPrimaryUserId(int primaryUserId);
		public List<Association> GetOutgoingAssociationsByPrimaryUserId(int primaryUserId);
		public void CreateFriendRequest(int senderUserId, int receiverUserId);
		public void AnswerFriendRequest(int receiverUserId, int senderUserId, bool accept);
		public List<Association> GetFriendsByPrimaryUserId(int primaryUserId);
		public void Unfriend(int primaryUserId, int secondaryUserId);

	}
}
