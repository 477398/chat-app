﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IServices
{
	public interface IAuthService
	{
		public void AuthenticateUser(User user, string hashedPassword);
		public User AuthenticateUserByLoginName(string loginName, string hashedPassword);
		public User AuthenticateUserByEmail(string email, string hashedPassword);
		public string GenerateJWT(int userId);
		public int GetUserIdFromClaimPrincipal(ClaimsPrincipal principal);
	}
}
