﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IRepositories
{
    public interface IChatRepository : IRepository<Chat>
    {
        public List<Chat> GetAllPrivateChats();
        public List<Chat> GetAllGroupChats();
    }
}
