﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IRepositories
{
    public interface IChatUserRepository : IRepository<ChatUser>
    {
        public List<ChatUser> GetAllMembersByChatId(int chatId);
        public ChatUser? GetByChatIdAndUserId(int chatId, int userId);
        public List<ChatUser> GetAllPendingByUserId(int userId);
    }
}
