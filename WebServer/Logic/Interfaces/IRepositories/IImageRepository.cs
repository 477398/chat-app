﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IRepositories
{
    public interface IImageRepository
    {
        public Task UploadImage(IFormFile file, string publicId);

        public Task UploadImage(string imagePath, string fileName, string publicId);
    }
}
