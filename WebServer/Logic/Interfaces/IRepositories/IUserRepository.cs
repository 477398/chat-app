﻿using Logic.Entities;

namespace Logic.Interfaces.IRepositories
{
    public interface IUserRepository : IRepository<User>
    {
        public User? GetUserByLoginName(string loginName);
        public User? GetUserByEmail(string email);
        public List<User> GetAllByPartialDisplayName(string partialDisplayName);
    }
}
