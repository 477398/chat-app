﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IRepositories
{
    public interface IMessageRepository : IRepository<Message>
    {
        public List<Message> GetAllVisiblesByChatId(int chatId);

        public List<Message> GetAllByUserId(int userId);
    }
}
