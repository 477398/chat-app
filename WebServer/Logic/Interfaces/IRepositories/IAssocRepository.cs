﻿using Logic.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Interfaces.IRepositories
{
    public interface IAssocRepository : IRepository<Association>
    {
        public Association? GetByUserIds(int primaryUserId, int secondaryUserId);
        public List<Association> GetIncomingsByPrimaryUserId(int primaryUserId);
        public List<Association> GetOutgoingsByPrimaryUserId(int primaryUserId);
        public List<Association> GetFriendsByPrimaryUserId(int primaryUserId);
    }
}
