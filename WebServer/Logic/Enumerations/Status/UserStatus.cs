﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Enumerations.Status
{
    public enum UserStatus
    {
        TerminatedByAdmin = -2,
        DeactivatedByUser = -1,
        Active = 0,
    }
}
