﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Enumerations.Status
{
    public enum MessageStatus
    {
        DeletedByAdmin = -2,
        Deleted = -1,
        Displaying = 0,
        DisplayingModified = 1,
    }
}
