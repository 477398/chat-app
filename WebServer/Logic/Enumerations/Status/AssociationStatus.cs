﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Enumerations.Status
{
    public enum AssociationStatus
    {
        Blocked = -1,
        Dissociated = 0,
        PendingSender = 1,
        PendingReceiver = 2,
        Consociated = 3,
    }
}
