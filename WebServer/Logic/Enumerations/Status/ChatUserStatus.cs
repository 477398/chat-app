﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Enumerations.Status
{
    public enum ChatUserStatus
    {
        Dissociated = -1,
        Pending = 0,
        JoinedAsMember = 1,
        JoinedAsModerator = 2,
        JoinedAsFounder = 3,

    }
}
