﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Enumerations.Status
{
    public enum ChatStatus
    {
        TerminatedByAdmin = -2,
        Deserted = -1,
        Active = 0,
    }
}
