﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Utilities
{
    public class PasswordHasher
    {
        public static string Hash(string unhashedPassword)
        {
            using (SHA256 sha256 = SHA256.Create()) 
            {
                byte[] hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(unhashedPassword));
                string hashedPassword = "";
                for (int i = 0; i < hashedBytes.Length;i ++)
                {
                    hashedPassword = hashedPassword + hashedBytes[i].ToString("x2");
                }
                return hashedPassword;
            }
        }
    }
}
