﻿using Logic.Enumerations.Status;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Entities
{
    public class Message : Entity
    {
        [Column] public int ChatId { get; set; }
        [Column] public int UserId { get; set; }
        [Column] public string Content { get; set; } = string.Empty;
        [Column] public MessageStatus Status { get; set; } = MessageStatus.Displaying;

    }
}
