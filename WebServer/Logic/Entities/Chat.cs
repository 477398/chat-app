﻿using Logic.Enumerations;
using Logic.Enumerations.Status;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Entities
{
    public class Chat : Entity
    {
        [Column] public string DisplayName { get; set; } = "";
        [Column] public string Description { get; set; } = "";
        [Column] public ChatType Type { get; set; } = ChatType.Private;
        [Column] public ChatStatus Status { get; set; } = ChatStatus.Active;
    }
}
