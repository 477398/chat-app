﻿using Logic.Enumerations.Status;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Entities
{
    public class Entity
    {
        [Key] public int Id { get; set; } = 0;
        [Column] public DateTime CreationDate { get; set; }
        [Column] public DateTime ModifiedDate { get; set; }
    }
}
