﻿using Logic.Enumerations;
using Logic.Enumerations.Status;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Entities
{
    public class ChatUser : Entity
    {
        [Column] public int ChatId { get; set; }
        [Column] public int UserId { get; set; }
        [Column] public ChatUserStatus Status { get; set; } = ChatUserStatus.Dissociated;
    }
}
