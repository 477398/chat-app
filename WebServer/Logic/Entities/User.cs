﻿using Logic.Enumerations.Status;
using System.ComponentModel.DataAnnotations.Schema;

namespace Logic.Entities
{
    public class User : Entity
    {
        [Column] public string LoginName { get; set; } = string.Empty;
        [Column] public string DisplayName { get; set; } = string.Empty;
        [Column] public string Email { get; set; } = string.Empty;
        [Column] public string HashedPassword { get; set; } = string.Empty;
        [Column] public UserStatus Status { get; set; } = UserStatus.Active;
    }
}
