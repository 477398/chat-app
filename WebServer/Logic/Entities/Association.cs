﻿using Logic.Enumerations.Status;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Entities
{
    public class Association : Entity
    {
        [Column] public int PrimaryUserId {  get; set; }
        [Column] public int SecondaryUserId {  get; set; }
        [Column] public AssociationStatus Status { get; set; }

    }
}
