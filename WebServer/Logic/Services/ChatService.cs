﻿using Logic.Entities;
using Logic.Enumerations;
using Logic.Enumerations.Status;
using System.Data;
using Logic.Exceptions;
using System.ComponentModel;
using Logic.Interfaces.IRepositories;
using Logic.Interfaces.IServices;

namespace Logic.Services
{
	public class ChatService : IChatService
	{
		private readonly IChatRepository _chatRepo;
		private readonly IChatUserRepository _chatUserRepo;
		private readonly IMessageRepository _messageRepo;

		public ChatService(IChatRepository chatRepo, IChatUserRepository chatUserRepo, IMessageRepository messageRepo)
		{
			this._chatRepo = chatRepo;
			this._chatUserRepo = chatUserRepo;
			this._messageRepo = messageRepo;
		}

		public Chat CreatePrivateChat(int userId, int friendUserId)
		{
			// Creating the private chat
			Chat chat = new Chat
			{
				DisplayName = "Private Chat",
				Description = $"Private Chat of User {userId} and {friendUserId}",
				Type = ChatType.Private,
			};
			this._chatRepo.Add(chat);

			// Adding the two users to the chat
			ChatUser yourChatUser = new ChatUser
			{
				UserId = userId,
				ChatId = chat.Id,
				Status = ChatUserStatus.JoinedAsMember,
			};
			ChatUser friendChatUser = new ChatUser
			{
				UserId = friendUserId,
				ChatId = chat.Id,
				Status = ChatUserStatus.JoinedAsMember,
			};
			this._chatUserRepo.Add(yourChatUser);
			this._chatUserRepo.Add(friendChatUser);

			// return chat
			return chat;
		}

		public Chat GetPrivateChatByUserIds(int userId, int friendUserId)
		{
			List<Chat> privateChats = this._chatRepo.GetAllPrivateChats();
			Chat? returnChat = null;
			foreach (Chat chat in privateChats)
			{
				List<ChatUser> chatUsers = this._chatUserRepo.GetAllMembersByChatId(chat.Id);
				if (chatUsers.Any(x => x.UserId == userId) && chatUsers.Any(x => x.UserId == friendUserId))
				{
					returnChat = chat;
					break;
				}
			}

			if (returnChat == null)
			{
				return this.CreatePrivateChat(userId, friendUserId);
			}
			else
			{
				return returnChat;
			}
		}

		public void ValidateUserIsAChatMember(int chatId, int userId)
		{
			List<ChatUser> chatUserList = this._chatUserRepo.GetAllMembersByChatId(chatId);
			ChatUser? chatUser = chatUserList.FirstOrDefault(x => x.UserId == userId) ?? throw new ChatException("ChatUser does not exist.");
			if (chatUser.Status < ChatUserStatus.JoinedAsMember)
			{
				throw new ChatException("User is not a member of the chat.");
			}
		}

		public bool IsUserAChatMember(int chatId, int userId)
		{
			try
			{
				this.ValidateUserIsAChatMember(chatId, userId);
				return true;
			}
			catch (ChatException)
			{
				return false;
			}
		}

		public List<ChatUser> GetChatMembers(int chatId)
		{
			return this._chatUserRepo.GetAllMembersByChatId(chatId);
		}

		public Chat CreateGroupChat(int founderUserId)
		{
			// Create the new group chat
			Chat newChat = new Chat
			{
				DisplayName = "New Group Chat",
				Description = "A group chat",
				Type = ChatType.Group,
				Status = ChatStatus.Active,
			};
			this._chatRepo.Add(newChat);

			// Add the user to this chat as Founder.
			ChatUser founderChatUser = new ChatUser
			{
				ChatId = newChat.Id,
				UserId = founderUserId,
				Status = ChatUserStatus.JoinedAsFounder,
			};
			this._chatUserRepo.Add(founderChatUser);

			// Return
			return newChat;
		}

		public List<Chat> GetGroupChatsWithUserInIt(int userId)
		{
			List<Chat> allGroupChats = this._chatRepo.GetAllGroupChats();
			// Seperate active group chats from deserted/terminated group chats.
			List<Chat> activeGroupChats = allGroupChats.Where(x => x.Status >= ChatStatus.Active).ToList();
			// Get the group chats the user is in it
			List<Chat> groupChatsWithUserInIt = activeGroupChats.Where(c => this.IsUserAChatMember(c.Id, userId)).ToList();
			return groupChatsWithUserInIt;
		}

		public void LeaveGroupChat(int chatId, int userId)
		{
			// Get ChatUser
			ChatUser chatUser = this._chatUserRepo.GetByChatIdAndUserId(chatId, userId) ?? throw new ChatException("ChatUser does not exist.");

			// Validation
			if (chatUser.Status < ChatUserStatus.JoinedAsMember) throw new ChatException("User is not a member of the chat; unable to leave.");

			// Update ChatUser as if the user left the chat
			ChatUserStatus prevStatus = chatUser.Status; // Needed for later to transfer Founder (if needed).
			chatUser.Status = ChatUserStatus.Dissociated;
			this._chatUserRepo.Update(chatUser);

			// Update the state of the chat
			Chat chat = this._chatRepo.GetById(chatId) ?? throw new ChatException("Chat does not exist.");
			List<ChatUser> chatUsers = this._chatUserRepo.GetAllMembersByChatId(chatId);
			// Any users left in it?
			if (chatUsers.Count <= 0)
			{
				// Mark chat as deserted; no users in it
				chat.Status = ChatStatus.Deserted;
				this._chatRepo.Update(chat);
			}
			else
			{
				// Check if this user was the founder, if so then transfer it's founder status to the oldest member
				if (prevStatus == ChatUserStatus.JoinedAsFounder)
				{
					ChatUser oldestMember = chatUsers.OrderBy(x => x.CreationDate).First();
					oldestMember.Status = ChatUserStatus.JoinedAsFounder;
					this._chatUserRepo.Update(oldestMember);
				}
			}
		}

		public ChatUser GetChatUserByChatIdAndUserId(int chatId, int userId)
		{
			ChatUser? cu = this._chatUserRepo.GetByChatIdAndUserId(chatId, userId);

			if (cu == null)
			{
				cu = new ChatUser
				{
					ChatId = chatId,
					UserId = userId,
					Status = ChatUserStatus.Dissociated,
				};
				this._chatUserRepo.Add(cu);
			}

			return cu;
		}

		public void SendGroupChatInvitation(int chatId, int userId)
		{
			ChatUser cu = this.GetChatUserByChatIdAndUserId(chatId, userId);

			// Check if the user is in a state to be able to receive invitations
			if (cu.Status >= ChatUserStatus.JoinedAsMember) throw new ChatException("User cannot receive groupchat invitations, because user is already a member of the group chat.");

			cu.Status = ChatUserStatus.Pending;
			this._chatUserRepo.Update(cu);
		}

		public List<ChatUser> GetGroupInvitationsByUserId(int userId)
		{
			return this._chatUserRepo.GetAllPendingByUserId(userId);
		}

		public Chat GetChatById(int chatId)
		{
			return this._chatRepo.GetById(chatId) ?? throw new ChatException("Chat does not exist.");
		}

		public void AnswerGroupChatInvitation(int chatId, int userId, bool accept)
		{
			ChatUser inv = this._chatUserRepo.GetByChatIdAndUserId(chatId, userId) ?? throw new Exception("Chat User does not exist.");

			if (inv.Status == ChatUserStatus.Pending)
			{
				if (accept)
				{
					inv.Status = ChatUserStatus.JoinedAsMember;
				}
				else
				{
					inv.Status = ChatUserStatus.Dissociated;
				}
				this._chatUserRepo.Update(inv);
			}
			else
			{
				if (inv.Status >= ChatUserStatus.JoinedAsMember)
				{
					throw new ChatException("User is already a member of this group chat.");
				}
				else
				{
					inv.Status = ChatUserStatus.Dissociated;
					this._chatUserRepo.Update(inv);
					throw new ChatException("An unknown error occured while processing the invitation. Reverting state...");
				}
			}
		}

		public bool IsAllowedToKick(int chatId, int userId, int kickUserId)
		{
			// Allowed if it's the user himself
			if (kickUserId == userId)
			{
				return true;
			}
			else
			{
				// Allowed if the userId has higher (group) chat privileges
				ChatUser requestChatUser = this._chatUserRepo.GetByChatIdAndUserId(chatId, userId) ?? throw new MessageException("ChatUser does not exist.");
				ChatUser kickChatUser = this._chatUserRepo.GetByChatIdAndUserId(chatId, kickUserId) ?? throw new MessageException("ChatUser does not exist.");
				return requestChatUser.Status > ChatUserStatus.JoinedAsMember && requestChatUser.Status > kickChatUser.Status;
			}
		}

		public void ChangeChatName(int chatId, string newName)
		{
			newName = newName.Trim();

			// Validation
			if (newName.Length < 4)
			{
				throw new ChatException("New chat name is too short. Minimum of 4 letters required.");
			}

			Chat chat = this._chatRepo.GetById(chatId) ?? throw new ChatException("Chat does not exist.");
			chat.DisplayName = newName;
			this._chatRepo.Update(chat);
		}

		public bool IsAllowedToChangeChatName(int chatId, int userId)
		{
			ChatUser chatUser = this._chatUserRepo.GetByChatIdAndUserId(chatId, userId) ?? throw new ChatException("Chat does not exist.");
			return chatUser.Status >= ChatUserStatus.JoinedAsFounder;
		}
	}
}
