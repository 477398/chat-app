﻿using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Interfaces.IServices;
using Logic.Utilities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Services
{
    public class UserService : IUserService
	{
		private readonly IUserRepository _userRepo;
		private readonly IMessageRepository _messageRepo;
		private readonly IImageRepository _imgRepo;

		public UserService(IUserRepository userRepo, IMessageRepository messageRepo, IImageRepository imgRepo)
		{
			this._userRepo = userRepo;
			this._messageRepo = messageRepo;
			this._imgRepo = imgRepo;
		}

		public User CreateUser(string loginName, string displayName, string email, string unhashedPassword, string unhashedPasswordConfirm)
		{
			// Trimming
			loginName = loginName.Trim();
			displayName = displayName.Trim();
			email = email.Trim();
			unhashedPassword = unhashedPassword.Trim();
			unhashedPasswordConfirm = unhashedPasswordConfirm.Trim();

			// Validation
			if (this._userRepo.GetUserByLoginName(loginName) != null)
			{
				throw new UserException("Account already exists with this login name");
			}
			else if (loginName.Length < 4)
			{
				throw new UserException("Login name too short. Minimum 4 letters required.");
			}
			else if (displayName.Length < 4)
			{
				throw new UserException("Display name too short. Minimum 4 letters required.");
			}
			else if (!EmailValidator.IsValid(email))
			{
				throw new UserException("Invalid e-mail");
			}
			else if (this._userRepo.GetUserByEmail(email) != null)
			{
				throw new UserException("Account already exists with this e-mail");
			}
			else if (unhashedPassword.Length < 8)
			{
				throw new UserException("Password too short. Minimum 8 letters required.");
			}
			else if (unhashedPassword != unhashedPasswordConfirm)
			{
				throw new UserException("Passwords don't match.");
			};

			// Create user
			User user = new User
			{
				LoginName = loginName,
				DisplayName = displayName,
				HashedPassword = PasswordHasher.Hash(unhashedPassword),
				Email = email,
			};

			// Saving to database
			this._userRepo.Add(user);

			// Return
			return user;
		}

		public User GetUserById(int id)
		{
			return this._userRepo.GetById(id) ?? throw new UserException("User does not exist.");
		}

		public void UpdateUser(User user)
		{
			// Validation
			if (user.LoginName.Length < 4)
			{
				throw new UserException("Login name too short. Minimum 4 letters required.");
			}
			else if (user.DisplayName.Length < 4)
			{
				throw new UserException("Display name too short. Minimum 4 letters required.");
			}
			else if (!EmailValidator.IsValid(user.Email))
			{
				throw new UserException("Invalid e-mail");
			}

			this._userRepo.Update(user);
		}

		public List<Message> GetMessagesByUserId(int userId)
		{
			return this._messageRepo.GetAllByUserId(userId);
		}

		public async Task UploadPfp(IFormFile file, int userId)
		{
			await this._imgRepo.UploadImage(file, $"user{userId}");
		}

		public async Task UploadDefaultPfp(int userId)
		{
			// Get the image path
			DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
			while (dirInfo != null && dirInfo.Name != "WebServer")
			{
				dirInfo = dirInfo.Parent ?? throw new NullReferenceException();
			}
			if (dirInfo == null)
			{
				throw new NullReferenceException();
			}
			string imagePath = dirInfo.FullName + "/Server/Images/default_pfp.png";

			// Validation
			if (!System.IO.File.Exists(imagePath))
			{
				Console.Error.WriteLine("Default user image does not exist");
			}
			else
			{
				await this._imgRepo.UploadImage(imagePath, "default_pfp", $"user{userId}");
			}
		}

	}
}
