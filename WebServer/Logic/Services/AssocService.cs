﻿using Logic.Entities;
using Logic.Enumerations.Status;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Interfaces.IServices;

namespace Logic.Services
{
    public class AssocService : IAssocService
	{

		private readonly IAssocRepository _assocRepo;
		private readonly IUserRepository _userRepo;

		public AssocService(IAssocRepository assocRepo, IUserRepository userRepo)
		{
			this._assocRepo = assocRepo;
			this._userRepo = userRepo;
		}

		public Association CreateAssociation(int primaryUserId, int secondaryUserId)
		{
			Association assoc = new Association
			{
				PrimaryUserId = primaryUserId,
				SecondaryUserId = secondaryUserId,
				Status = AssociationStatus.Dissociated,
			};
			this._assocRepo.Add(assoc);
			return assoc;
		}

		public List<User> GetOtherUsersByPartialDisplayName(int ignoreId, string partialDisplayName)
		{
			List<User> users = this._userRepo.GetAllByPartialDisplayName(partialDisplayName);
			return users.Where(x => x.Id != ignoreId).ToList();
		}

		public Association GetAssociationByUserIds(int primaryUserId, int secondaryUserId)
		{
			return this._assocRepo.GetByUserIds(primaryUserId, secondaryUserId) ?? this.CreateAssociation(primaryUserId, secondaryUserId);
		}

		public List<Association> GetIncomingAssociationsByPrimaryUserId(int primaryUserId)
		{
			return this._assocRepo.GetIncomingsByPrimaryUserId(primaryUserId);
		}

		public List<Association> GetOutgoingAssociationsByPrimaryUserId(int primaryUserId)
		{
			return this._assocRepo.GetOutgoingsByPrimaryUserId(primaryUserId);
		}

		public void CreateFriendRequest(int senderUserId, int receiverUserId)
		{
			Association senderAssoc = this._assocRepo.GetByUserIds(senderUserId, receiverUserId) ?? this.CreateAssociation(senderUserId, receiverUserId);
			Association receiverAssoc = this._assocRepo.GetByUserIds(receiverUserId, senderUserId) ?? this.CreateAssociation(receiverUserId, senderUserId);

			if (senderAssoc.Status == AssociationStatus.Dissociated && receiverAssoc.Status == AssociationStatus.Dissociated)
			{
				senderAssoc.Status = AssociationStatus.PendingSender;
				receiverAssoc.Status = AssociationStatus.PendingReceiver;

				this._assocRepo.Update(senderAssoc);
				this._assocRepo.Update(receiverAssoc);
			}
			else
			{
				if (senderAssoc.Status == AssociationStatus.Blocked || receiverAssoc.Status == AssociationStatus.Blocked)
				{
					throw new AssociationException("Failed to sent friend request. One (or both) user(s) blocked each other.");
				}
				else if (senderAssoc.Status == AssociationStatus.Consociated && receiverAssoc.Status == AssociationStatus.Consociated)
				{
					throw new AssociationException("Users are already friends with each other");
				}
				else if ((senderAssoc.Status == AssociationStatus.PendingSender && receiverAssoc.Status == AssociationStatus.PendingReceiver)
					|| (senderAssoc.Status == AssociationStatus.PendingReceiver && receiverAssoc.Status == AssociationStatus.PendingSender))
				{
					throw new AssociationException("Friend request was already made, and in pending.");
				}
				else
				{
					senderAssoc.Status = AssociationStatus.Dissociated;
					receiverAssoc.Status = AssociationStatus.Dissociated;

					this._assocRepo.Update(senderAssoc);
					this._assocRepo.Update(receiverAssoc);
					throw new AssociationException("Unknown error has occured while sending a friend request. Reverting states...");
				}
			}
		}

		public void AnswerFriendRequest(int receiverUserId, int senderUserId, bool accept)
		{
			Association receiverAssoc = this._assocRepo.GetByUserIds(receiverUserId, senderUserId) ?? throw new AssociationException("Receiver does not exist");
			Association senderAssoc = this._assocRepo.GetByUserIds(senderUserId, receiverUserId) ?? throw new AssociationException("Sender does not exist");

			if (receiverAssoc.Status != AssociationStatus.PendingReceiver || senderAssoc.Status != AssociationStatus.PendingSender)
			{
				if (receiverAssoc.Status == AssociationStatus.Consociated && senderAssoc.Status == AssociationStatus.Consociated)
				{
					throw new AssociationException("These two users are already friends with each other.");
				}
				if (senderAssoc.Status == AssociationStatus.Blocked || receiverAssoc.Status == AssociationStatus.Blocked)
				{
					throw new AssociationException("Attempting (somehow) to accept friend requests, when one or both users blocked each other.");
				}
				else
				{   // Set the status back to "dissociated" so they can try again to add each other.
					receiverAssoc.Status = AssociationStatus.Dissociated;
					senderAssoc.Status = AssociationStatus.Dissociated;
					this._assocRepo.Update(receiverAssoc);
					this._assocRepo.Update(senderAssoc);
					throw new AssociationException("Unable to answer a friend request, because the status are incorrect. Did they even sent a friend request?");
				}
			}
			else
			{
				if (accept)
				{
					receiverAssoc.Status = AssociationStatus.Consociated;
					senderAssoc.Status = AssociationStatus.Consociated;
				}
				else
				{
					receiverAssoc.Status = AssociationStatus.Dissociated;
					senderAssoc.Status = AssociationStatus.Dissociated;
				}
				this._assocRepo.Update(receiverAssoc);
				this._assocRepo.Update(senderAssoc);
			}
		}

		public List<Association> GetFriendsByPrimaryUserId(int primaryUserId)
		{
			return this._assocRepo.GetFriendsByPrimaryUserId(primaryUserId);
		}

		public void Unfriend(int primaryUserId, int secondaryUserId)
		{
			Association? yourAssoc = this._assocRepo.GetByUserIds(primaryUserId, secondaryUserId) ?? throw new AssociationException("Sender does not exist");
			Association? friendAssoc = this._assocRepo.GetByUserIds(secondaryUserId, primaryUserId) ?? throw new AssociationException("Receiver does not exist");

			yourAssoc.Status = AssociationStatus.Dissociated;
			friendAssoc.Status = AssociationStatus.Dissociated;

			this._assocRepo.Update(yourAssoc);
			this._assocRepo.Update(friendAssoc);
		}
	}
}
