﻿using Logic.Entities;
using Logic.Enumerations.Status;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Interfaces.IServices;

namespace Logic.Services
{
    public class MessageService : IMessageService
	{

		private readonly IMessageRepository _messageRepo;
		private readonly IChatRepository _chatRepo;
		private readonly IChatUserRepository _chatUserRepo;

		public MessageService(IMessageRepository messageRepo, IChatRepository chatRepo, IChatUserRepository chatUserRepo)
		{
			this._messageRepo = messageRepo;
			this._chatRepo = chatRepo;
			this._chatUserRepo = chatUserRepo;
		}

		public List<Message> GetAllVisibleMessagesByChatId(int chatId)
		{
			return this._messageRepo.GetAllVisiblesByChatId(chatId);
		}

		public Message CreateMessage(int userId, int chatId, string content)
		{
			content = content.Trim();

			if (content == "") throw new MessageException("Message cannot be empty");

			Message msg = new Message
			{
				UserId = userId,
				ChatId = chatId,
				Content = content,
			};
			this._messageRepo.Add(msg);
			return msg;
		}

		public bool IsAllowedToEditMessage(int msgId, int userId)
		{
			// Allowed if the msg sender's is this user
			Message msg = this._messageRepo.GetById(msgId) ?? throw new MessageException("Message does not exist.");
			return msg.UserId == userId;
		}

		public void EditMessage(int msgId, string newContent)
		{
			Message msg = this._messageRepo.GetById(msgId) ?? throw new MessageException("Message does not exist."); ;
			msg.Content = newContent;
			msg.Status = MessageStatus.DisplayingModified;
			this._messageRepo.Update(msg);

		}

		public bool IsAllowedToHideMessage(int msgId, int userId)
		{
			Message msg = this._messageRepo.GetById(msgId) ?? throw new MessageException("Message does not exist.");
			// Allowed if it's the message's sender
			if (msg.UserId == userId)
			{
				return true;
			}
			else
			{
				// Allowed if the userId has higher (group) chat privileges
				ChatUser senderChatUser = this._chatUserRepo.GetByChatIdAndUserId(msg.ChatId, msg.UserId) ?? throw new MessageException("Sender's ChatUser does not exist.");
				ChatUser requestChatUser = this._chatUserRepo.GetByChatIdAndUserId(msg.ChatId, userId) ?? throw new MessageException("Requester's ChatUser does not exist.");
				return requestChatUser.Status > ChatUserStatus.JoinedAsMember && requestChatUser.Status > senderChatUser.Status;
			}
		}

		public void HideMessage(int msgId)
		{
			Message msg = this._messageRepo.GetById(msgId) ?? throw new MessageException("Message does not exist.");
			msg.Status = MessageStatus.Deleted;
			this._messageRepo.Update(msg);
		}
	}
}
