﻿using Logic.Entities;
using Logic.Exceptions;
using Logic.Interfaces.IRepositories;
using Logic.Interfaces.IServices;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Logic.Services
{
	public class AuthService : IAuthService
    {
        private readonly IConfiguration _config;
        private readonly IUserRepository _userRepo;

        public AuthService(IConfiguration config, IUserRepository userRepo)
        {
            this._config = config;
            this._userRepo = userRepo;
        }

        public void AuthenticateUser(User user, string hashedPassword)
        {
            if (user.HashedPassword != hashedPassword)
            {
                throw new AuthException("Incorrect password");
            }
        }

        public User AuthenticateUserByLoginName(string loginName, string hashedPassword)
        {
            User user = this._userRepo.GetUserByLoginName(loginName) ?? throw new AuthException("No account found with this login name");
            this.AuthenticateUser(user, hashedPassword);
            return user;
        }

        public User AuthenticateUserByEmail(string email, string hashedPassword)
        {

            User user = this._userRepo.GetUserByEmail(email) ?? throw new AuthException("No account found with this e-mail");
            this.AuthenticateUser(user, hashedPassword);
            return user;
        }

        public string GenerateJWT(int userId)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            string key = this._config["JWT:Key"] ?? throw new NullReferenceException();
            byte[] keyByte = Encoding.ASCII.GetBytes(key);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(keyByte), SecurityAlgorithms.HmacSha256Signature)
                // Add Expiration Date (maybe)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        
        public int GetUserIdFromClaimPrincipal(ClaimsPrincipal principal)
        {
            Claim? userIdClaim = principal.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).FirstOrDefault();
            if (userIdClaim == null)
            {
                throw new AuthException("Invalid token, missing user id");
            }
            return int.Parse(userIdClaim.Value);
        }
    }
}
