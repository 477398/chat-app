﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Exceptions
{
    public class MessageException : Exception
    {
        public MessageException() { }

        public MessageException(string? message) : base(message)
        {
        }

        public MessageException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
