﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Exceptions
{
    public class ChatException : Exception
    {
        public ChatException() { }

        public ChatException(string? message) : base(message)
        {
        }

        public ChatException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
