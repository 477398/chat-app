﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic.Exceptions
{
    public class AssociationException : Exception
    {
        public AssociationException()
        {
        }

        public AssociationException(string? message) : base(message)
        {
        }

        public AssociationException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}
