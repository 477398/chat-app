import React from 'react'
import ReactDOM from 'react-dom/client'
import './main.css'
import { LoginComponent } from "./components/Login/LoginComponent"

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <LoginComponent />
    </React.StrictMode>
)