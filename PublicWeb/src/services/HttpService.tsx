import { getToken } from "../entities/JWT";

export class HttpService {

    apiUrl: string;

    constructor() {
        this.apiUrl = "https://localhost:7149/"
    }

    // GET (Read)
    getFetch = <Response,>(route: string): Promise<Response> => {
        return fetch(`${this.apiUrl}/${route}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${getToken()}`,
            },
        })
            .then((response) => { return response.json() })
            .catch((error) => console.log(error));
    }

    // POST (Create)
    postFetch = <Request, Response>(route: string, request: Request): Promise<Response> => {
        return fetch(`${this.apiUrl}/${route}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${getToken()}`,
            },
            body: JSON.stringify(request),
        })
            .then((response) => { return response.json() })
            .catch((error) => console.log(error));
    }

    postFetchFormData = <Response,>(route: string, formData: FormData): Promise<Response> => {
        return fetch(`${this.apiUrl}/${route}`, {
            method: "POST",
            // headers: {"Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryiB9gHFIDsfROQwkA"},
            headers: {
                "Authorization": `Bearer ${getToken()}`,
            },
            body: formData,
        })
            .then((response) => { return response.json() })
            .catch((error) => console.log(error));
    }

    // PUT (Update)
    putFetch = <Request, Response>(route: string, request: Request): Promise<Response> => {
        return fetch(`${this.apiUrl}/${route}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${getToken()}`,
            },
            body: JSON.stringify(request),
        })
            .then((response) => { return response.json() })
            .catch((error) => console.log(error));
    }


}