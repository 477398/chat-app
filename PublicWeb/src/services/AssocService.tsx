import { HttpService } from "./HttpService.tsx";
import { AnswerFriendRequest, BefriendRequest, UserDiscoveryRequest, UnfriendRequest } from "../entities/Requests.tsx"
import { FriendInfoResponse } from "../entities/Responses.tsx";

class AssocService extends HttpService {

    constructor() {
        super();
        this.apiUrl += "Assoc"
    }

    //GETs
    discoverUsers = (request: UserDiscoveryRequest): Promise<FriendInfoResponse[]> => {
        return this.getFetch(`Others?partialDisplayName=${request.partialDisplayName}`);
    }

    getIncomingFriendRequests = (): Promise<FriendInfoResponse[]> => {
        return this.getFetch(`Incomings`);
    }

    getFriendsByPartialDisplayName = (request: UserDiscoveryRequest): Promise<FriendInfoResponse[]> => {
        return this.getFetch(`Friends?partialDisplayName=${request.partialDisplayName}`);
    }

    getFriends = (): Promise<FriendInfoResponse[]> => {
        return this.getFetch(`Friends`);
    }

    getOutgoingFriendRequests = (): Promise<FriendInfoResponse[]> => {
        return this.getFetch(`Outgoings`);
    }

    //PUTs
    sendFriendRequest = (request: BefriendRequest): Promise<Response> => {
        return this.putFetch(`Befriend`, request);
    }

    answerFriendRequest = (request: AnswerFriendRequest): Promise<Response> => {
        return this.putFetch(`Answer`, request);
    }

    unfriend = (request: UnfriendRequest): Promise<Response> => {
        return this.putFetch(`Unfriend`, request);
    }
}

export const assocService = new AssocService();