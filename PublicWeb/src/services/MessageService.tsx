import { CreateMessageRequest, DeleteMessageRequest, EditMessageRequest } from "../entities/Requests";
import { HttpService } from "./HttpService";

class MessageService extends HttpService {

    constructor() {
        super();
        this.apiUrl += "Message";
    }

    // POSTs (Create)
    sendMessage = (request: CreateMessageRequest): Promise<Response> => {
        return this.postFetch("", request);
    }

    // PUTs (Update)
    deleteMessage = (request: DeleteMessageRequest): Promise<Response> => {
        return this.putFetch("Delete", request);
    }

    editMessage = (request: EditMessageRequest): Promise<Response> => {
        return this.putFetch("Edit", request);
    }
}

export const msgService: MessageService = new MessageService();