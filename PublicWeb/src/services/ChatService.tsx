import { GetGroupChatsRequest, GetFriendsForGroupInvitationRequest, GetChatInfoRequest, SendGroupChatInvitationRequest, AnswerGroupChatInvitationRequest, KickMemberRequest, ChangeChatNameRequest } from "../entities/Requests";
import { Response, GetChatInfoResponse, GetGroupInvitationResponse, GroupInvitationFriendInfoResponse, GetGroupChatResponse } from "../entities/Responses";
import { HttpService } from "./HttpService";

export class ChatService extends HttpService {

    constructor() {
        super();
        this.apiUrl += "Chat";
    }

    // GETs (Read)

    getGroupChatsByPartialDisplayName = (request: GetGroupChatsRequest): Promise<GetGroupChatResponse[]> => {
        return this.getFetch(`Groups?partialDisplayName=${request.partialDisplayName}`);
    }

    getGroupChats = (): Promise<GetGroupChatResponse[]> => {
        return this.getFetch(`Groups`);
    }

    getFriendsForGroupInvitation = (request: GetFriendsForGroupInvitationRequest): Promise<GroupInvitationFriendInfoResponse[]> => {
        return this.getFetch(`Group/Invite/Friends?chatId=${request.chatId}`);
    }

    getFriendsForGroupInvitationByPartialDisplayName = (request: GetFriendsForGroupInvitationRequest): Promise<GroupInvitationFriendInfoResponse[]> => {
        return this.getFetch(`Group/Invite/Friends?chatId=${request.chatId}&partialDisplayName=${request.partialDisplayName}`);
    }

    getIncomingGroupInvitations = (): Promise<GetGroupInvitationResponse[]> => {
        return this.getFetch(`Group/Invites`)
    }

    getChatInfo = (request: GetChatInfoRequest): Promise<GetChatInfoResponse> => {
        return this.getFetch(`${request.chatId}`);
    }

    // PUTs (Update)
    kickMember = (request: KickMemberRequest): Promise<Response> => {
        return this.putFetch("Group/Kick", request);
    }

    sendGroupChatInvitationToFriend = (request: SendGroupChatInvitationRequest): Promise<Response> => {
        return this.putFetch(`Group/Invite`, request);
    }

    answerGroupChatInvitation = (request: AnswerGroupChatInvitationRequest): Promise<Response> => {
        return this.putFetch(`Group/Invite/Answer`, request);
    }

    changeChatName = (request: ChangeChatNameRequest): Promise<Response> => {
        return this.putFetch(`${request.chatId}/Name`, request);
    }

    // POSTs (Create)
    createGroupChat = (): Promise<Response> => {
        return this.postFetch("Group", {});
    }
}

export const chatService: ChatService = new ChatService();