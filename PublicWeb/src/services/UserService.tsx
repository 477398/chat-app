import { LoginRequest, RegistrationRequest, UploadPfpRequest, UpdateMyProfileRequest } from "../entities/Requests";
import { Response, GetMyProfileResponse, LoginResponse } from "../entities/Responses";
import { HttpService } from "./HttpService";

class UserService extends HttpService {

    constructor() {
        super();
        this.apiUrl += "User";
    }

    // GETs (Read)
    loginUser = (request: LoginRequest): Promise<LoginResponse> => {
        return this.getFetch(`Login?loginNameOrEmail=${request.loginNameOrEmail}&unhashedPassword=${request.unhashedPassword}`);
    }

    getMyProfile = (): Promise<GetMyProfileResponse> => {
        return this.getFetch(`MyProfile`);
    }


    // POSTs (Create)
    createUser = (request: RegistrationRequest): Promise<LoginResponse> => {
        return this.postFetch("", request);
    }

    uploadPfp = (request: UploadPfpRequest): Promise<Response> => {
        const formData: FormData = new FormData();
        formData.append("file", request.file);

        return this.postFetchFormData(`Pfp`, formData);
    }

    // PUTs (Update)
    updateMyProfile = (request: UpdateMyProfileRequest): Promise<Response> => {
        return this.putFetch("MyProfile", request);
    }

}

export const userService: UserService = new UserService();