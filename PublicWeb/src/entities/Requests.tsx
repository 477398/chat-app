export interface LoginRequest {
    loginNameOrEmail: string;
    unhashedPassword: string;
}

export interface RegistrationRequest {
    loginName: string;
    displayName: string;
    email: string;
    unhashedPassword: string;
    unhashedPasswordConfirm: string;
}

export interface Request { }

export interface CreateMessageRequest extends Request {
    content: string;
    chatId: number;
}

export interface AnswerFriendRequest extends Request {
    senderUserId: number;
    acceptFriendship: boolean;
}

export interface UserDiscoveryRequest extends Request {
    partialDisplayName: string,
}

export interface BefriendRequest extends Request {
    receiverUserId: number;
}

export interface UnfriendRequest extends Request {
    friendUserId: number;
}

export interface DeleteMessageRequest extends Request {
    msgId: number;
}

export interface EditMessageRequest extends Request {
    msgId: number;
    content: string;
}

export interface GetGroupChatsRequest extends Request {
    partialDisplayName: string;
}

export interface KickMemberRequest extends Request {
    chatId: number;
    kickUserId: number;
}

export interface GetFriendsForGroupInvitationRequest extends Request {
    chatId: number,
    partialDisplayName: string,
}

export interface SendGroupChatInvitationRequest extends Request {
    chatId: number,
    friendUserId: number,
}

export interface AnswerGroupChatInvitationRequest extends Request {
    chatId: number,
    accept: boolean,
}

export interface GetChatInfoRequest extends Request {
    chatId: number,
}

export interface ChangeChatNameRequest extends Request {
    chatId: number,
    newName: string,
}

export interface UploadPfpRequest extends Request {
    file: File,
}

export interface UpdateMyProfileRequest extends Request {
    loginName: string,
    displayName: string,
    email: string,
}