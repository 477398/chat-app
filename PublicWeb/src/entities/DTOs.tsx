export interface MessageDTO {
    msgId: number;
    senderUserId: number;
    content: string;
    userDisplayName: string;
    senderChatRole: number;
}

export interface ChatUserDTO extends Response {
    userId: number,
    userDisplayName: string,
    chatRole: number,
}