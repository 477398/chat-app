import { jwtDecode } from 'jwt-decode';

const storageKey = "JWT";

export interface DecodedToken {
    nameid: number; // Same as user id
}

export const setToken = (newToken: string) => {
    localStorage.setItem(storageKey, newToken);
};

export const getToken = (): string | null => {
    return localStorage.getItem(storageKey);
};

export const removeToken = (): void => {
    localStorage.removeItem(storageKey);
};

export const getDecodedToken = (): DecodedToken | null => {
    const token = getToken();
    if (token === null) return null;
    return jwtDecode(token);
}
