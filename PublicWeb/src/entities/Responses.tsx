import { ChatUserDTO, MessageDTO } from "./DTOs";

export interface Response {
    success: boolean
    message: string,
}

export interface FriendInfoResponse extends Response {
    friendUserId: number,
    displayName: string,
    loginName: string,
    associationStatus: number,
    chatId: number,
}

export interface LoginResponse extends Response {
    jwt: string;
}

export interface GetGroupChatResponse extends Response {
    chatId: number;
    chatDisplayName: string;
}

export interface GroupInvitationFriendInfoResponse extends Response {
    friendUserId: number,
    friendDisplayName: string,
    invitationStatus: number,
}

export interface GetGroupInvitationResponse extends Response {
    chatDisplayName: string,
    chatId: number,
}

export interface GetChatInfoResponse extends Response {
    chatId: number,
    myChatRole: number,
    chatDisplayName: string,
    chatDescription: string,
    messages: MessageDTO[],
    chatUsers: ChatUserDTO[],
}

export interface GetMyProfileResponse extends Response {
    userId: number,
    loginName: string,
    displayName: string,
    email: string,
}