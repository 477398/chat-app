import React from "react";
import "./Groups.css"
import { chatService } from "../../services/ChatService";
import { getDecodedToken } from "../../entities/JWT";
import { GetGroupChatsRequest, KickMemberRequest } from "../../entities/Requests";
import { GetGroupChatResponse } from "../../entities/Responses";
import { ChatComponent } from "../Chat/Chat";
import { GroupInvitationComponent } from "../SendGroupInvitation/GroupInvitationComponent";
import { ViewGroupInvitations } from "../ViewGroupInvitations/ViewGroupInvitations";

interface Props {

}

interface States {
    searchBarInput: string;
    groupChats: GetGroupChatResponse[];
    childComponent: JSX.Element;
}

export class GroupsComponent extends React.Component<Props, States> {

    interval: NodeJS.Timeout | null;

    constructor(props: Props) {
        super(props);
        this.state = {
            searchBarInput: "",
            groupChats: [],
            childComponent: <></>,
        }

        this.interval = null;
    }

    onSearchBarInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            searchBarInput: e.target.value,
        })
        this.getGroupChats(e.target.value);
    }

    createGroupChat = (): void => {
        chatService.createGroupChat()
            .then(() => {
                const { searchBarInput } = this.state;
                this.getGroupChats(searchBarInput);
            });
    }

    getGroupChats = (partialDisplayName: string): void => {
        const request: GetGroupChatsRequest = {
            partialDisplayName: partialDisplayName,
        };
        if (partialDisplayName == "") {
            chatService.getGroupChats()
                .then((response: GetGroupChatResponse[]) => {
                    this.setState({
                        groupChats: response,
                    });
                });
        }
        else {
            chatService.getGroupChatsByPartialDisplayName(request)
                .then((response: GetGroupChatResponse[]) => {
                    this.setState({
                        groupChats: response,
                    });
                });
        }
    }

    leaveGroupChat = (chatId: number): void => {
        const request: KickMemberRequest = {
            chatId: chatId,
            kickUserId: getDecodedToken()?.nameid as number,
        };
        chatService.kickMember(request)
            .then(() => {
                const { searchBarInput } = this.state;
                this.getGroupChats(searchBarInput);
            });
    }

    openGroupChat = (chatId: number): void => {
        this.setState({
            childComponent: <ChatComponent chatId={chatId} closeChat={this.resetChildComponent} />
        })
    }

    resetChildComponent = (): void => {
        this.setState({
            childComponent: <></>,
        });
    }

    openGroupInvitationScreen = (chat: GetGroupChatResponse): void => {
        this.setState({
            childComponent: <GroupInvitationComponent info={chat} />
        })
    }

    onViewInvitationsClick = (): void => {
        this.setState({
            childComponent: <ViewGroupInvitations refreshGroupList={
                () => {
                    const { searchBarInput } = this.state;
                    this.getGroupChats(searchBarInput);
                }
            } />,
        });
    }

    componentDidMount() {
        this.getGroupChats("");
        this.interval = setInterval(() => {
            const { searchBarInput } = this.state;
            this.getGroupChats(searchBarInput);
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval as NodeJS.Timeout);
        this.interval = null;
    }

    render(): JSX.Element {

        const { groupChats, childComponent } = this.state;

        return <div id="groupsComponent">
            <div id="groupsListContainer" className="container-style2 flex-column">
                <label>Your Groups</label>
                <button id="viewInvitationsBtn" className="button-style" onClick={this.onViewInvitationsClick }>View Invitations</button>
                <button id="createNewGroupBtn" className="button-style" onClick={this.createGroupChat}>Create a New Group</button>
                <input type="text" placeholder="Search for groups" className="input-style" onChange={this.onSearchBarInputChange} />
                <ol className="flex-column">
                    {groupChats.map((chat: GetGroupChatResponse) => (
                        <li id="groupsListItem" key={chat.chatId} className="li-friend-style">
                            <label>{chat.chatDisplayName}</label>
                            <button id="groupLeaveButton" className="button-style" onClick={() => this.leaveGroupChat(chat.chatId)}>Leave Group</button>
                            <button id="inviteFriendsButton" className="button-style" onClick={() => this.openGroupInvitationScreen(chat)}>Invite Friends</button>
                            <button id="openChatButton" className="button-style" onClick={() => this.openGroupChat(chat.chatId)}>Open Chat</button>
                        </li>
                    ))}
                </ol>
            </div>
            <div id="childComponent">
                {childComponent}
            </div>
        </div>
    }
}