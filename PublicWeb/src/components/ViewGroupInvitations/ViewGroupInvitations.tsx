import React from "react";
import "./ViewGroupInvitations.css"
import { GetGroupInvitationResponse } from "../../entities/Responses";
import { AnswerGroupChatInvitationRequest } from "../../entities/Requests";
import { chatService } from "../../services/ChatService";

interface Props {
    refreshGroupList: () => void,
}

interface States {
    invitations: GetGroupInvitationResponse[],
}

export class ViewGroupInvitations extends React.Component<Props, States> {

    interval: NodeJS.Timeout | null;

    constructor(props: Props) {
        super(props)
        this.state = {
            invitations: [],
        }

        this.interval = null;
    }

    getInvitations = (): void => {
        chatService.getIncomingGroupInvitations()
            .then((response: GetGroupInvitationResponse[]) => {
                this.setState({
                    invitations: response,
                });
            })
    }

    answerInvitation = (chatId: number, accept: boolean): void => {
        const request: AnswerGroupChatInvitationRequest = {
            chatId: chatId,
            accept: accept,
        };
        chatService.answerGroupChatInvitation(request)
            .then(() => {
                this.getInvitations(); // Refresh the list
                this.props.refreshGroupList();
            });
    }

    componentDidMount() {
        this.getInvitations();
        this.interval = setInterval(() => {
            this.getInvitations();
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval as NodeJS.Timeout);
        this.interval = null;
    }

    render(): JSX.Element {

        const { invitations } = this.state;

        return <div id="viewGroupInvitations" className="container-style2 flex-column">
            <label>Group Chat Invitations</label>
            <ol className="flex-column">
                {invitations.map((inv: GetGroupInvitationResponse, index) => (
                    <li id="invitationListItem" key={index} className="container-style">
                        <label>{inv.chatDisplayName}</label>
                        <button id="acceptButton" className="button-style" onClick={() => this.answerInvitation(inv.chatId, true)}>Accept</button>
                        <button id="declineButton" className="button-style" onClick={() => this.answerInvitation(inv.chatId, false)}>Decline</button>
                    </li>
                ))}
            </ol>
        </div>
    }
}