import React from "react";
import "./FriendSearch.css";
import { assocService } from "../../services/AssocService";
import { AnswerFriendRequest, BefriendRequest, UserDiscoveryRequest } from "../../entities/Requests.tsx";
import { FriendInfoResponse } from "../../entities/Responses.tsx";

enum AssociationStatus {
    Blocked = -1,
    Dissociated = 0,
    PendingSender = 1,
    PendingReceiver = 2,
    Consociated = 3,
}
interface Props {
    refreshFriendList: () => void,
}

interface States {
    searchBarInput: string;
    searchResults: FriendInfoResponse[];
    incomingRequests: FriendInfoResponse[];
    outgoingRequests: FriendInfoResponse[];
}

export class FriendSearchComponent extends React.Component<Props, States> {

    refreshTimer: NodeJS.Timeout | null;

    constructor(props: Props) {
        super(props)
        this.state = {
            searchBarInput: "",
            searchResults: [],
            incomingRequests: [],
            outgoingRequests: [],
        }

        this.refreshTimer = null;
    }

    refreshFriendRequestLists = (): void => {
        const { searchBarInput } = this.state;
        assocService.getIncomingFriendRequests()
            .then((response: FriendInfoResponse[]) => {
                this.setState({
                    incomingRequests: response,
                })
            });

        assocService.getOutgoingFriendRequests()
            .then((response: FriendInfoResponse[]) => {
                this.setState({
                    outgoingRequests: response,
                })
            });
        this.searchForFriends(searchBarInput);
    }

    onSearchBarChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            searchBarInput: e.target.value,
        })
        this.searchForFriends(e.target.value);
    }

    searchForFriends = (partialDisplayName: string): void => {
        if (partialDisplayName == "") {
            this.setState({
                searchResults: [],
            })
        }
        else {
            const request: UserDiscoveryRequest = {
                partialDisplayName: partialDisplayName,
            };
            assocService.discoverUsers(request)
                .then((response: FriendInfoResponse[]) => {
                    this.onSearchResponse(response)
                    console.log(response)
                });
        }
    }

    onSearchResponse = (response: FriendInfoResponse[]): void => {
        this.setState({
            searchResults: response,
        })
    }

    sendFriendRequest = (receiverUserId: number): void => {
        const request: BefriendRequest = {
            receiverUserId: receiverUserId
        }
        assocService.sendFriendRequest(request)
            .then(() => {
                // refresh the lists
                this.refreshFriendRequestLists();
            });
    }

    answerFriendRequest = (senderUserId: number, accept: boolean): void => {
        const request: AnswerFriendRequest = {
            senderUserId: senderUserId,
            acceptFriendship: accept,
        }
        assocService.answerFriendRequest(request)
            .then(() => {
                // refresh the lists
                this.props.refreshFriendList();
                this.refreshFriendRequestLists();
            });
    }

    componentDidMount(): void {
        // this.refreshTimer = setInterval(this.refreshFriendRequestLists, 1000);
        this.refreshFriendRequestLists();
    }

    componentWillUnmount(): void {
        clearInterval(this.refreshTimer as NodeJS.Timeout);
        this.refreshTimer = null;
    }

    render(): JSX.Element {

        const { searchResults, searchBarInput, incomingRequests, outgoingRequests } = this.state;

        return <div id="friendSearchComponent">
            <div id="searchBody" className="flex-column">
                <label>Search for friends</label>
                <div className="container-style2 flex-column">
                    <input
                        placeholder="Search for friends"
                        type="text"
                        className="input-style"
                        value={searchBarInput}
                        onChange={this.onSearchBarChange}
                    />
                    <ol className="flex-column">
                        {searchResults.map((info: FriendInfoResponse, index) => (
                            // @ts-expect-error harmless
                            <li name={"userId" + info.friendUserId} key={index} id="discoverUsersListItem" className="li-friend-style">
                                <img className="img-pfp" src={`https://res.cloudinary.com/ddi8u0lxw/image/upload/user${info.friendUserId}`} />
                                <label>{info.displayName}</label>
                                <p className="p-style">{info.loginName}</p>
                                {info.associationStatus <= AssociationStatus.Dissociated ?
                                    <button
                                        className="button-style"
                                        onClick={() => this.sendFriendRequest(info.friendUserId)}
                                    >Send Friend Request</button>
                                    : (info.associationStatus == AssociationStatus.PendingReceiver || info.associationStatus == AssociationStatus.PendingSender) ?
                                        <div className="container-style">Request pending</div>
                                        : info.associationStatus == AssociationStatus.Consociated ?
                                            <div className="container-style">Already friends</div>
                                            : <></>}

                            </li>
                        ))}
                    </ol>
                </div>
            </div>
            <div id="outgoingContainer" className="flex-column">
                <label>Outgoing friend requests</label>
                <ol className="container-style2 flex-column">
                    {outgoingRequests.map((info: FriendInfoResponse, index) => (
                        <li key={index} id="outgoingsListItem" className="li-friend-style">
                            <img className="img-pfp" src={`https://res.cloudinary.com/ddi8u0lxw/image/upload/user${info.friendUserId}`} />
                            <label>{info.displayName}</label>
                            <p className="p-style">{info.loginName}</p>
                        </li>
                    ))}
                </ol>
            </div>
            <div id="incomingContainer" className="flex-column">
                <label>Incoming friend requests</label>
                <ol className="container-style2 flex-column">
                    {incomingRequests.map((info: FriendInfoResponse, index) => (
                        <li key={index} id="incomingsListItem" className="li-friend-style">
                            <img className="img-pfp" src={`https://res.cloudinary.com/ddi8u0lxw/image/upload/user${info.friendUserId}`} />
                            <label>{info.displayName}</label>
                            <p className="p-style">{info.loginName}</p>
                            <button
                                className="button-style"
                                id="acceptButton"
                                onClick={() => this.answerFriendRequest(info.friendUserId, true)}
                            >Accept</button>
                            <button
                                className="button-style"
                                id="declineButton"
                                onClick={() => this.answerFriendRequest(info.friendUserId, false)}
                            >Decline</button>
                        </li>
                    ))}
                </ol>
            </div>
        </div>
    }
}