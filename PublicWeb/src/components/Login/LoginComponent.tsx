import React from "react";
import "./Login.css";
import { RegisterComponent } from "../Registration/RegistrationComponent";
import { getToken, removeToken, setToken } from "../../entities/JWT";
import { AppComponent } from "../App/AppComponent";
import { userService } from "../../services/UserService";
import { LoginRequest } from "../../entities/Requests";
import { LoginResponse } from "../../entities/Responses";

interface Props {

}

interface States {
    registering: boolean;
    loginRequest: LoginRequest;
    errorMessage: string;
}

export class LoginComponent extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props)
        this.state = {
            loginRequest: { loginNameOrEmail: "", unhashedPassword: "" },
            errorMessage: "",
            registering: false,
        }
        // this.logout();
    }

    onLoginNameChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { loginRequest } = this.state;
        this.setState({
            loginRequest: { ...loginRequest, loginNameOrEmail: e.target.value }
        })
    }

    onPasswordChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { loginRequest } = this.state;
        this.setState({
            loginRequest: { ...loginRequest, unhashedPassword: e.target.value }
        })
    }

    onLoginClick = (): void => {
        const { loginRequest } = this.state;
        this.login(loginRequest);
    }

    login = (loginRequest: LoginRequest): void => {
        if (loginRequest.loginNameOrEmail == "" || loginRequest.unhashedPassword == "") {
            this.setState({
                errorMessage: "Fill in the empty fields",
            })
        }
        else {
            userService.loginUser(loginRequest)
                .then(this.onLoginResponse);
        }
    }

    onLoginResponse = (response: LoginResponse): void => {
        if (response.success == true) {
            setToken(response.jwt);
            this.setState({
                registering: false,
            });
        }
        else {
            this.setState({
                errorMessage: response.message,
            })
        }
    }

    logout = (): void => {
        removeToken();
        this.setState({
            registering: false,
        })
    }

    toggleRegisterComponent = (): void => {
        const { registering } = this.state;
        this.setState({
            registering: !registering,
        })
    }

    render(): JSX.Element {

        const { errorMessage, loginRequest, registering } = this.state;

        return <>
            {getToken() === null ?
                <>
                    {registering ?
                        <RegisterComponent onLoginResponse={this.onLoginResponse} toggleRegisterComponent={this.toggleRegisterComponent} />
                        :
                        <div id="loginBody" className="container-style">
                            <div id="loginContainer" className="flex-center-items">
                                <label>Login to your account!</label>
                                <input
                                    name="id"
                                    value={loginRequest.loginNameOrEmail}
                                    type="text"
                                    className="input-style"
                                    placeholder="Login Name / E-mail"
                                    onChange={this.onLoginNameChange}
                                />
                                <input
                                    name="password"
                                    value={loginRequest.unhashedPassword}
                                    type="password"
                                    className="input-style"
                                    placeholder="Password"
                                    onChange={this.onPasswordChange}
                                />
                                <button
                                    name="loginBtn"
                                    id="loginButton"
                                    className="button-style"
                                    onClick={this.onLoginClick}
                                >Login</button>
                                {errorMessage != "" ? <p id="loginError" className="p-error-style">{errorMessage}</p> : <></>}
                            </div>
                            <div id="registerContainer" className="flex-center-items">
                                <label>Don't have an account?</label>
                                <button
                                    id="registerButton"
                                    className="button-style"
                                    onClick={this.toggleRegisterComponent}
                                >Register Here</button>
                            </div>
                        </div>
                    }
                </>
                :
                <>
                    <AppComponent logout={this.logout} />
                </>}
        </>
    }

}