import React from "react";
import "./Friends.css"
import { FriendSearchComponent } from "../FriendSearch/FriendSearchComponent";
import { assocService } from "../../services/AssocService";
import { ChatComponent } from "../Chat/Chat.tsx";
import { UnfriendRequest, UserDiscoveryRequest } from "../../entities/Requests.tsx";
import { FriendInfoResponse } from "../../entities/Responses.tsx";

interface Props {

}

interface States {
    childComponent: JSX.Element,
    friendList: FriendInfoResponse[],
    searchBarInput: string,
}

export class FriendsComponent extends React.Component<Props, States> {

    refreshTimer: NodeJS.Timeout | null;

    constructor(props: Props) {
        super(props);
        this.state = {
            childComponent: <></>,
            friendList: [],
            searchBarInput: "",
        }

        this.refreshTimer = null;
    }

    onSearchFriendsClick = (): void => {
        this.setState({
            childComponent: <FriendSearchComponent refreshFriendList={() => {
                const { searchBarInput } = this.state;
                this.getFriends(searchBarInput)
            }} />,
        })
    }

    getFriends = (partialDisplayName: string): void => {
        const request: UserDiscoveryRequest = {
            partialDisplayName: partialDisplayName,
        };
        if (partialDisplayName == "") {
            assocService.getFriends()
                .then((response: FriendInfoResponse[]) => {
                    this.setState({
                        friendList: response,
                    })
                });
        }
        else {
            assocService.getFriendsByPartialDisplayName(request)
                .then((response: FriendInfoResponse[]) => {
                    this.setState({
                        friendList: response,
                    })
                });
        }
    }

    onSearchBarChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            searchBarInput: e.target.value,
        })
        this.getFriends(e.target.value);
    }

    unfriend = (friendUserId: number): void => {
        const { searchBarInput } = this.state;
        const request: UnfriendRequest = {
            friendUserId: friendUserId
        };
        assocService.unfriend(request)
            .then(() => {
                this.getFriends(searchBarInput);
            });
    }

    openChat = (chatId: number): void => {
        this.setState({
            childComponent: <ChatComponent chatId={chatId} closeChat={this.onSearchFriendsClick} />
        });
    }

    componentDidMount() {
        this.onSearchFriendsClick();
        this.refreshTimer = setInterval(() => {
            const { searchBarInput } = this.state;
            this.getFriends(searchBarInput);
        }, 1000);
        this.getFriends("");
    }

    componentWillUnmount() {
        clearInterval(this.refreshTimer as NodeJS.Timeout);
        this.refreshTimer = null;
    }

    render(): JSX.Element {

        const { childComponent, friendList, searchBarInput } = this.state;

        return <div id="friendsComponent">
            <div id="friendsList" className="container-style2 flex-column">
                <label>Your friends</label>
                <button id="searchFriendsButton" className="button-style" onClick={this.onSearchFriendsClick}>
                    Find Friends / Friend Requests
                </button>
                <input
                    className="input-style"
                    placeholder="Search for friends"
                    value={searchBarInput}
                    onChange={this.onSearchBarChange}
                />
                <ol className="flex-column">
                    {friendList.map((info: FriendInfoResponse, index) => (
                        // @ts-expect-error harmless
                        <li name={"friendId" + info.friendUserId} key={index} className="li-friend-style">
                            <img className="img-pfp" src={`https://res.cloudinary.com/ddi8u0lxw/image/upload/user${info.friendUserId}`}/>
                            <label className="hidden-overflow">{info.displayName}</label>
                            <button
                                id="openChatButton"
                                className="button-style"
                                onClick={() => this.openChat(info.chatId)}
                            >Open Chat</button>
                            <button
                                id="unfriendButton"
                                className="button-style"
                                onClick={() => this.unfriend(info.friendUserId)}
                            >Unfriend</button>
                        </li>
                    ))}
                </ol>
            </div>
            <div id="childComponent">
                {childComponent}
            </div>
        </div>
    }
}