import React from "react";
import "./Registration.css"
import { userService } from "../../services/UserService";
import { RegistrationRequest } from "../../entities/Requests";
import { LoginResponse } from "../../entities/Responses";
interface Props {
    toggleRegisterComponent: () => void,
    onLoginResponse: (response: LoginResponse) => void,
}

interface States {
    registrationRequest: RegistrationRequest,
    errorMessage: string;
}

export class RegisterComponent extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props)
        this.state = {
            registrationRequest: { loginName: "", displayName: "", email: "", unhashedPassword: "", unhashedPasswordConfirm: "" },
            errorMessage: "",
        }
    }

    onLoginNameChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { registrationRequest } = this.state;
        this.setState({
            registrationRequest: { ...registrationRequest, loginName: e.target.value }
        })
    }

    onDisplayNameChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { registrationRequest } = this.state;
        this.setState({
            registrationRequest: { ...registrationRequest, displayName: e.target.value }
        })
    }

    onEmailChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { registrationRequest } = this.state;
        this.setState({
            registrationRequest: { ...registrationRequest, email: e.target.value }
        })
    }

    onPasswordChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { registrationRequest } = this.state;
        this.setState({
            registrationRequest: { ...registrationRequest, unhashedPassword: e.target.value }
        })
    }

    onPasswordConfirmChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        const { registrationRequest } = this.state;
        this.setState({
            registrationRequest: { ...registrationRequest, unhashedPasswordConfirm: e.target.value }
        })
    }

    onRegisterClick = (): void => {
        const { registrationRequest } = this.state;
        userService.createUser(registrationRequest)
            .then(this.onRegisterResponse);
    }

    onRegisterResponse = (response: LoginResponse) => {
        if (response.success == true) {
            this.props.onLoginResponse(response);
        }
        else {
            this.setState({
                errorMessage: response.message,
            })
        }
    }

    render(): JSX.Element {

        const { registrationRequest, errorMessage } = this.state;

        return <>
            <div id="registerBody">
                <button
                    id="backToLoginButton"
                    className="button-style"
                    onClick={this.props.toggleRegisterComponent}
                >Back to Login</button>
                <div id="registerMain" className="container-style flex-center-items">
                    <label >Register an account</label>
                    <input
                        value={registrationRequest.loginName}
                        type="text"
                        className="input-style"
                        placeholder="Enter Login Name"
                        onChange={this.onLoginNameChange}
                    />
                    <p className="p-register">The name that is used to login (min. 4 characters)</p>
                    <input
                        value={registrationRequest.displayName}
                        type="text"
                        className="input-style"
                        placeholder="Enter Display Name"
                        onChange={this.onDisplayNameChange}
                    />
                    <p className="p-register">The name that is displayed to others (min. 4 characters)</p>
                    <input
                        value={registrationRequest.email}
                        type="text"
                        className="input-style"
                        placeholder="Enter E-mail"
                        onChange={this.onEmailChange}
                    />
                    <p className="p-register">Your e-mailaddress is used to login and to sent announcements, updates & account security</p>
                    <input
                        value={registrationRequest.unhashedPassword}
                        type="password"
                        className="input-style"
                        placeholder="Enter Password"
                        onChange={this.onPasswordChange}
                    />
                    <input
                        value={registrationRequest.unhashedPasswordConfirm}
                        type="password"
                        className="input-style"
                        placeholder="Re-enter Password"
                        onChange={this.onPasswordConfirmChange}
                    />
                    <p className="p-register">Password needs to have atleast 8 characters.</p>
                    {errorMessage ? <p
                        className="p-error-style"
                        id="registerError"
                    >{errorMessage}</p> : <></>}
                    <button
                        id="registerButton"
                        className="button-style"
                        onClick={this.onRegisterClick}
                    >Register</button>
                </div>
            </div>
        </>
    }
}