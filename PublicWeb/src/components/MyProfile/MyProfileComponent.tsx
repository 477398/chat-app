import React from "react";
import "./MyProfile.css"
import { userService } from "../../services/UserService";
import { UpdateMyProfileRequest, UploadPfpRequest } from "../../entities/Requests";
import { GetMyProfileResponse } from "../../entities/Responses";

interface Props {

}

interface States {
    myProfile: GetMyProfileResponse | null,
    PfpSrc: string,

    editPfpEnabled: boolean,
    editPfpFile: File | null,

    editDisplayNameEnabled: boolean,
    editDisplayNameValue: string,
}

export class MyProfileComponent extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props);
        this.state = {
            myProfile: null,
            PfpSrc: "",

            editPfpEnabled: false,
            editPfpFile: null,

            editDisplayNameEnabled: false,
            editDisplayNameValue: "",
        }
    }


    getMyProfile = (): void => {
        userService.getMyProfile()
            .then((response: GetMyProfileResponse) => {
                this.setState({
                    myProfile: response,
                    PfpSrc: `https://res.cloudinary.com/ddi8u0lxw/image/upload/user${response.userId}`
                })
            })
    }

    onPfpInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {

        if (!e.target.files || !e.target.files[0]) {
            console.error("No files uploaded");
            return;
        }

        const reader = new FileReader();
        reader.onloadend = (): void => {
            this.setState({
                PfpSrc: reader.result as string,
            });
        }
        reader.readAsDataURL(e.target.files[0]);

        this.setState({
            editPfpFile: e.target.files[0],
        });
    }

    onPfpEditSave = (): void => {
        const { editPfpFile } = this.state;

        if (editPfpFile === null) {
            console.error("No file selected");
            return;
        }

        const request: UploadPfpRequest = {
            file: editPfpFile,
        }

        userService.uploadPfp(request)
            .then(() => {
                this.setState({
                    editPfpEnabled: false,
                });
            });
    }

    togglePfpEdit = (): void => {
        const { myProfile, editPfpEnabled } = this.state;
        this.setState({
            editPfpEnabled: !editPfpEnabled,
            PfpSrc: `https://res.cloudinary.com/ddi8u0lxw/image/upload/user${myProfile?.userId}`,
        });
    }

    onDisplayNameInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            editDisplayNameValue: e.target.value,
        })
    }

    toggleChangeDisplayName = (): void => {
        const { editDisplayNameEnabled, myProfile } = this.state;
        this.setState({
            editDisplayNameEnabled: !editDisplayNameEnabled,
            editDisplayNameValue: myProfile?.displayName as string,
        });
    }

    onDisplayNameSave = (): void => {
        const { editDisplayNameValue, myProfile } = this.state;
        const request: UpdateMyProfileRequest = {
            ...myProfile as GetMyProfileResponse,
            displayName: editDisplayNameValue,
        };
        userService.updateMyProfile(request)
            .then(() => {
                this.getMyProfile();
                this.toggleChangeDisplayName();
            });
    }

    componentDidMount() {
        this.getMyProfile()
    }

    render(): JSX.Element {

        const { myProfile, editPfpEnabled, PfpSrc, editDisplayNameEnabled, editDisplayNameValue } = this.state;

        return <div id="profileComponent" className="flex-center-items">
            {myProfile !== null ?
                <>
                    <label id="mainLabel">My Profile</label>
                    <div className="my-profile-container">
                            <img id="myPfp" className="img-pfp" src={PfpSrc} />
                        {editPfpEnabled ?
                            <div id="editPfpContainer">
                                <input type="file" className="button-style" accept="image/png, image/jpeg" onChange={this.onPfpInputChange} />
                                <button className="button-style" onClick={this.onPfpEditSave}>Save</button>
                                <button className="button-style btn-decline" onClick={this.togglePfpEdit}>Cancel</button>
                            </div>
                            :
                            <button className="button-style" onClick={this.togglePfpEdit}>Change Profile Picture</button>
                        }
                    </div>
                    <div className="my-profile-container flex-center-items">
                        <label>Display Name</label>
                        {editDisplayNameEnabled ? 
                            <div id="editDisplayNameContainer">
                                <p className="p-fineprint">Minimum of 4 characters required</p>
                                <input className="input-style" value={editDisplayNameValue} onChange={this.onDisplayNameInputChange} />
                                <button className="button-style btn-decline" onClick={this.toggleChangeDisplayName}>Cancel</button>
                                <button className="button-style btn-accept" onClick={this.onDisplayNameSave}>Save</button>
                            </div>
                            :
                            <>
                                <input name="disabledDisplayNameInput" className="input-style" value={myProfile.displayName} disabled />
                                <button id="changeDisplayNameButton" className="button-style" onClick={this.toggleChangeDisplayName}>Change Display Name</button>
                            </>
                        }
                    </div>
                    <div className="my-profile-container">
                        <label>Login Name</label>
                        <input className="input-style" value={myProfile.loginName} disabled />
                    </div>
                    <div className="my-profile-container">
                        <label>E-mail</label>
                        <input className="input-style" value={myProfile.email} disabled />
                    </div>
                </>
                :
                <>
                    <label>Loading...</label>
                </>
            }

        </div>
    }
}