import React from "react";
import "./GroupInvitation.css"
import { GetGroupChatResponse, GroupInvitationFriendInfoResponse } from "../../entities/Responses";
import { GetFriendsForGroupInvitationRequest, SendGroupChatInvitationRequest } from "../../entities/Requests";
import { chatService } from "../../services/ChatService";

enum InvitationStatus {
    Dissociated = -1,
    Pending = 0,
    Joined = 1,
}

interface Props {
    info: GetGroupChatResponse,
}

interface States {
    searchBarInput: string,
    friends: GroupInvitationFriendInfoResponse[],
}

export class GroupInvitationComponent extends React.Component<Props, States> {

    interval: NodeJS.Timeout | null;

    constructor(props: Props) {
        super(props)
        this.state = {
            searchBarInput: "",
            friends: [],
        }

        this.interval = null;
    }

    getFriends = (partialDisplayName: string): void => {
        const request: GetFriendsForGroupInvitationRequest = {
            chatId: this.props.info.chatId,
            partialDisplayName: partialDisplayName,
        };
        if (partialDisplayName == "") {
            chatService.getFriendsForGroupInvitation(request).
                then((response: GroupInvitationFriendInfoResponse[]) => {
                    this.setState({
                        friends: response,
                    })
                });
        }
        else {
            chatService.getFriendsForGroupInvitationByPartialDisplayName(request).
                then((response: GroupInvitationFriendInfoResponse[]) => {
                    this.setState({
                        friends: response,
                    })
                });
        }
    }

    sendInvitation = (friendUserId: number): void => {
        const request: SendGroupChatInvitationRequest = {
            friendUserId: friendUserId,
            chatId: this.props.info.chatId,
        }
        chatService.sendGroupChatInvitationToFriend(request)
            .then(() => {
                const { searchBarInput } = this.state;
                this.getFriends(searchBarInput);
            });
    }

    onSearchBarInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            searchBarInput: e.target.value,
        })
        this.getFriends(e.target.value);
    }

    componentDidMount() {
        this.getFriends("");
        this.interval = setInterval(() => {
            const { searchBarInput } = this.state;
            this.getFriends(searchBarInput);
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.interval as NodeJS.Timeout);
        this.interval = null;
    }

    render(): JSX.Element {

        const { searchBarInput, friends } = this.state;

        return <div id="groupInvitationComponent" className="container-style2 flex-column">
            <label id="mainLabel">Invite people to </label>
            <p>{this.props.info.chatDisplayName}</p>
            <input placeholder="Search for friends" className="input-style" value={searchBarInput} onChange={this.onSearchBarInputChange} />
            <ol className="flex-column">
                {friends.map((info: GroupInvitationFriendInfoResponse) => (
                    <li key={info.friendUserId} id="friendListItem" className="li-friend-style flex-column">
                        <label>{info.friendDisplayName}</label>
                        {info.invitationStatus == InvitationStatus.Dissociated ?
                            <button className="button-style" onClick={() => this.sendInvitation(info.friendUserId)}>Invite</button>
                            : info.invitationStatus == InvitationStatus.Pending ?
                                <div className="container-style">Pending</div>
                                : info.invitationStatus >= InvitationStatus.Joined ?
                                    <div className="container-style">Joined</div>
                                    : <></>
                        }
                    </li>
                ))}
            </ol>
        </div>
    }
}