import React from "react";
import "./App.css"
import { FriendsComponent } from "../Friends/FriendsComponent";
import { MyProfileComponent } from "../MyProfile/MyProfileComponent";
import { GroupsComponent } from "../Groups/GroupsComponent";

interface Props {
    logout: () => void,
}

interface States {
    childComponent: JSX.Element

}

export class AppComponent extends React.Component<Props, States> {

    constructor(props: Props) {
        super(props)
        this.state = {
            childComponent: this.getFriendsElement(),
        };
    }

    onFriendsClick = (): void => {
        this.setState({
            childComponent: this.getFriendsElement(),
        })
    }

    getFriendsElement = (): JSX.Element => {
        return <FriendsComponent />
    }

    onGroupsClick = (): void => {
        this.setState({
            childComponent: <GroupsComponent />
        })
    }

    onProfileClick = (): void => {
        this.setState({
            childComponent: <MyProfileComponent />
        })
    }

    public render(): JSX.Element {

        const { childComponent } = this.state;

        return <div id="appBody">
            <header>
                <div id="logo">

                </div>
                <nav>
                    <button
                        name="friendsPageBtn"
                        className="nav-button button-style"
                        onClick={this.onFriendsClick}
                    >
                        <img />
                        <label>Friends</label>
                    </button>
                    <button
                        name="groupsPageBtn"
                        className="nav-button button-style"
                        onClick={this.onGroupsClick}
                    >
                        <img />
                        <label>Groups</label>
                    </button>
                    <button
                        name="myProfilePageBtn"
                        className="nav-button button-style"
                        onClick={this.onProfileClick}
                    >
                        <img />
                        <label>My Profile</label>
                    </button>
                    <button
                        name="logoutBtn"
                        className="nav-button button-style btn-decline"
                        onClick={this.props.logout}
                    >
                        <img />
                        <label>Logout</label>
                    </button>
                </nav>
            </header>
            <main>
                {childComponent}
            </main>
            <footer>

            </footer>
        </div>
    }
}

