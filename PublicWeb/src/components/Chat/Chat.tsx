import React from "react";
import "./Chat.css"
import { getDecodedToken } from "../../entities/JWT";
import { chatService } from "../../services/ChatService";
import { msgService } from "../../services/MessageService";
import { GetChatInfoResponse, Response } from "../../entities/Responses";
import { ChangeChatNameRequest, CreateMessageRequest, DeleteMessageRequest, EditMessageRequest, GetChatInfoRequest, KickMemberRequest } from "../../entities/Requests";
import { ChatUserDTO, MessageDTO } from "../../entities/DTOs";

enum ChatRole {
    Unknown = 0,
    Member = 1,
    Moderator = 2,
    Founder = 3,
}

interface Props {
    chatId: number;
    closeChat: () => void,
}

interface States {
    messageInput: string,

    messageEditIndex: number | null; // Which message is selected to be edited by the user. Null means no message selected for edit.
    messageEditContent: string;

    chatInfo: GetChatInfoResponse | null;
    enableChatNameEdit: boolean;
    newChatName: string;
}

export class ChatComponent extends React.Component<Props, States> {

    interval: NodeJS.Timeout | null;

    constructor(props: Props) {
        super(props)
        this.state = {
            messageInput: "",

            messageEditIndex: null,
            messageEditContent: "",

            chatInfo: null,
            enableChatNameEdit: false,
            newChatName: "",
        }

        this.interval = null;
    }

    getChatInfo = (): void => {

        const request: GetChatInfoRequest = {
            chatId: this.props.chatId,
        }
        chatService.getChatInfo(request)
            .then((response: GetChatInfoResponse) => {
                console.log(response);
                if (response.success) {
                    this.setState({
                        chatInfo: response,
                    })
                }
                else {
                    this.props.closeChat();
                }
            })
    }

    sendMessage = (): void => {
        const { messageInput } = this.state;

        this.setState({
            messageInput: "",
        })

        const request: CreateMessageRequest = {
            chatId: this.props.chatId,
            content: messageInput,
        }
        msgService.sendMessage(request)
            .then(() => {
                this.getChatInfo();
            });
    }

    onMessageInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            messageInput: e.target.value,
        })
    }

    onMessageDeleteClick = (msgId: number): void => {
        const request: DeleteMessageRequest = {
            msgId: msgId,
        }
        msgService.deleteMessage(request)
            .then(this.getChatInfo);
    }

    onMessageEditClick = (index: number, content: string): void => {
        this.setState({
            messageEditIndex: index,
            messageEditContent: content,
        })
    }

    onMessageEditInputChange = (e: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({
            messageEditContent: e.target.value,
        })
    }

    onAcceptMessageEditClick = (): void => {
        const { messageEditContent, messageEditIndex } = this.state;
        const request: EditMessageRequest = {
            msgId: messageEditIndex as number,
            content: messageEditContent,
        }
        msgService.editMessage(request)
            .then(() => {
                this.getChatInfo();
                this.onCancelMessageEditClick();
            });

    }

    onCancelMessageEditClick = (): void => {
        this.setState({
            messageEditIndex: null,
            messageEditContent: "",
        })
    }

    toggleChatNameEdit = (): void => {
        const { enableChatNameEdit } = this.state;
        this.setState({
            enableChatNameEdit: !enableChatNameEdit,
            newChatName: "",
        })
    }

    onChatNameEditAccept = (): void => {
        const { newChatName } = this.state;
        const request: ChangeChatNameRequest = {
            chatId: this.props.chatId,
            newName: newChatName,
        }
        chatService.changeChatName(request)
            .then((response: Response) => {
                if (response.success) {
                    this.getChatInfo();
                    this.toggleChatNameEdit();
                }
            })
    }

    onChatNameEditInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            newChatName: e.target.value,
        })
    }

    kickMember = (kickUserId: number): void => {
        const request: KickMemberRequest = {
            chatId: this.props.chatId,
            kickUserId: kickUserId,
        };
        chatService.kickMember(request)
            .then(() => {
                this.getChatInfo();
            });
    }

    componentDidMount() {
        this.getChatInfo();
        this.interval = setInterval(() => {
            this.getChatInfo();
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval as NodeJS.Timeout);
        this.interval = null;
    }

    render(): JSX.Element {

        const { messageInput, messageEditIndex, messageEditContent, chatInfo, enableChatNameEdit, newChatName } = this.state;

        if (chatInfo === null) {
            return <label>Loading...</label>
        }

        return <div id="chatComponent" className="container-style2">
            <div id="chatInfoContainer">
                {enableChatNameEdit ?
                    <div id="chatNameEditContainer" className="flex-center-row">
                        <input className="input-style" placeholder="Minimum 4 characters" value={newChatName} onChange={this.onChatNameEditInputChange} />
                        <button className="button-style btn-accept" onClick={this.onChatNameEditAccept}>Save</button>
                        <button className="button-style btn-decline" onClick={this.toggleChatNameEdit}>Cancel</button>
                    </div>
                    :
                    <div className="flex-center-row">
                        <label>{chatInfo.chatDisplayName}</label>
                        {chatInfo.myChatRole == ChatRole.Founder ?
                            <button className="button-style" onClick={this.toggleChatNameEdit}>Edit</button> : <></>
                        }
                    </div>
                }
            </div>
            <label id="chatListLabel">Members</label>
            <ol id="chatLog" className="flex-column">
                {chatInfo.messages.map((msg: MessageDTO) => (
                    <li key={msg.msgId} id="chatLogItem" className={`container-style ${msg.senderUserId == getDecodedToken()?.nameid ? "myMessage" : ""}`}>
                        <img className="img-pfp" src={`https://res.cloudinary.com/ddi8u0lxw/image/upload/user${msg.senderUserId}`} />
                        <label>{msg.userDisplayName}</label>
                        {messageEditIndex == msg.msgId ?
                            <>
                                <input value={messageEditContent} className="input-style" onChange={this.onMessageEditInputChange} />
                                <div id="editMessageTools">
                                    <button id="acceptEditButton" className="button-style" onClick={this.onAcceptMessageEditClick}>Confirm</button>
                                    <button id="cancelEditButton" className="button-style" onClick={this.onCancelMessageEditClick}>Cancel</button>
                                </div>
                            </>
                            :
                            <>
                                <p>{msg.content}</p>
                                {msg.senderUserId == getDecodedToken()?.nameid ?
                                    <div className="hiddenTools">
                                        <button id="deleteMessageButton" className="button-style" onClick={() => this.onMessageDeleteClick(msg.msgId)}>Delete</button>
                                        <button id="editMessageButton" className="button-style" onClick={() => this.onMessageEditClick(msg.msgId, msg.content)}>Edit</button>
                                    </div>
                                    : chatInfo.myChatRole != ChatRole.Member && chatInfo.myChatRole > msg.senderChatRole ? <>
                                        <div className="hiddenTools">
                                            <button id="deleteMessageButton" className="button-style" onClick={() => this.onMessageDeleteClick(msg.msgId)}>Delete</button>
                                        </div>
                                    </> : <></>}
                            </>
                        }
                    </li>
                ))}
            </ol>

            <input
                id="messageInput"
                className="input-style"
                placeholder="Type here your message"
                value={messageInput}
                onChange={this.onMessageInputChange}
            />

            <button
                id="messageSendButton"
                className="button-style"
                onClick={this.sendMessage}
            >Send</button>

            <ol id="chatMembersList" className="container-style2 flex-column">
                {chatInfo.chatUsers.map((member: ChatUserDTO) => (
                    // @ts-expect-error harmless
                    <li name={"userId" + member.userId} key={member.userId} id="memberListLogItem">
                        <img className="img-pfp" src={`https://res.cloudinary.com/ddi8u0lxw/image/upload/user${member.userId}`} />
                        <p>{member.userId == getDecodedToken()?.nameid ? ">" : ""} {member.userDisplayName}</p>
                        <label>{ChatRole[member.chatRole]}</label>
                        {chatInfo.myChatRole != ChatRole.Member && chatInfo.myChatRole > member.chatRole ? <>
                            <div className="hiddenTools">
                                <button id="kickBtn" className="button-style btn-decline" onClick={() => this.kickMember(member.userId)}>Kick</button>
                            </div>
                        </> : <></>}
                    </li>
                ))}
            </ol>

        </div>

    }
}