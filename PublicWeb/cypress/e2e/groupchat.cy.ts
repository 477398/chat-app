describe("Groupchats", () => {

    beforeEach(() => {
        cy.visit('/');
        // Login
        cy.get(`input[name="id"]`).type("test")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();
        // Goto groups page
        cy.get(`button[name="groupsPageBtn"]`).click();
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).should("have.length", 0)
        cy.get(`button[id="createNewGroupBtn"]`).click();
        cy.get(`button[id="openChatButton"]`).click();
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).should("have.length", 1)
    });

    /*
    afterEach(() => {
        cy.get(`button[id="groupLeaveButton"]`).click();
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).should("have.length", 0)
    });
    */

    it("Should able to sent a message", () => {
        // Send message
        cy.get(`input[id="messageInput"]`).type("this is a test message");
        cy.get(`button[id="messageSendButton"]`).click();
        cy.contains("this is a test message");

        // Remove chat
        cy.get(`button[id="groupLeaveButton"]`).click();
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).should("have.length", 0)
    });


    it("Test1 invites Test2. Test2 accepts. Test1 kicks Test2.", () => {
        // Invite Test2
        cy.get(`button[id="inviteFriendsButton"]`).click();
        cy.get(`div[id="groupInvitationComponent"] button`).click();

        // Login Test2 and accept the invitation
        cy.get(`button[name="logoutBtn"]`).click();
        cy.get(`input[name="id"]`).clear().type("test2");
        cy.get(`input[name="password"]`).clear().type("12345678");
        cy.get(`button[name="loginBtn"]`).click();
        cy.get(`button[name="groupsPageBtn"]`).click();
        cy.get(`button[id="viewInvitationsBtn"]`).click();
        cy.get(`button[id="acceptButton"]`).click();

        // Verify the group got added to the list
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).then((lis) => {
            expect(lis.length).to.be.at.least(1);
        })

        // Log back into Test1 and kick Test2
        cy.get(`button[name="logoutBtn"]`).click();
        cy.get(`input[name="id"]`).clear().type("test")
        cy.get(`input[name="password"]`).clear().type("12345678")
        cy.get(`button[name="loginBtn"]`).click();
        cy.get(`button[name="groupsPageBtn"]`).click();
        cy.get(`button[id="openChatButton"]`).click();
        // cy.get(`ol[id="chatMembersList"] li[name="userId10"]`).trigger("mouseover");
        cy.get(`button[id="kickBtn"]`).click({ force: true });

        // Verify he got kicked
        cy.get(`ol[id="chatMembersList"]`).children(`li`).should("have.length", 1);

        // Remove chat
        cy.get(`button[id="groupLeaveButton"]`).click();
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).should("have.length", 0)
    });

    it("Change chat name", () => {
        // Enable edit
        cy.get(`div[id="chatInfoContainer"] button`).click();

        // Change name
        cy.get(`div[id="chatInfoContainer"] input`).clear().type("test name change");
        cy.get(`div[id="chatInfoContainer"] button[class="button-style btn-accept"]`).click();

        // Verify
        cy.contains("test name change")

        // Remove chat
        cy.get(`button[id="groupLeaveButton"]`).click();
        cy.get(`div[id="groupsListContainer"] ol`).children(`li`).should("have.length", 0)
    });

});