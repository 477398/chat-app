describe('Login', () => {

    beforeEach(() => {
        cy.visit("/")
    });

    it('Login to the test account', () => {
        // Login with test account credentials
        cy.get(`input[name="id"]`).type("test")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();

        // Verify
        cy.contains("Friends")
        cy.contains("Groups")
        cy.contains("My Profile")
        cy.contains("Logout")
    });

    it(`Logout of the test account`, () => {
        // Login with test account credentials
        cy.get(`input[name="id"]`).type("test")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();

        // Logout
        cy.get(`button[name="logoutBtn"]`).click()

        // Verify
        cy.contains("Login")
        cy.contains("Register")
    });

    it(`Errors on invalid e-mail`, () => {
        // Input credentials
        cy.get(`input[name="id"]`).type("vnjksehtwuenfjdkvbighw9fjladkndsgbiwruthsjf@adaljkdghwehgoiasnvdsjbsuiodyhuwrfjpdnvsobosagoagagnoagbksfgbajgbabrgag.com")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();

        // Verify
        cy.get(`p[id="loginError"]`).should("be.visible").contains("e-mail")
    });

    it(`Errors on invalid login name`, () => {
        // Input credentials
        cy.get(`input[name="id"]`).type("vnjksehtwuenfjdkvbighw9fjladkndsgbiwruthsjf@adaljkdghwehgoiasnvdsjbsuiodyhuwrfjpdnvsobosagoagagnoagbksfgbajgbabrgag")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();

        // Verify
        cy.get(`p[id="loginError"]`).should("be.visible").contains("login name")
    });

    it(`Errors on invalid password`, () => {
        // Input credentials
        cy.get(`input[name="id"]`).type("test")
        cy.get(`input[name="password"]`).type("1")
        cy.get(`button[name="loginBtn"]`).click();

        // Verify
        cy.get(`p[id="loginError"]`).should("be.visible").contains("password")
    });
})