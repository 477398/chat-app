describe('My Profile', () => {

    beforeEach(() => {
        cy.visit('/')
        // Input credentials
        cy.get(`input[name="id"]`).type("test")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();
    });

    it('Able to change display name', () => {
        // Navigate to My Profile
        cy.get(`button[name="myProfilePageBtn"]`).click()

        // Change to a different name
        cy.get(`button[id="changeDisplayNameButton"]`).click();
        let lastName;
        cy.get(`div[id=editDisplayNameContainer] input`).then((input) => {
            lastName = input.val();
            if (lastName == "test") {
                cy.get(`div[id=editDisplayNameContainer] input`).clear().type("ttest");
            }
            else {
                cy.get(`div[id=editDisplayNameContainer] input`).clear().type("test");
            }
        });

        cy.get(`div[id="editDisplayNameContainer"] button[class="button-style btn-accept"]`).click();

        // Verify
        if (lastName == "ttest") {
            cy.get(`input[name="disabledDisplayNameInput"]`).should("have.value", "ttest");
        }
        else {
            cy.get(`input[name="disabledDisplayNameInput"]`).should("have.value", "test");
        }

    });

    it(`Bad request if new display name too short`, () => {
        // Intercept
        cy.intercept("PUT", "User/MyProfile").as("request")

        // Goto my profile page
        cy.get(`button[name="myProfilePageBtn"]`).click()

        // Input an invalid new display name
        cy.get(`button[id="changeDisplayNameButton"]`).click();
        cy.get(`div[id=editDisplayNameContainer] input`).clear().type("a");
        cy.get(`div[id="editDisplayNameContainer"] button[class="button-style btn-accept"]`).click();

        // Verify
        cy.wait("@request").then((interception) => {
            expect(interception.response.statusCode).to.equal(400);
        })
    })

    it(`Cancel display name edit and discard changes`, () => {
        // Goto my profile
        cy.get(`button[name="myProfilePageBtn"]`).click()

        // Edit and cancel
        cy.get(`button[id="changeDisplayNameButton"]`).click();
        cy.get(`div[id=editDisplayNameContainer] input`).clear().type("a");
        cy.get(`div[id="editDisplayNameContainer"] button[class="button-style btn-decline"]`).click();

        // Verify
        cy.get(`input[name="disabledDisplayNameInput"]`).should("not.have.value", "a");
    });

})