describe('Friends', () => {

    beforeEach(() => {
        cy.visit('/')
        // Input credentials
        cy.get(`input[name="id"]`).type("test")
        cy.get(`input[name="password"]`).type("12345678")
        cy.get(`button[name="loginBtn"]`).click();
        // Goto friends page
        cy.get(`button[name="friendsPageBtn"]`).click();
    });

    it("Discover users should return test2 account", () => {
        // Enable the discover user component
        cy.get(`button[id="searchFriendsButton"]`).click();

        // Search for "test2" account
        cy.get(`div[id="searchBody"] input[placeholder="Search for friends"]`).type("test2");

        // Verify he appears in the list
        cy.get(`div[id="searchBody"] ol`).children("li").then((lis) => {
            expect(lis.length).to.be.at.least(1);
        })
        cy.contains("test2");
    });

    it("Discover users should show no results", () => {
        // Enable the discover user component
        cy.get(`button[id="searchFriendsButton"]`).click();

        // Search for "test2" account
        cy.get(`div[id="searchBody"] input[placeholder="Search for friends"]`).type("aklsabjkasdbgjaksddbgjksbgjkabgjisbkjdsk");

        // Verify
        cy.get(`div[id="searchBody"] ol`).children(`li`).should("have.length", 0);
    });

    it("Friendlist search returns test2 account", () => {
        // Search for "test2" account
        cy.get(`div[id="friendsList"] input[placeholder="Search for friends"]`).type("test2");

        // Verify he appears in the list
        cy.get(`div[id="friendsList"] ol`).children("li").then((lis) => {
            expect(lis.length).to.be.at.least(1);
        })
        cy.contains("test2");
    });
    
    it("Friendlist show no results", () => {
        // Search for "test2" account
        cy.get(`div[id="friendsList"] input[placeholder="Search for friends"]`).type("afnjkasfjkafhjqhfajfkdasbhguiwefhjdbvj");

        // Verify
        cy.get(`div[id="friendsList"] ol`).children("li").should("have.length", 0);
    });

    it("Test1 unfriends Test2, Test1 sends new friend request to Test2. Test2 accepts.", () => {
        // Click on unfriend test2
        cy.get(`div[id="friendsList"] li[name="friendId10"] button[id="unfriendButton"]`).click();

        // Send new friend request
        cy.get(`button[id="searchFriendsButton"]`).click();
        cy.get(`div[id="searchBody"] input[placeholder="Search for friends"]`).type("test2");
        cy.get(`div[id="searchBody"] li[name="userId10"] button`).click();

        // Verify
        cy.get(`div[id="outgoingContainer"] ol`).children("li").should("have.length", 1);

        // Accept friend request on test2 account so the same test can be ran again
        cy.get(`button[name="logoutBtn"]`).click();
        cy.get(`input[name="id"]`).clear().type("test2");
        cy.get(`input[name="password"]`).clear().type("12345678");
        cy.get(`button[name="loginBtn"]`).click();
        cy.get(`button[name="friendsPageBtn"]`)
        cy.get(`button[id="searchFriendsButton"]`).click();
        cy.get(`button[id="acceptButton"]`).click();
        cy.get(`div[id="friendsList"] ol`).children(`li`).should("have.length", 1);

    });

});
